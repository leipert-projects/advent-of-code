import { exit, loadStdInIntoArray, toInteger } from "../lib/shared.js";

const input = await loadStdInIntoArray();

const STARTING_POINT = "AA";

function loadValves(data) {
  const valves = {};
  for (const path of data) {
    const match = path.match(/Valve ([A-Z]{2}).+rate=(\d+).+valves?(.+)/);
    if (match) {
      const [, valve] = match;
      const rate = toInteger(match[2]);
      const leadsTo = match[3].split(/,/).map((n) => n.trim());
      valves[valve] = {
        valve,
        leadsTo,
        rate,
      };
    }
  }
  return valves;
}

const mem = {};

function dijkstra(valves, start = STARTING_POINT) {
  if (mem[start]) {
    return mem[start];
  }
  const distances = {};
  const prev = {};
  for (const key of Object.keys(valves)) {
    distances[key] = Number.MAX_SAFE_INTEGER;
    prev[key] = null;
  }
  distances[start] = 0;
  const Q = new Set(Object.keys(valves));
  while (Q.size) {
    let max = Number.MAX_SAFE_INTEGER;
    let u = null;
    for (const u1 of Q.keys()) {
      if (distances[u1] < max) {
        u = u1;
        max = distances[u1];
      }
    }
    Q.delete(u);
    for (const v of valves[u].leadsTo) {
      if (Q.has(v)) {
        const newDistance = distances[u] + 1;
        if (newDistance < distances[v]) {
          distances[v] = newDistance;
          prev[v] = u;
        }
      }
    }
  }
  mem[start] = distances;
  return distances;
}

const allPathsOrderedByPressure = (valves, valveOrder, totalTime = 30) => {
  const stack = [];
  const res = [];
  stack.push([0, [], valveOrder, totalTime]);

  while (stack.length) {
    const [currentPressure, currentSteps, unvisitedValves, currentTimeLeft] = stack.pop();

    const travelTimes = dijkstra(valves, currentSteps[0]);

    let terminated = true;
    for (const nextValve of unvisitedValves) {
      const timeLeft = currentTimeLeft - travelTimes[nextValve] - 1;
      if (timeLeft > 0) {
        terminated = false;

        const pressure = currentPressure + timeLeft * valves[nextValve].rate;
        const steps = [nextValve, ...currentSteps];

        stack.push([
          pressure,
          steps,
          unvisitedValves.filter((v) => v !== nextValve),
          timeLeft,
        ]);
        res.push([
          pressure,
          steps,
        ]);
      }
    }
    if (terminated) {
      res.push([currentPressure, currentSteps]);
    }
  }

  return res.sort((a, b) => b[0] - a[0]);
};

function getMaxPressure(valves, valveOrder) {
  return allPathsOrderedByPressure(valves, valveOrder)[0][0];
}

function getMaxPressureWithElephant(valves, valveOrder) {
  let mostPressureReleased = -1;
  const paths = allPathsOrderedByPressure(valves, valveOrder, 26);
  const maxPressure = paths[0][0];
  for (let human = 0; human < paths.length; human += 1) {
    const [humanPressure, humanSteps] = paths[human];
    if (humanPressure + maxPressure < mostPressureReleased) {
      return mostPressureReleased;
    }
    for (let elephant = human + 1; elephant < paths.length; elephant += 1) {
      if (
        humanPressure + paths[elephant][0] > mostPressureReleased &&
        paths[elephant][1].every((x) => !humanSteps.includes(x))
      ) {
        mostPressureReleased = humanPressure + paths[elephant][0];
      }
    }
  }
  return mostPressureReleased;
}

try {
  const valves = loadValves(input);

  const allFunctionalValves = Object.values(valves).filter((x) => x.rate > 0).map((x) => x.valve);

  const maxPressure = getMaxPressure(valves, allFunctionalValves);

  console.log(`Part 1: ${maxPressure}`);

  const maxPressureWithElephant = getMaxPressureWithElephant(
    valves,
    allFunctionalValves,
  );
  console.log(`Part 2: ${maxPressureWithElephant}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
