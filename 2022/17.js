import { exit, loadStdIn } from "../lib/shared.js";
import Map2D from "../lib/Map2D.js";

const DEBUG = false;
const FIELD_WIDTH = 7;

const log = DEBUG
  ? (...args) => {
    console.log(...args);
  }
  : () => {};

const renderMap = DEBUG
  ? (map, rock) => {
    /* eslint-disable no-param-reassign */
    const oldMax = map.maxY;
    map.maxY += 10;
    const lines = map.render((x) => x ?? ".");
    rock.forEach(([x, y]) => {
      lines[y] = `${lines[y].substring(0, x)}@${lines[y].substring(x + 1)}`;
    });
    map.maxY = oldMax;
    console.log(lines.reverse().join("\n"));
    console.log();
    /* eslint-enable no-param-reassign */
  }
  : () => {};

// deno-fmt-ignore
const rockGenerator = [
  // ####
  (y) => [[2, y + 3], [3, y + 3], [4, y + 3], [5, y + 3]],
  /*
  .#.
  ###
  .#.
  */
  (y) => [
                [3, y + 5],
    [2, y + 4], [3, y + 4], [4, y + 4],
                [3, y + 3],
  ],
  /*
  ..#
  ..#
  ###
  */
  (y) => [
                            [4, y + 5],
                            [4, y + 4],
    [2, y + 3], [3, y + 3], [4, y + 3],
  ],
  /*
  #
  #
  #
  #
  */
  (y) => [[2, y + 3], [2, y + 4], [2, y + 5], [2, y + 6]],
  /*
  ##
  ##
  */
  (y) => [
    [2, y + 4], [3, y + 4],
    [2, y + 3], [3, y + 3]
  ],
];

function rockLanded(map, rock) {
  return rock.some(([x, y]) => map.get(x, y - 1));
}

// function renderAroundY(map, minY) {
//   const res = [];
//
//   for (let y = minY - 5; y <= minY + 5; y += 1) {
//     let line = "";
//     for (let x = map.minX; x <= map.maxX; x += 1) {
//       line += map.get(x, y) ?? ".";
//     }
//     line += `| ${y}`;
//     res.push(line);
//   }
//
//   return res.reverse().join("\n");
// }

function getFingerPrint(map, x, y, currJet) {
  let fp = `${x}|${currJet}\n`;
  for (let i = 0; i < 100; i += 1) {
    for (let j = 0; j < FIELD_WIDTH; j += 1) {
      fp += map.get(j, y - i) ?? ".";
    }
    fp += "\n";
  }
  return fp;
}

function playGame(jetPattern, turns = 2022) {
  const map = new Map2D();
  for (let x = 0; x < FIELD_WIDTH; x += 1) {
    map.set(x, 0, "-");
  }
  let currRock = 0;
  let currJet = 0;

  let cycleSearch = [];

  while (currRock < turns) {
    const generator = currRock % rockGenerator.length;
    let rock = rockGenerator[generator](map.maxY + 1);

    renderMap(map, rock);

    let doJet = true;

    while (doJet || !rockLanded(map, rock)) {
      if (doJet) {
        log(`JET ${jetPattern[currJet]}`);
        let oldRock = false;
        const rockNext = [];
        const dir = jetPattern[currJet] === ">" ? +1 : -1;
        for (const [x, y] of rock) {
          const nextX = x + dir;
          if (
            nextX < map.minX || nextX > map.maxX || map.get(nextX, y)
          ) {
            oldRock = true;
            break;
          }
          rockNext.push([x + dir, y]);
        }
        rock = oldRock ? rock : rockNext;
        currJet += 1;
        currJet %= jetPattern.length;
      } else {
        log("DROP");
        for (const rockItem of rock) {
          rockItem[1] -= 1;
        }
      }
      doJet = !doJet;

      renderMap(map, rock);
    }
    let cycleFound;

    if (cycleSearch && generator === 0) {
      const [x, y] = rock[0];
      const fp = getFingerPrint(map, x, y, currJet);

      for (const entry of cycleSearch) {
        if (fp === entry[0]) {
          cycleFound = entry;
          break;
        }
      }
      cycleSearch.push([fp, currRock, y]);
    }
    if (cycleFound) {
      const y = rock[0][1];
      cycleSearch = null;

      // How Long is this cycle
      const cycleLength = currRock - cycleFound[1];
      // How much height is added during one cycle
      const cycleHeight = y - cycleFound[2];

      console.log(`Found cycle of length ${cycleLength}, covering a height of ${cycleHeight}`);

      const fittingCycles = Math.floor((turns - currRock) / cycleLength);
      if (fittingCycles > 0) {
        currRock += fittingCycles * cycleLength;
        const newY = (fittingCycles * cycleHeight) + y;

        console.log(`Fast forwarding ${fittingCycles} cycles to rock ${currRock} at ${newY}`);

        for (const rockItem of rock) {
          rockItem[1] = newY;
        }

        const seen = new Set();
        let i = 0;
        while (seen.size < FIELD_WIDTH) {
          for (let j = 0; j < FIELD_WIDTH; j += 1) {
            const val = map.get(j, y - i);
            if (val) {
              map.set(j, newY - i, "#");
              seen.add(j);
            }
          }
          i += 1;
        }
      }
    }

    rock.forEach((point) => map.set(...point, "#"));

    currRock += 1;
  }

  return map.maxY;
}

try {
  const input = await loadStdIn();
  const example = ">>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>";
  const part2Length = 1_000_000_000_000;

  let part1 = playGame(example, DEBUG ? 10 : 2022);
  console.log(`EXAMPLE - Part 1: ${part1}\n`);
  console.assert(part1 === 3068);

  let part2 = playGame(example, DEBUG ? 10 : part2Length);
  console.log(`EXAMPLE - Part 2: ${part2}\n`);
  console.assert(part2 === 1514285714288);

  part1 = playGame(input, DEBUG ? 10 : 2022);
  console.log(`INPUT - Part 1: ${part1}\n`);
  part2 = playGame(input, DEBUG ? 10 : part2Length);
  // 1572093023267
  console.log(`INPUT - Part 2: ${part2}\n`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
