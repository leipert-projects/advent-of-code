import { exit, loadStdInIntoArray, toInteger } from "../lib/shared.js";
import Map2D from "../lib/Map2D.js";

const stdIn = await loadStdInIntoArray();

const getNextByDir = (curr, dir) => {
  if (dir === 0) {
    return [curr[0] + 1, curr[1]];
  }
  if (dir === 1) {
    return [curr[0], curr[1] + 1];
  }
  if (dir === 2) {
    return [curr[0] - 1, curr[1]];
  }
  if (dir === 3) {
    return [curr[0], curr[1] - 1];
  }
  throw new Error("What");
};
function part1Fn(data) {
  const map = new Map2D();
  const command = data[data.length - 1].split(/([LR])/);
  let curr = null;
  for (let y = 0; y < data.length - 1; y += 1) {
    for (let x = 0; x < data[y].length; x += 1) {
      if (data[y][x] !== " ") {
        map.set(x, y, data[y][x]);
        if (!curr && data[y][x] === ".") {
          curr = [x, y];
        }
      }
    }
  }
  let dir = 0;
  const arrow = ">v<^";
  while (command.length) {
    const currCommand = command.shift();
    if (currCommand === "L") {
      dir -= 1;
      if (dir < 0) {
        dir = 3;
      }
      continue;
    }
    if (currCommand === "R") {
      dir += 1;
      if (dir > 3) {
        dir = 0;
      }
      continue;
    }
    let steps = toInteger(currCommand);
    while (steps > 0) {
      map.set(...curr, arrow[dir]);
      let next = [...curr];
      let nextVal;
      let pristine = true;
      do {
        next = getNextByDir(next, dir);
        nextVal = map.get(...next);
        if (!nextVal && pristine) {
          pristine = false;
          if (dir === 0) {
            next[0] = map.minX - 1;
          } else if (dir === 1) {
            next[1] = map.minY - 1;
          } else if (dir === 2) {
            next[0] = map.maxX + 1;
          } else if (dir === 3) {
            next[1] = map.maxY + 1;
          }
        }
      } while (!nextVal);
      if (nextVal === "#") {
        break;
      }
      curr = next;
      steps -= 1;
    }
  }
  return dir + (curr[1] + 1) * 1000 + (curr[0] + 1) * 4;
}

// function part2Fn(data) {
//   console.log(data)
// }

try {
  const part1 = part1Fn(stdIn);

  console.log(`\tPart 1: ${part1}`);

  // const part2 = part2Fn(stdIn);
  //
  // console.log(`\tPart 2: ${part2}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
