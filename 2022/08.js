import { exit, loadStdInIntoArray, toInteger } from "../lib/shared.js";

const input = await loadStdInIntoArray();

const columns = [];
const rows = [];

const EDGE = Symbol("Edge");

class Tree {
  #blockingTrees;

  constructor(height, x, y) {
    this.height = height;
    this.x = x;
    this.y = y;
  }

  isVisible() {
    return this.blockingTrees().some((x) => x === EDGE);
  }

  valueOf() {
    return this.height;
  }

  toString() {
    return `Tree: (${this.x},${this.y}) Height: ${this.height} Score: ${this.scenicScore()}`;
  }

  blockingTrees() {
    if (!this.#blockingTrees) {
      this.#blockingTrees = [
        columns[this.x].findLast((t, i) => i < this.y && t >= this.height) || EDGE,
        rows[this.y].findLast((t, i) => i < this.x && t >= this.height) || EDGE,
        rows[this.y].find((t, i) => i > this.x && t >= this.height) || EDGE,
        columns[this.x].find((t, i) => i > this.y && t >= this.height) || EDGE,
      ];
    }
    return this.#blockingTrees;
  }

  scenicScore() {
    const [t, l, r, b] = this.blockingTrees();

    return [
      this.y - (t === EDGE ? 0 : t.y),
      this.x - (l === EDGE ? 0 : l.x),
      (r === EDGE ? columns.length - 1 : r.x) - this.x,
      (b === EDGE ? rows.length - 1 : b.y) - this.y,
    ].reduce((acc, c) => acc * c, 1);
  }
}

for (let y = 0; y < input.length; y += 1) {
  const line = input[y];
  rows[y] = [];
  for (let x = 0; x < line.length; x += 1) {
    columns[x] ||= [];
    const height = toInteger(line[x]);
    const tree = new Tree(height, x, y);
    rows[y].push(tree);
    columns[x].push(tree);
  }
}

try {
  const part1 = columns.flat()
    .reduce((acc, node) => node.isVisible() ? acc + 1 : acc, 0);
  console.warn(`Part 1: ${part1}`);

  const part2 = columns.flat()
    .reduce((acc, curr) => acc.scenicScore() < curr.scenicScore() ? curr : acc);

  console.log(`Part 2: ${part2}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
