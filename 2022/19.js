import { exit, loadStdIn, toInteger } from "../lib/shared.js";

class BluePrint {
  constructor(line) {
    const ore = line.match(/Each ore robot costs (\d+) ore/);
    const clay = line.match(/Each clay robot costs (\d+) ore/);
    const obsidian = line.match(/Each obsidian robot costs (\d+) ore and (\d+) clay/);
    const geode = line.match(/Each geode robot costs (\d+) ore and (\d+) obsidian/);

    this.id = toInteger(line.match(/Blueprint (\d+):/)[1]);
    this.bots = [
      [toInteger(geode[1]), 0, toInteger(geode[2]), 0], // geode
      [toInteger(obsidian[1]), toInteger(obsidian[2]), 0, 0], // obsidian
      [toInteger(clay[1]), 0, 0, 0], // clay
      [toInteger(ore[1]), 0, 0, 0], // ore
      [0, 0, 0, 0],
    ];
  }

  run2(steps = 24) {
    let states = [
      [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
    ];
    for (let i = 0; i < steps; i += 1) {
      const queue = [];
      for (const state of states) {
        for (let k = 0; k < 5; k += 1) {
          if (
            state[1] >= this.bots[k][2] &&
            state[2] >= this.bots[k][1] &&
            state[3] >= this.bots[k][0]
          ) {
            const newState = [...state];
            newState[0] += newState[4];
            newState[1] += newState[5] - this.bots[k][2];
            newState[2] += newState[6] - this.bots[k][1];
            newState[3] += newState[7] - this.bots[k][0];
            if (k !== 4) {
              newState[4 + k] += 1;
            }
            queue.push(newState);
          }
        }
      }
      states = queue.sort((a, b) => {
        for (let j = 0; j < 8; j += 1) {
          if ((a[j] + a[j + 4]) === (b[j] + b[j + 4])) {
            continue;
          }
          return b[j] - a[j];
        }
        return 0;
      }).slice(0, 1000);
    }
    return states[0][0];
  }
}

const blueprints = (await loadStdIn()).split(/\n+/).map((line) => new BluePrint(line));

function part1Fn(input) {
  return input.reduce((sum, blueprint) => sum + blueprint.run2(24) * blueprint.id, 0);
}

function part2Fn(input) {
  return input.slice(0, 3).reduce((product, blueprint) => product * blueprint.run2(32), 1);
}

try {
  const part1 = part1Fn(blueprints);

  console.log(`\tPart 1: ${part1}`);

  const part2 = part2Fn(blueprints);

  console.log(`\tPart 2: ${part2}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
