import { exit, loadStdInIntoArray, toInteger } from "../lib/shared.js";
import { bisect } from "../lib/math.js";

const stdIn = await loadStdInIntoArray();

function parseInput(data) {
  const monkeys = {};
  for (const line of data) {
    const [name, cmd] = line.split(": ");

    const int = toInteger(cmd);

    if (Number.isNaN(int)) {
      const [monkey1, operator, monkey2] = cmd.split(/ ([/*+-]) /);
      monkeys[name] = function monkey() {
        const a = monkeys[monkey1]();
        const b = monkeys[monkey2]();
        switch (operator) {
          case "+":
            return a + b;
          case "-":
            return a - b;
          case "*":
            return a * b;
          case "/":
            return a / b;
          default:
            throw new Error("Unknown operator");
        }
      };
      monkeys[name].monkeyA = monkey1;
      monkeys[name].monkeyB = monkey2;
    } else {
      monkeys[name] = () => int;
    }
  }

  return monkeys;
}

function scream(monkeys) {
  const cache = {};
  const diff = (hmn) => {
    if (!cache[hmn]) {
      // eslint-disable-next-line no-param-reassign
      monkeys.humn = () => hmn;
      cache[hmn] = (monkeys[monkeys.root.monkeyB]()) - (monkeys[monkeys.root.monkeyA]());
    }
    return cache[hmn];
  };
  return bisect(
    diff,
    Number.MIN_SAFE_INTEGER / 2,
    Number.MAX_SAFE_INTEGER / 2,
  );
}

try {
  const monkeys = parseInput(stdIn);

  console.log(`\tPart 1: The root monkey yells: ${monkeys.root()}`);

  const part2 = scream(monkeys);
  //
  console.log(`\tPart 2: I scream ${part2}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
