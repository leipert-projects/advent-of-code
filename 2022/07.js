// eslint-disable-next-line max-classes-per-file
import { exit, loadStdInIntoArray, sumArray, toInteger } from "../lib/shared.js";

const input = await loadStdInIntoArray();

const DIR = Symbol("DIR");
const FILE = Symbol("FILE");

class FSNode {
  constructor(type, name) {
    this.type = type;
    this.name = name;
  }
}
class DirNode extends FSNode {
  #size;

  constructor(name) {
    super(DIR, name);
    this.children = [];
  }

  addChild(node) {
    // eslint-disable-next-line no-param-reassign
    node.parent = this;
    this.children.push(node);
  }

  get size() {
    if (!this.#size) {
      this.#size = sumArray(this.children.map((x) => x.size));
    }
    return this.#size;
  }
}

class FileNode extends FSNode {
  constructor(name, size) {
    super(FILE, name);
    this.size = size;
  }
}

function buildFS(instructions) {
  const root = new DirNode("/");
  const fs = [root];
  fs.$root = root;

  let curr = null;

  for (const line of instructions) {
    if (!curr) {
      curr = root;
      // eslint-disable-next-line no-continue
      continue;
    }

    const [instruction, arg0, arg1] = line.split(" ");

    if (instruction === "$") {
      if (arg0 !== "cd") {
        // eslint-disable-next-line no-continue
        continue;
      }

      if (arg1 === "..") {
        curr = curr.parent;
      } else {
        curr = curr.children.find((x) => x.name === arg1);
      }
    } else if (instruction === "dir") {
      const dir = new DirNode(arg0);
      curr.addChild(dir);
      fs.push(dir);
    } else {
      const file = new FileNode(arg0, toInteger(instruction));
      curr.addChild(file);
      fs.push(file);
    }
  }

  return fs;
}

try {
  const fs = buildFS(input);

  const smallDirs = fs.filter((x) => x.type === DIR && x.size < 100_000);

  const part1 = sumArray(smallDirs.map((x) => x.size));

  console.log(`Part 1: ${part1}`);

  const maxRootSize = 70_000_000 - 30_000_000;

  const largeDirs = fs.filter((x) => x.type === DIR && (fs.$root.size - x.size) < maxRootSize);

  const part2 = Math.min(...largeDirs.map((x) => x.size));

  console.log(`Part 2: ${part2}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
