import { exit, loadStdInIntoIntegerArray } from "../lib/shared.js";

const stdIn = await loadStdInIntoIntegerArray();
function decrypt(numbers, rounds = 1) {
  const curr = numbers.map((_, i) => i);

  const { length } = numbers;

  for (let j = 0; j < rounds; j += 1) {
    for (let i = 0; i < length; i += 1) {
      const currIndex = curr.indexOf(i);

      const newIndex = (currIndex + numbers[i]) % (length - 1);

      // Remove item
      curr.splice(currIndex, 1);
      // Add item at new position
      curr.splice(newIndex, 0, i);
    }
  }

  const indexOfZero = curr.indexOf(numbers.indexOf(0));
  return (
    numbers[curr[(indexOfZero + 1000) % length]] +
    numbers[curr[(indexOfZero + 2000) % length]] +
    numbers[curr[(indexOfZero + 3000) % length]]
  );
}

try {
  const part1 = decrypt(stdIn);

  console.log(`\tPart 1: Coordinate sum: ${part1}`);

  const part2 = decrypt(stdIn.map((x) => x * 811589153), 10);

  console.log(`\tPart 2: Coordinate sum decrypted: ${part2}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
