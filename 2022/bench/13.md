## 2022/data/13_example.txt

```
Part 1: 13
Part 2: 140
```

| Command                                                                                              |  Mean [ms] | Min [ms] | Max [ms] |    Relative |
| :--------------------------------------------------------------------------------------------------- | ---------: | -------: | -------: | ----------: |
| `bun run 2022/13.js < ./2022/data/13_example.txt`                                                    | 12.6 ± 0.4 |     11.8 |     14.7 |        1.00 |
| `deno run --allow-read=./ --no-config --allow-net=deno.land 2022/13.js < ./2022/data/13_example.txt` | 18.3 ± 0.7 |     17.4 |     24.3 | 1.45 ± 0.07 |
| `node 2022/13.js < ./2022/data/13_example.txt`                                                       | 35.5 ± 0.7 |     34.2 |     37.7 | 2.82 ± 0.11 |

## 2022/data/13.txt

```
Part 1: 5825
Part 2: 24477
```

| Command                                                                                      |  Mean [ms] | Min [ms] | Max [ms] |    Relative |
| :------------------------------------------------------------------------------------------- | ---------: | -------: | -------: | ----------: |
| `bun run 2022/13.js < ./2022/data/13.txt`                                                    | 13.3 ± 0.4 |     12.5 |     15.1 |        1.00 |
| `deno run --allow-read=./ --no-config --allow-net=deno.land 2022/13.js < ./2022/data/13.txt` | 20.0 ± 0.5 |     19.0 |     22.5 | 1.51 ± 0.06 |
| `node 2022/13.js < ./2022/data/13.txt`                                                       | 37.3 ± 0.6 |     36.6 |     40.1 | 2.81 ± 0.10 |

- bun 1.0.13
- deno 1.38.2 (release, aarch64-apple-darwin)
- node v21.2.0
