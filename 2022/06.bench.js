import { findMarkerLoop, findMarkerSlice, findMarkerXOR } from "./lib/06.js";
import { dirname, loadFile } from "../lib/shared.js";

const input = await loadFile(`${dirname(import.meta.url)}/data/06.txt`);

const benchFactory = (fn, size) => () => {
  fn("mjqjpqmgbljsphdztnvjfqwrcgsmlb", size);
  fn("bvwbjplbgvbhsrlpgdmjqwftvncz", size);
  fn("nppdvjthqldpwncqszvftbrmjlhg", size);
  fn("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", size);
  fn("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", size);
  fn(input, size);
};

Deno.bench("findMarkerLoop(4)", { group: "timing-4" }, benchFactory(findMarkerLoop, 4));
Deno.bench("findMarkerSlice(4)", { group: "timing-4" }, benchFactory(findMarkerSlice, 4));
Deno.bench("findMarkerXOR(4)", { group: "timing-4" }, benchFactory(findMarkerXOR, 4));

Deno.bench("findMarkerLoop(14)", { group: "timing-14" }, benchFactory(findMarkerLoop, 14));
Deno.bench("findMarkerSlice(14)", { group: "timing-14" }, benchFactory(findMarkerSlice, 14));
Deno.bench("findMarkerXOR(14)", { group: "timing-14" }, benchFactory(findMarkerXOR, 14));
