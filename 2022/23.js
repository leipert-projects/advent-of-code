import { exit, loadStdInIntoArray } from "../lib/shared.js";
import ConwaysGame from "../lib/ConwaysGame.js";
import MapND from "../lib/MapND.js";

const stdIn = await loadStdInIntoArray();

/**
 * @typedef {'north'|'south'|'west'|'east'} Direction
 */

/**
 * @extends {ConwaysGame}
 */
class UnstableDiffusion extends ConwaysGame {
  constructor() {
    super();
    /**
     * @enum {number[]}
     */
    this.directions = {
      north: this.neighbors.flatMap(([, y], idx) => y === -1 ? idx : []),
      south: this.neighbors.flatMap(([, y], idx) => y === 1 ? idx : []),
      east: this.neighbors.flatMap(([x], idx) => x === 1 ? idx : []),
      west: this.neighbors.flatMap(([x], idx) => x === -1 ? idx : []),
    };

    /**
     * @type {Direction[]}
     */
    this.order = [
      "north",
      "south",
      "west",
      "east",
    ];
  }

  /**
   * @param {number} steps
   * @return {number}
   */
  play(steps) {
    for (let i = 0; i < steps; i += 1) {
      const candidates = new MapND(this.dimensions);
      let someMoves = false;
      for (const { coordinates } of this) {
        const neighbors = this.getNeighbors(coordinates);

        if (neighbors.some((x) => x.value)) {
          someMoves = true;
          const move = this.order.find((key) =>
            this.directions[key].every((x) => !neighbors[x].value)
          );

          const newCoordinates = UnstableDiffusion.coordinatedMove(coordinates, move);
          const prev = candidates.get(...newCoordinates) || [];

          prev.push(coordinates);

          candidates.set(prev, ...newCoordinates);
        } else {
          this.setNext(true, ...coordinates);
        }
      }

      if (!someMoves) {
        return i + 1;
      }

      for (const { coordinates, value } of candidates) {
        if (value.length === 1) {
          this.setNext(true, ...coordinates);
        } else {
          value.forEach((/** @type {[number, number]} */ xy) => {
            this.setNext(true, ...xy);
          });
        }
      }
      this.commit();
      this.order.push(/** @type {Direction} */ (this.order.shift()));
    }
    throw new Error("Not enough steps");
  }

  /**
   * @param {[number,number]} coordinates
   * @param {Direction|undefined} move
   * @return {[number,number]}
   */
  static coordinatedMove(coordinates, move) {
    switch (move) {
      case "north":
        return [coordinates[0], coordinates[1] - 1];
      case "south":
        return [coordinates[0], coordinates[1] + 1];
      case "east":
        return [coordinates[0] + 1, coordinates[1]];
      case "west":
        return [coordinates[0] - 1, coordinates[1]];
      default:
        return coordinates;
    }
  }
}

/**
 * @param {string[]} data
 * @return {UnstableDiffusion}
 */
function buildGame(data) {
  const game = new UnstableDiffusion();
  for (let y = 0; y < data.length; y += 1) {
    for (let x = 0; x < data[y].length; x += 1) {
      if (data[y][x] === "#") {
        game.set(true, x, y);
      }
    }
  }
  return game;
}
/**
 * @param {string[]} data
 * @return {number}
 */
function part1Fn(data) {
  const game = buildGame(data);
  game.play(10);
  return (
    (game.maxCoordinates[0] - game.minCoordinates[0] + 1) *
      (game.maxCoordinates[1] - game.minCoordinates[1] + 1) - (game.map.size)
  );
}

/**
 * @param {string[]} data
 * @return {number}
 */
function part2Fn(data) {
  const game = buildGame(data);
  return game.play(1000);
}

try {
  const part1 = part1Fn(stdIn);

  console.log(`\tPart 1: ${part1} empty ground tiles`);

  const part2 = part2Fn(stdIn);

  console.log(`\tPart 2: ${part2} steps saved`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
