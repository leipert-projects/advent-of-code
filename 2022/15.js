import { exit, loadStdInIntoArray, toInteger } from "../lib/shared.js";

import { manhattanDistance } from "../lib/math.js";

const input = await loadStdInIntoArray();

function loadSensors(data) {
  const sensors = [];
  for (const path of data) {
    const [sX, sY, bX, bY] = path.match(/(-?\d+)/g).map(toInteger);
    const sensor = {};

    sensor.point = [sX, sY];
    sensor.nearestBeacon = [bX, bY];
    sensor.distanceToBeacon = manhattanDistance(sensor.point, sensor.nearestBeacon);

    sensors.push(sensor);
  }
  return sensors;
}

function noBeaconsOnY(sensors, y) {
  const set = new Set();
  const remove = [];
  for (const { point, nearestBeacon, distanceToBeacon } of sensors) {
    if (nearestBeacon[1] === y) {
      remove.push(nearestBeacon[0]);
    }
    const lot = [point[0], y, point[0]];
    while (manhattanDistance(lot, point) <= distanceToBeacon) {
      set.add(lot[0]);
      set.add(lot[2]);
      lot[0] -= 1;
      lot[2] += 1;
    }
  }
  remove.forEach((n) => set.delete(n));
  return set;
}

function searchDistressBeacon(sensors, maxVal) {
  let count = 0;
  for (const { point, distanceToBeacon } of sensors) {
    const minY = point[1] - (distanceToBeacon + 1);
    const maxY = point[1] + distanceToBeacon + 1;
    const minX = point[0] - (distanceToBeacon + 1);
    const maxX = point[0] + distanceToBeacon + 1;
    const current = [point[0], maxY];
    const directions = [[+1, -1], [-1, -1], [-1, +1], [+1, +1]];
    while (directions.length) {
      if (
        current[0] >= 0 && current[0] <= maxVal &&
        current[1] >= 0 && current[1] <= maxVal
      ) {
        count += 1;
        if (
          sensors.every((sensor) =>
            manhattanDistance(current, sensor.point) > sensor.distanceToBeacon
          )
        ) {
          console.log(`Searched ${(100 * count / (maxVal * maxVal))} of total search space`);
          return current;
        }
      }
      current[0] += directions[0][0];
      current[1] += directions[0][1];
      if (
        current[0] === maxX || current[0] === minX ||
        current[1] === maxY || current[1] === minY
      ) {
        directions.shift();
      }
    }
  }
  return null;
}

try {
  const [settings, ...sensorsInput] = input;
  const sensors = loadSensors(sensorsInput);
  const y = toInteger(settings);
  const part1 = noBeaconsOnY(sensors, y).size;
  console.log(`Part 1: ${part1}`);

  const beacon = searchDistressBeacon(sensors, y * 2);
  console.log(`Part 2: ${beacon[0] * 4_000_000 + beacon[1]}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
