import { exit, loadStdInIntoArray } from "../lib/shared.js";
import Map2D from "../lib/Map2D.js";

const data = await loadStdInIntoArray();

class HeightMap extends Map2D {
  getNeighbors(node, includeDiagonal = false) {
    return super.getNeighbors(node, includeDiagonal).filter((n) => n.value <= (node.value + 1));
  }
}

try {
  const map = new HeightMap();
  const starts = [];
  let end;
  for (let y = 0; y < data.length; y += 1) {
    const line = data[y];
    for (let x = 0; x < line.length; x += 1) {
      let char = line[x];
      if (char === "a") {
        starts.push([x, y]);
      }
      if (char === "S") {
        starts.unshift([x, y]);
        char = "a";
      }
      if (char === "E") {
        end = [x, y];
        char = "z";
      }
      map.set(x, y, char.charCodeAt(0));
    }
  }

  const allPaths = starts.flatMap((start) => {
    try {
      return [map.a_star(start, end)];
    } catch {
      // No path found
      return [];
    }
  });

  console.log(`Part 1: ${allPaths[0].length - 1}`);

  console.log(`Part 2: ${Math.min(...allPaths.map((x) => x.length)) - 1}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
