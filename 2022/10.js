import { exit, loadStdIn, sumArray, toInteger } from "../lib/shared.js";

const input = (await loadStdIn()).split(/\s+/);

const runInstructions = (instructions, pullCycles) => {
  let cycle = 1;
  let register = 1;
  let currTarget = pullCycles.shift();
  const signalStrengths = [];
  let crt = "";
  for (const inst of instructions) {
    if (cycle === currTarget) {
      signalStrengths.push(register * currTarget);

      currTarget = pullCycles.shift();
    }
    const posInLine = (cycle - 1) % 40;
    crt += posInLine >= register - 1 && posInLine <= register + 1 ? "▓" : " ";
    cycle += 1;
    const parsed = toInteger(inst);
    if (!Number.isNaN(parsed)) {
      register += parsed;
    }
  }
  return { signalStrengths, crt };
};

try {
  const { signalStrengths, crt } = runInstructions(input, [20, 60, 100, 140, 180, 220]);
  console.log(`Part 1: ${sumArray(signalStrengths)}`);
  console.log(`Part 2:`);
  console.log(crt.match(/.{40}/g).join("\n"));
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
