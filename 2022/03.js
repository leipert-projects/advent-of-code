import { exit, loadStdInIntoArray, sumArray } from "../lib/shared.js";
import { A_MAJOR, A_MINOR } from "../lib/ASCII.js";

const rucksacks = await loadStdInIntoArray();

/**
 * @param {string} rucksack
 * @return {[string,string]}
 */
const compartmentalize = (rucksack) => {
  const comp1 = rucksack.substring(0, rucksack.length / 2);
  const comp2 = rucksack.substring(rucksack.length / 2);
  return [comp1, comp2];
};

/**
 * @param {Array<string>} p
 * @return {Array<string>}
 */
function overlap([comp1, ...comp2]) {
  const res = new Set();
  for (let i = 0; i < comp1.length; i += 1) {
    if (comp2.every((s) => s.includes(comp1[i]))) {
      res.add(comp1[i]);
    }
  }
  return [...res];
}

/**
 * @param {string} letter
 * @return {number}
 */
function toVal(letter) {
  if (letter < "a") {
    return letter.charCodeAt(0) - A_MAJOR + 27;
  }
  return letter.charCodeAt(0) - A_MINOR + 1;
}

/**
 * @param {Array<Array<string>>} acc
 * @param {string} line
 * @param {number} index
 * @return {Array<Array<string>>}
 */
function groupBy3(acc, line, index) {
  if (index % 3 === 0) {
    acc.unshift([]);
  }
  acc[0].push(line);
  return acc;
}

try {
  const part1 = rucksacks.map(compartmentalize).flatMap(overlap).map(toVal);

  console.log(`\tPart 1: ${sumArray(part1)}`);

  const part2 = rucksacks.reduce(groupBy3, []).flatMap(overlap).map(toVal);

  console.log(`\tPart 2: ${sumArray(part2)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
