import { exit, loadStdInIntoArray, toInteger } from "../lib/shared.js";
import { multiplyArray } from "../lib/math.js";

const getExampleMonkeys = () => [
  {
    startItems: [79, 98],
    operation: (old) => old * 19,
    divisor: 23,
    decision: [2, 3],
  },
  {
    startItems: [54, 65, 75, 74],
    operation: (old) => old + 6,
    divisor: 19,
    decision: [2, 0],
  },
  {
    startItems: [79, 60, 97],
    operation: (old) => old * old,
    divisor: 13,
    decision: [1, 3],
  },
  {
    startItems: [74],
    operation: (old) => old + 3,
    divisor: 17,
    decision: [0, 1],
  },
];

const parseMonkeys = (monkeys) => {
  const bunch = [];
  let monkey;
  // Monkey 2:
  // Starting items: 81, 89, 56, 61, 99
  // Operation: new = old * 7
  // Test: divisible by 5
  //   If true: throw to monkey 1
  //   If false: throw to monkey 5
  for (const line of monkeys) {
    if (line.startsWith("Monkey")) {
      if (monkey) {
        bunch.push(monkey);
      }
      monkey = {};
      // eslint-disable-next-line no-continue
      continue;
    }
    const nums = line.match(/\d+/g)?.map(toInteger);

    if (line.includes("Starting items:")) {
      monkey.startItems = nums;
    } else if (line.includes("Test:")) {
      // eslint-disable-next-line prefer-destructuring
      monkey.divisor = nums[0];
    } else if (line.includes("throw to monkey")) {
      monkey.decision ||= [];
      monkey.decision.push(nums[0]);
    } else if (line.includes("Operation:")) {
      if (line.includes("old * old")) {
        monkey.operation = (n) => n * n;
      } else if (line.includes("*")) {
        monkey.operation = (n) => n * nums[0];
      } else if (line.includes("+")) {
        monkey.operation = (n) => n + nums[0];
      }
    }
  }
  bunch.push(monkey);
  return bunch;
};

const playGame = (monkeys, rounds, reduceWorryLevel) => {
  for (const monkey of monkeys) {
    monkey.currentItems = [...monkey.startItems];
    monkey.inspected = 0;
  }
  for (let i = 0; i < rounds; i += 1) {
    for (const monkey of monkeys) {
      while (monkey.currentItems.length > 0) {
        monkey.inspected += 1;
        const item = monkey.currentItems.shift();
        const worryLevel = reduceWorryLevel(monkey.operation(item));
        const nextMonkey = monkey.decision[Math.sign(worryLevel % monkey.divisor)];
        monkeys[nextMonkey].currentItems.push(worryLevel);
      }
    }
  }
};

function calculateMonkeyBusiness(monkeys) {
  const [first, second] = monkeys.map((x) => x.inspected).sort((b, a) => a - b);
  return first * second;
}

function createLCMWorryReducer(exampleMonkeys) {
  const LCM = multiplyArray(exampleMonkeys.map((x) => x.divisor));

  return (n) => n % LCM;
}

try {
  const exampleMonkeys = getExampleMonkeys();
  const reduceWorryLevel = (n) => Math.floor(n / 3);
  playGame(exampleMonkeys, 20, reduceWorryLevel);

  const realMonkeys = parseMonkeys(await loadStdInIntoArray());
  playGame(realMonkeys, 20, reduceWorryLevel);

  const examplePart1 = calculateMonkeyBusiness(exampleMonkeys);
  console.assert(examplePart1 === 10605);
  console.log(`Example Part 1: ${examplePart1}`);
  console.log(`Part 1: ${calculateMonkeyBusiness(realMonkeys)}`);

  playGame(exampleMonkeys, 10000, createLCMWorryReducer(exampleMonkeys));
  playGame(realMonkeys, 10000, createLCMWorryReducer(realMonkeys));
  const examplePart2 = calculateMonkeyBusiness(exampleMonkeys);
  console.assert(examplePart2 === 2713310158);
  console.log(`Example Part 2: ${examplePart2}`);
  console.log(`Part 2: ${calculateMonkeyBusiness(realMonkeys)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
