import { exit, loadStdInIntoArray, sumArray } from "../lib/shared.js";

const data = await loadStdInIntoArray();

/** @typedef {"A"|"B"|"C"} Opponent */
/** @typedef {"X"|"Y"|"Z"} Me */

const points = {
  // Rock
  A: 1,
  // Paper
  B: 2,
  // Scissor
  C: 3,
};

/**
 * @param {xyzToLooseWinDraw|xyzToRPS} choose
 * @return {function(string): number}
 */
const evaluateRPS = (choose) => (line) => {
  const [opponent, _me] = /** @type {[Opponent, Me]} */ (line.split(/\s+/));
  const me = choose(_me, opponent);
  const res = points[me];
  if (opponent === me) {
    return 3 + res;
  }
  if (
    (opponent === "A" && me === "B") ||
    (opponent === "B" && me === "C") ||
    (opponent === "C" && me === "A")
  ) {
    return 6 + res;
  }
  return res;
};

/**
 * @param {Me} xyz
 * @return {Opponent}
 */
function xyzToRPS(xyz) {
  if (xyz === "X") {
    return "A";
  }
  if (xyz === "Y") {
    return "B";
  }
  if (xyz === "Z") {
    return "C";
  }
  throw new Error(`Invalid "Me" value: ${xyz}`);
}

/**
 * @param {Me} xyz
 * @param {Opponent} opponent
 * @return {Opponent}
 */
function xyzToLooseWinDraw(xyz, opponent) {
  if (xyz === "Y") {
    return opponent;
  }
  if (opponent === "A") {
    return xyz === "X" ? "C" : "B";
  }
  if (opponent === "B") {
    return xyz === "X" ? "A" : "C";
  }
  if (opponent === "C") {
    return xyz === "X" ? "B" : "A";
  }
  throw new Error(`Invalid "Me" value: ${xyz}`);
}

try {
  console.log(`\tPart 1: ${sumArray(data.map(evaluateRPS(xyzToRPS)))}`);

  console.log(`\tPart 2: ${sumArray(data.map(evaluateRPS(xyzToLooseWinDraw)))}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
