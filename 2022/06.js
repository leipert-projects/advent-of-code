import { exit, loadStdIn } from "../lib/shared.js";
import { findMarkerXOR as findMarker } from "./lib/06.js";

const input = await loadStdIn();

try {
  console.assert(findMarker("mjqjpqmgbljsphdztnvjfqwrcgsmlb") === 7);
  console.assert(findMarker("bvwbjplbgvbhsrlpgdmjqwftvncz") === 5);
  console.assert(findMarker("nppdvjthqldpwncqszvftbrmjlhg") === 6);
  console.assert(findMarker("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg") === 10);
  console.assert(findMarker("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw") === 11);
  const part1 = findMarker(input);
  console.assert(part1 === 1210);
  console.log(`Part 1: ${part1}`);

  console.assert(findMarker("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 14) === 19);
  const part2 = findMarker(input, 14);
  console.assert(part2 === 3476);
  console.log(`Part 2: ${part2}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
