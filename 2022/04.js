import { exit, loadStdInIntoArray, toInteger } from "../lib/shared.js";

const assignments = (await loadStdInIntoArray()).map((line) => {
  const [elfA, elfB] = line.split(",");
  const sectionA = elfA.split("-").map(toInteger);
  const sectionB = elfB.split("-").map(toInteger);
  if (sectionA[0] < sectionB[0]) {
    return [sectionA, sectionB];
  }
  return [sectionB, sectionA];
});

function detectCompleteOverlaps([sectionA, sectionB]) {
  return !(sectionA[0] < sectionB[0] && sectionA[1] < sectionB[1]);
}

function detectPartialOverlap([sectionA, sectionB]) {
  return sectionA[1] >= sectionB[0];
}

try {
  const part1 = assignments.filter(detectCompleteOverlaps);

  console.log(`\tPart 1: ${part1.length}`);

  const part2 = assignments.filter(detectPartialOverlap);

  console.log(`\tPart 2: ${part2.length}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
