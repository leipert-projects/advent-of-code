import { copyObject, exit, loadStdIn, toInteger } from "../lib/shared.js";

const input = (await loadStdIn()).trimEnd().split("\n").reduce((acc, curr) => {
  if (curr.startsWith("move")) {
    acc.moves ||= [];
    acc.moves.push([...curr.matchAll(/\d+/g)].map(toInteger));
  } else if (curr.includes("[")) {
    const stacks = curr.split(/(....|...$)/);
    for (let i = 0; i < stacks.length; i += 1) {
      if (stacks[i].trim()) {
        const idx = (i - 1) / 2;
        acc.stacks ||= [];
        acc.stacks[idx] ||= [];
        acc.stacks[idx].push(stacks[i].trim().replace(/[[\]]/g, ""));
      }
    }
  }
  return acc;
}, {});

const crateMover9000 = ({ stacks, moves }) => {
  for (const [amount, from, to] of moves) {
    for (let i = 0; i < amount; i += 1) {
      stacks[to - 1].unshift(stacks[from - 1].shift());
    }
  }
  return stacks;
};

const crateMover9001 = ({ stacks, moves }) => {
  for (const [amount, from, to] of moves) {
    stacks[to - 1].unshift(...stacks[from - 1].splice(0, amount));
  }
  return stacks;
};

try {
  const part1 = crateMover9000(copyObject(input)).flatMap((x) => x[0]).join("");

  console.log(`\tPart 1: ${part1}`);

  const part2 = crateMover9001(copyObject(input)).flatMap((x) => x[0]).join("");

  console.log(`\tPart 1: ${part2}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
