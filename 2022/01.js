import { exit, loadStdInIntoChunks, sumArray, toInteger } from "../lib/shared.js";

const data = await loadStdInIntoChunks(toInteger);

try {
  const elves = data.map(sumArray).sort((a, b) => b - a);
  console.log(`\tPart 1: ${elves[0]}`);

  console.log(`\tPart 2: ${elves[0] + elves[1] + elves[2]}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
