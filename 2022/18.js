import { exit, loadStdIn, toInteger } from "../lib/shared.js";
import MapND from "../lib/MapND.js";

const stdIn = (await loadStdIn()).split(/\s+/);

function getSurfaceArea(map) {
  const soloSides = [];
  const sideDiffs = [
    [-1, 0, 0],
    [+1, 0, 0],
    [0, -1, 0],
    [0, +1, 0],
    [0, 0, -1],
    [0, 0, +1],
  ];
  for (const chunk of map) {
    const [x, y, z] = chunk.coordinates;
    for (const [x1, y1, z1] of sideDiffs) {
      const side = [x + x1, y + y1, z + z1];
      if (!map.get(...side)) {
        soloSides.push(side);
      }
    }
  }

  return soloSides;
}
function part2Fn(map, soloSides) {
  const stack = [map.minCoordinates];
  for (const x of soloSides) {
    map.set("A", ...x);
  }
  while (stack.length) {
    const coordinates = stack.pop();
    if (map.outOfBounds(...coordinates)) {
      continue;
    }
    const curr = map.get(...coordinates);
    if (curr !== "S" && curr !== "W") {
      map.set("W", ...coordinates);
      const [x, y, z] = coordinates;
      stack.push(
        [x + 1, y, z],
        [x - 1, y, z],
        [x, y + 1, z],
        [x, y - 1, z],
        [x, y, z + 1],
        [x, y, z - 1],
      );
    }
  }

  return soloSides.filter((coordinates) => map.get(...coordinates) !== "A");
}

try {
  const map = new MapND(3);
  map.minCoordinates = [Number.MAX_SAFE_INTEGER, Number.MAX_SAFE_INTEGER, Number.MAX_SAFE_INTEGER];

  stdIn.forEach((chunk) => {
    map.set("S", ...chunk.split(",").map(toInteger));
  });
  map.minCoordinates = map.minCoordinates.map((i) => i - 1);
  map.maxCoordinates = map.maxCoordinates.map((i) => i + 1);

  const part1 = getSurfaceArea(map);

  console.log(`\tPart 1: The total surface area is ${part1.length}`);

  const part2 = part2Fn(map, part1);

  console.log(`\tPart 2: The outer surface area is ${part2.length}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
