import { exit, loadStdInIntoArray, toInteger } from "../lib/shared.js";
import Map2D from "../lib/Map2D.js";

const input = await loadStdInIntoArray();

class SandMap extends Map2D {
  #boundary = null;

  get(x, y) {
    if (this.#boundary === y) {
      return "#";
    }
    return super.get(x, y);
  }

  enableBoundaryAt(y) {
    this.#boundary = y;
  }
}

function toPoint(p) {
  const [x, y] = p.split(",");
  return [toInteger(x), toInteger(y)];
}

function loadRocks(map, data) {
  for (const path of data) {
    const points = path.split(/\s+->\s+/);
    let from = toPoint(points.shift());

    while (points.length) {
      const to = toPoint(points.shift());
      const axis = from[0] === to[0] ? 1 : 0;
      const dir = Math.sign(to[axis] - from[axis]);
      for (from[axis]; from[axis] !== to[axis]; from[axis] += dir) {
        map.set(...from, "#");
      }
      map.set(...to, "#");
      from = to;
    }
  }
}

function trickleSand(map, entry) {
  const { maxY } = map;
  let [x, y] = entry;
  while (y < maxY && map.get(...entry) !== "o") {
    y += 1;
    if (map.get(x, y)) {
      if (!map.get(x - 1, y)) {
        x -= 1;
      } else if (!map.get(x + 1, y)) {
        x += 1;
      } else {
        map.set(x, y - 1, "o");
        [x, y] = entry;
      }
    }
  }
}

try {
  const map = new SandMap();
  map.minX = Number.POSITIVE_INFINITY;
  map.minY = Number.POSITIVE_INFINITY;
  loadRocks(map, input);
  trickleSand(map, [500, 0]);

  if (input.length < 10) {
    console.log(map.render((x) => x ?? ".").join("\n"));
  }
  console.log(`Part 1: ${map.stats.get("o")}`);

  map.maxY += 2;
  map.enableBoundaryAt(map.maxY);
  trickleSand(map, [500, 0]);

  if (input.length < 10) {
    console.log(map.render((x) => x ?? ".").join("\n"));
  }
  console.log(`Part 2: ${map.stats.get("o")}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
