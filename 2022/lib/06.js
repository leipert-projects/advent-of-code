import { bitCount } from "../../lib/math.js";

export const findMarkerLoop = (str, markerLength = 4) => {
  for (let i = 0; i < str.length - markerLength - 1; i += 1) {
    for (let j = markerLength - 1; j > -1; j -= 1) {
      const idx = j + i;
      const lastIndex = str.indexOf(str[idx], i);
      if (lastIndex !== idx) {
        j = -1;
        i = lastIndex;
      } else if (j === 0) {
        return i + markerLength;
      }
    }
  }
  return -1;
};

export function findMarkerSlice(str, markerLength = 4) {
  const arr = str.split("");
  for (let i = 0; i < arr.length - markerLength - 1; i += 1) {
    const substr = arr.slice(i, i + markerLength);
    if (substr.every((c, idx) => substr.lastIndexOf(c) === idx)) {
      return i + markerLength;
    }
  }
  return -1;
}

// Thanks to: https://www.mattkeeter.com/blog/2022-12-10-xor/
export function findMarkerXOR(str, markerLength = 4) {
  let set = 0;
  for (let i = 0; i < str.length; i += 1) {
    // eslint-disable-next-line no-bitwise
    set ^= 1 << (str.charCodeAt(i) - 97);

    if (i >= markerLength) {
      // eslint-disable-next-line no-bitwise
      set ^= 1 << (str.charCodeAt(i - markerLength) - 97);
    }

    if (bitCount(set) === markerLength) {
      return i + 1;
    }
  }
  return -1;
}
