import { exit, loadStdInIntoArray, toInteger } from "../lib/shared.js";
import Map2D from "../lib/Map2D.js";

const input = await loadStdInIntoArray();

const VISITED = Symbol("VISITED");

const runInstructions = (instructions, knotCount) => {
  const map = new Map2D();
  const tail = [0, 0];
  const knots = Array(knotCount - 1).fill(0).map(() => [0, 0]);
  knots.push(tail);
  const def = {
    R: [0, 1],
    L: [0, -1],
    U: [1, 1],
    D: [1, -1],
  };
  map.set(...tail, VISITED);

  for (const line of instructions) {
    const [arg1, arg2] = line.split(" ");
    const [axis, direction] = def[arg1];
    const distance = toInteger(arg2);
    for (let i = 0; i < distance; i += 1) {
      knots[0][axis] += direction;

      for (let j = 1; j < knotCount; j += 1) {
        const diffX = knots[j - 1][0] - knots[j][0];
        const diffY = knots[j - 1][1] - knots[j][1];
        if (Math.abs(diffX) > 1) {
          knots[j][0] += Math.sign(diffX);
          if (Math.abs(diffY) >= 1) {
            knots[j][1] += Math.sign(diffY);
          }
        } else if (Math.abs(diffY) > 1) {
          knots[j][1] += Math.sign(diffY);
          if (Math.abs(diffX) >= 1) {
            knots[j][0] += Math.sign(diffX);
          }
        }
      }

      map.set(...tail, VISITED);
    }
  }

  return map;
};

try {
  console.log(`Part 1: ${runInstructions(input, 2).stats.get(VISITED)}`);
  console.log(`Part 2: ${runInstructions(input, 10).stats.get(VISITED)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
