import { exit, loadStdInIntoChunks } from "../lib/shared.js";

const data = await loadStdInIntoChunks(JSON.parse);

const UNDECIDED = Symbol("undecided");

const inOrder = (a, b) => {
  if (Number.isInteger(a)) {
    if (Number.isInteger(b)) {
      if (a !== b) {
        /*
        If the left integer is lower than the right integer,
        the inputs are in the right order.
        If the left integer is higher than the right integer,
        the inputs are not in the right order.
        */
        return a < b;
      }
      /*
      Otherwise, the inputs are the same integer;
      continue checking the next part of the input.
      */
      return UNDECIDED;
    }
    /*
    If exactly one value is an integer, convert the integer to a list
    which contains that integer as its only value, then retry the comparison
     */
    // eslint-disable-next-line no-param-reassign
    a = [a];
  } else if (Number.isInteger(b)) {
    // eslint-disable-next-line no-param-reassign
    b = [b];
  }

  for (let i = 0; i < a.length; i += 1) {
    /*
    If the right list runs out of items first,
    the inputs are not in the right order.
    */
    if (i >= b.length) {
      return false;
    }
    const res = inOrder(a[i], b[i]);
    if (res !== UNDECIDED) {
      return res;
    }
  }
  /*
  If the left list runs out of items first,
  the inputs are in the right order.
   */
  if (a.length < b.length) {
    return true;
  }
  return UNDECIDED;
};

const customSort = (a, b) => inOrder(a, b) ? -1 : 1;

try {
  const part1 = data.reduce((acc, curr, idx) => acc + (inOrder(...curr) ? idx + 1 : 0), 0);
  console.log(`Part 1: ${part1}`);

  const dividerPackets = [[[2]], [[6]]];

  dividerPackets[0].$isDivider = true;
  dividerPackets[1].$isDivider = true;

  const part2 = data.flatMap((x) => x).concat(dividerPackets).sort(customSort);
  const firstIndex = part2.findIndex((x) => x.$isDivider) + 1;
  const secondIndex = part2.findLastIndex((x) => x.$isDivider) + 1;
  console.log(`Part 2: ${firstIndex * secondIndex}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
