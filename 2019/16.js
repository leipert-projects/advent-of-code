import { Logger } from "./lib/Logger.js";
import { loadStdIn } from "../lib/shared.js";

import { getSignal, runPhases } from "./lib/16_ffa.js";

const input = (await loadStdIn()).trim();

const logger = new Logger(import.meta.url);

logger.writeResults(runPhases(input, 100).substring(0, 8), getSignal(input));
