import { Logger } from "./lib/Logger.js";
import { System } from "./lib/12_moons.js";

const jupiterMoons = [
  [14, 4, 5],
  [12, 10, 8],
  [1, 7, -10],
  [16, -5, 3],
];

globalThis.logger = new Logger(import.meta.url);

const system = new System(jupiterMoons);

system.iterate(1000);

const part1 = system.totalEnergy();

const part2 = system.iterateUntilInitialState();

globalThis.logger.writeResults(part1, part2);
