import { destroyAsteroids, findStation } from "./lib/10_asteriods.js";

import { Logger } from "./lib/Logger.js";

import { loadStdIn } from "../lib/shared.js";

const data = (await loadStdIn()).trim().split(/\n/);

globalThis.logger = new Logger(import.meta.url);

const bestSpot = findStation(data);

const destroyed = destroyAsteroids(bestSpot);

const results = destroyed[199][0] * 100 + destroyed[199][1];

globalThis.logger.writeResults(bestSpot, results);
