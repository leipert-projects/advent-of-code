import { countOrbits, minimalTransfers } from "./lib/06_orbit_map.js";
import { loadStdInIntoArray } from "../lib/shared.js";

const map = await loadStdInIntoArray();

console.warn("Part 1:", countOrbits(map));
console.warn("Part 2:", minimalTransfers(map));
