import { Logger } from "./lib/Logger.js";
import { loadStdInIntoIntegerArray } from "../lib/shared.js";
import { deployDrone, findShipSpot } from "./lib/19_tractor_beam.js";

const program = await loadStdInIntoIntegerArray();

globalThis.logger = new Logger(import.meta.url);

const res = deployDrone(program);
const res2 = findShipSpot(program);

globalThis.logger.writeResults(res.stats.get(1), res2);
