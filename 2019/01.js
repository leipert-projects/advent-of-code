import {
  calculateFuelConsumption,
  calculateFuelConsumptionRecursive,
  sumFuelConsumption,
} from "./lib/01_fuel.js";

import { exit, loadStdInIntoArray, toInteger } from "../lib/shared.js";

const input = (await loadStdInIntoArray()).map(toInteger);

try {
  console.log(`Part 1: ${sumFuelConsumption(input, calculateFuelConsumption)}`);
  console.log(`Part 2: ${sumFuelConsumption(input, calculateFuelConsumptionRecursive)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
