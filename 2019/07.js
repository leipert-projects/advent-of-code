import { loadStdInIntoIntegerArray } from "../lib/shared.js";
import { runAmplifierWithLoop } from "./lib/07_amplifier.js";
import { permutate } from "../lib/sets.js";

const program = await loadStdInIntoIntegerArray();

const maxThrust = Math.max(
  ...permutate([0, 1, 2, 3, 4]).map((config) => runAmplifierWithLoop(program, config)),
);

console.warn("Part 1: ", maxThrust);

const maxThrust2 = Math.max(
  ...permutate([5, 6, 7, 8, 9]).map((config) => runAmplifierWithLoop(program, config)),
);

console.warn("Part 2: ", maxThrust2);
