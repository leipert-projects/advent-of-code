import { Logger } from "./lib/Logger.js";
import { loadStdInIntoIntegerArray } from "../lib/shared.js";

import { vacuumRobot } from "./lib/17_vacuum.js";

const program = await loadStdInIntoIntegerArray();

const logger = new Logger(import.meta.url);

const res = vacuumRobot(program);

logger.writeResults(res.calibration);
