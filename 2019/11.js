import { loadStdInIntoIntegerArray } from "../lib/shared.js";
import { Logger } from "./lib/Logger.js";
import { hullPaintingRobot, printRegistration } from "./lib/11_hull_painting_robot.js";

const program = await loadStdInIntoIntegerArray();

const logger = new Logger(import.meta.url);

const part1 = hullPaintingRobot(program).map.size;

logger.logArray(printRegistration(hullPaintingRobot(program, 1)));

logger.writeResults(part1);
