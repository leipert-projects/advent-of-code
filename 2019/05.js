import { loadStdInIntoIntegerArray } from "../lib/shared.js";
import { runOpCode } from "./lib/02_opcode.js";

const program = await loadStdInIntoIntegerArray();

console.warn("Part 1:", runOpCode(program, { input: 1 }).output);
console.warn("Part 2:", runOpCode(program, { input: 5 }).output);
