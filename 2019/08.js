import { loadStdIn } from "../lib/shared.js";

const data = (await loadStdIn()).trim();

const createLayer = (layer) => {
  const counts = layer
    .split("")
    .sort()
    .reduce((acc, s) => {
      acc[s] = !acc[s] ? 1 : acc[s] + 1;
      return acc;
    }, {});

  return { ...counts, layer };
};

const toLayers = (image, width, height) => {
  const splitter = new RegExp(`.{${width * height}}`, "g");

  return image.match(splitter).map(createLayer);
};

const decode = (layers, width, height) => {
  const splitter = new RegExp(`.{${width}}`, "g");

  return layers
    .reduce((image, layer) => {
      const top = image.split("");
      const bottom = layer.layer.split("");

      for (let i = 0; i < top.length; i += 1) {
        if (top[i] === "2") {
          top[i] = bottom[i];
        }
      }

      return top.join("");
    }, "".padStart(width * height, "2"))
    .replace(/1/g, "░")
    .replace(/0/g, "▓")
    .match(splitter)
    .join("\n");
};

const width = 25;
const height = 6;

const layers = toLayers(data, width, height);

let min = { 0: Number.MAX_SAFE_INTEGER };

layers.forEach((layer) => {
  if (layer["0"] < min["0"]) {
    min = layer;
  }
});

console.log(`Part 1: ${min["1"] * min["2"]}`);

const compiledImage = decode(layers, width, height);

console.log(`Part 2: \n${compiledImage}`);
