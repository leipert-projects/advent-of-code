import { runOpCode } from "./lib/02_opcode.js";

import { loadStdInIntoIntegerArray } from "../lib/shared.js";

const input = await loadStdInIntoIntegerArray();

console.log(`Part 1: Program 1202: ${runOpCode(input, { noun: 12, verb: 2 }).lastState[0]}`);

function search(output) {
  for (let noun = 0; noun < 100; noun += 1) {
    for (let verb = 0; verb < 100; verb += 1) {
      try {
        const res = runOpCode(input, { noun, verb });

        if (res.lastState[0] === output) {
          console.log(`${noun}:${verb} => ${100 * noun + verb} => ${res.lastState[0]}`);
          return;
        }
      } catch {
        // Nothing
      }
    }
  }
}

console.log("Part 2:");

search(19690720);
