import { Logger } from "./lib/Logger.js";
import { loadStdInIntoIntegerArray } from "../lib/shared.js";

import { arcadeCabinet } from "./lib/13_arcade.js";

const program = await loadStdInIntoIntegerArray();

// Inserting all the coins!
program[0] = 2;

globalThis.logger = new Logger(import.meta.url);

const { stats, score } = arcadeCabinet(program);
globalThis.logger.writeResults(stats.get(2), score);
