import { assertEquals } from "testing/asserts.ts";

import { countOrbits, minimalTransfers } from "./06_orbit_map.js";

const map = ["COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H", "D)I", "E)J", "J)K", "K)L"];

Deno.test("countOrbits", () => {
  assertEquals(countOrbits(map), 42);
});

const map2 = [...map, "K)YOU", "I)SAN"];

Deno.test("minimalTransfers", () => {
  assertEquals(minimalTransfers(map2), 4);
});
