import { assertEquals } from "testing/asserts.ts";
import { runOpCode } from "./02_opcode.js";

[
  [
    [1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50],
    [3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50],
  ],
  [
    [1, 0, 0, 0, 99],
    [2, 0, 0, 0, 99],
  ],
  [
    [2, 4, 4, 5, 99, 0],
    [2, 4, 4, 5, 99, 9801],
  ],
  [
    [1, 1, 1, 4, 99, 5, 6, 0, 99],
    [30, 1, 1, 4, 2, 5, 6, 0, 99],
  ],
].forEach(([a, expected]) => {
  Deno.test(`runOpcode([${a}]).lastState => ${expected}`, () =>
    assertEquals(runOpCode(a).lastState, expected));
});

[
  [
    [1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50],
    [100, 1, 1, 2, 2, 3, 11, 0, 99, 30, 40, 50],
  ],
  [
    [1, 0, 0, 0, 99],
    [2, 1, 1, 0, 99],
  ],
  [
    [2, 4, 4, 5, 99, 0],
    [2, 1, 1, 5, 99, 1],
  ],
  [
    [1, 1, 1, 4, 99, 5, 6, 0, 99],
    [30, 1, 1, 4, 2, 5, 6, 0, 99],
  ],
].forEach(([a, expected]) => {
  Deno.test(`runOpcode([${a}],{noun: 1, verb: 1}).lastState => ${expected}`, () =>
    assertEquals(runOpCode(a, { noun: 1, verb: 1 }).lastState, expected));
});
