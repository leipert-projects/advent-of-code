import { assertEquals } from "testing/asserts.ts";

import { destroyAsteroids, findStation, getAngle } from "./10_asteriods.js";

const largeMap = [
  ".#..##.###...#######",
  "##.############..##.",
  ".#.######.########.#",
  ".###.#######.####.#.",
  "#####.##.#.##.###.##",
  "..#####..#.#########",
  "####################",
  "#.####....###.#.#.##",
  "##.#################",
  "#####.##.###..####..",
  "..######..##.#######",
  "####.##.####...##..#",
  ".#####..#.######.###",
  "##...#.##########...",
  "#.##########.#######",
  ".####.#.###.###.#.##",
  "....##.##.###..#####",
  ".#.#.###########.###",
  "#.#.#.#####.####.###",
  "###.##.####.##.#..##",
];

[
  [[0, -1], 0],
  [[1 / Math.sqrt(3), -1], 30],
  [[1, -1], 45],
  [[1, -1 / Math.sqrt(3)], 60],
  [[1, 0], 90],
  [[1, 1 / Math.sqrt(3)], 120],
  [[1, 1], 135],
  [[1 / Math.sqrt(3), 1], 150],
  [[0, 1], 180],
  [[-1 / Math.sqrt(3), 1], 210],
  [[-1, 1], 225],
  [[-1, 1 / Math.sqrt(3)], 240],
  [[-1, 0], 270],
  [[-1, -1 / Math.sqrt(3)], 300],
  [[-1, -1], 315],
  [[-1 / Math.sqrt(3), -1], 330],
  [[0, -1], 0],
].forEach(([point, degree]) => {
  Deno.test(`getAngle([${point}], [0,0]) => ${degree}`, () => {
    assertEquals(Math.round(getAngle(point, [0, 0])), degree);
  });
});
Deno.test("analyzeMap: Map 1", () => {
  const map = [
    ".#..#",
    ".....",
    "#####",
    "....#",
    "...##",
  ];
  assertEquals(findStation(map).coordinates, [3, 4]);
  assertEquals(findStation(map).observable, 8);
});
Deno.test("analyzeMap: Map 2", () => {
  // prettier-ignore
  const map = [
    "......#.#.",
    "#..#.#....",
    "..#######.",
    ".#.#.###..",
    ".#..#.....",
    "..#....#.#",
    "#..#....#.",
    ".##.#..###",
    "##...#..#.",
    ".#....####",
  ];
  assertEquals(findStation(map).coordinates, [5, 8]);
  assertEquals(findStation(map).observable, 33);
});

Deno.test("analyzeMap: Map 3", () => {
  const map = [
    "#.#...#.#.",
    ".###....#.",
    ".#....#...",
    "##.#.#.#.#",
    "....#.#.#.",
    ".##..###.#",
    "..#...##..",
    "..##....##",
    "......#...",
    ".####.###.",
  ];
  assertEquals(findStation(map).coordinates, [1, 2]);
  assertEquals(findStation(map).observable, 35);
});

Deno.test("analyzeMap: Map 4", () => {
  const map = [
    ".#..#..###",
    "####.###.#",
    "....###.#.",
    "..###.##.#",
    "##.##.#.#.",
    "....###..#",
    "..#.#..#.#",
    "#..#.#.###",
    ".##...##.#",
    ".....#.#..",
  ];
  assertEquals(findStation(map).coordinates, [6, 3]);
  assertEquals(findStation(map).observable, 41);
});

Deno.test("analyzeMap: Map 5", () => {
  assertEquals(findStation(largeMap).coordinates, [11, 13]);
  assertEquals(findStation(largeMap).observable, 210);
});

Deno.test("analyzeMap: Map 6", () => {
  const map = [
    ".#....#####...#..",
    "##...##.#####..##",
    "##...#...#.#####.",
    "..#.....X...###..",
    "..#.#.....#....##",
  ];
  assertEquals(findStation(map).coordinates, [8, 3]);
  assertEquals(findStation(map).observable, 30);
});

Deno.test("Map 5: Destroy asteroids", () => {
  const station = findStation(largeMap);

  const destroyed = destroyAsteroids(station, 250);

  assertEquals(destroyed[0], [11, 12]);
  assertEquals(destroyed[1], [12, 1]);
  assertEquals(destroyed[2], [12, 2]);
  assertEquals(destroyed[9], [12, 8]);
  assertEquals(destroyed[19], [16, 0]);
  assertEquals(destroyed[49], [16, 9]);
  assertEquals(destroyed[99], [10, 16]);
  assertEquals(destroyed[198], [9, 6]);
  assertEquals(destroyed[199], [8, 2]);
  assertEquals(destroyed[200], [10, 9]);
  assertEquals(destroyed[298], [11, 1]);
});

Deno.test("Map 6: Destroy asteroids", () => {
  const map = [
    ".#....#####...#..",
    "##...##.#####..##",
    "##...#...#.#####.",
    "..#.....X...###..",
    "..#.#.....#....##",
  ];

  const station = findStation(map);

  assertEquals(
    destroyAsteroids(station).slice(0, 20),
    // deno-fmt-ignore
    [
      [8, 1], [9, 0], [9, 1],
      [10, 0], [9, 2], [11, 1],
      [12, 1], [11, 2], [15, 1],
      [12, 2], [13, 2], [14, 2],
      [15, 2], [12, 3], [16, 4],
      [15, 4], [10, 4] ,[4, 4],
      [2,4], [2,3]
    ],
  );
});
