import { Program } from "./02_opcode.js";

export const runAmplifierWithLoop = (program, config) => {
  let signal = 0;

  const amplifiers = [];

  config.forEach((setting, index) => {
    amplifiers[index] = new Program(program);
    amplifiers[index].setInput(setting);
  });

  let status = null;

  do {
    for (let i = 0; i < config.length; i += 1) {
      amplifiers[i].setInput(signal);

      amplifiers[i].run();

      const res = amplifiers[i].serialize();

      // eslint-disable-next-line prefer-destructuring
      signal = [...res.output].reverse()[0];

      status = res.status;
    }
  } while (status !== "TERMINATED");

  return signal;
};
