export const getBOM = (formulas, acc = { FUEL: -1 }) => {
  let replaced;

  const replace = (ingredients, [result, amount]) => {
    const times = Math.abs(Math.floor((acc[result] || 0) / amount));

    if (times > 0) {
      ingredients.forEach(([name, count]) => {
        acc[name] = (acc[name] || 0) - times * count;
      });

      acc[result] += times * amount;

      replaced = true;
    }
  };

  do {
    replaced = false;

    formulas.forEach(replace);
  } while (replaced);
  return acc;
};

const match = /(\d+) ([A-Z]+)/gi;

export const parseFormulas = (input) => {
  const formulas = new Map();
  input
    .trim()
    .split("\n")
    .forEach((line) => {
      const [res, ...rest] = [...line.matchAll(match)]
        .map(([, amount, name]) => [name, parseInt(amount, 10)])
        .reverse();

      formulas.set(res, rest);
    });

  return formulas;
};

export const getMaxFuel = (formulas) => {
  let FUEL = 1;

  let account = { FUEL: -1 };
  account = getBOM(formulas, account);

  const oreNeed = -account.ORE;

  account.ORE += 10 ** 12; // Adding one trillion ORE
  let nextProduction;
  do {
    nextProduction = Math.floor(account.ORE / oreNeed);
    FUEL += nextProduction;
    account.FUEL = -nextProduction;
    account = getBOM(formulas, account);
  } while (nextProduction >= 1);

  return FUEL;
};
