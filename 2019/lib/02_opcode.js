const RUNNING = "RUNNING";
const SET_UP = "SET_UP";
const TERMINATED = "TERMINATED";
const WAITING_FOR_INPUT = "WAITING_FOR_INPUT";

export class Program {
  constructor(program) {
    this.pointer = 0;
    this.opCodeCache = {};
    this.memory = [...program];
    this.output = [];
    this.input = [];
    this.status = SET_UP;
    this.relativePointer = 0;
  }

  runOpCode(opCode) {
    if (this.opCodeCache[opCode]) {
      return this.opCodeCache[opCode]();
    }

    const command = opCode.substr(-2);

    let paramModes = opCode.substr(0, 3).split("").reverse();

    let fn = null;

    switch (command) {
      case "01":
        paramModes[2] = `O${paramModes[2]}`;
        fn = (param1, param2, param3) => {
          this.memory[param3] = param1 + param2;
          this.pointer += 4;
        };
        break;
      case "02":
        paramModes[2] = `O${paramModes[2]}`;
        fn = (param1, param2, param3) => {
          this.memory[param3] = param1 * param2;
          this.pointer += 4;
        };
        break;
      case "03":
        paramModes = [`O${paramModes[0]}`];
        fn = (param1) => {
          if (this.input.length > 0) {
            this.memory[param1] = this.input.shift();
            this.pointer += 2;
          } else {
            this.status = WAITING_FOR_INPUT;
          }
        };
        break;
      case "04":
        fn = (param1) => {
          this.output.push(param1);
          this.pointer += 2;
        };
        break;
      case "05":
        fn = (param1, param2) => {
          if (param1 !== 0) {
            this.pointer = param2;
          } else {
            this.pointer += 3;
          }
        };
        break;
      case "06":
        fn = (param1, param2) => {
          if (param1 === 0) {
            this.pointer = param2;
          } else {
            this.pointer += 3;
          }
        };
        break;
      case "07":
        paramModes[2] = `O${paramModes[2]}`;
        fn = (param1, param2, param3) => {
          this.memory[param3] = param1 < param2 ? 1 : 0;
          this.pointer += 4;
        };
        break;
      case "08":
        paramModes[2] = `O${paramModes[2]}`;
        fn = (param1, param2, param3) => {
          this.memory[param3] = param1 === param2 ? 1 : 0;
          this.pointer += 4;
        };
        break;
      case "09":
        fn = (param1) => {
          this.relativePointer += param1;
          this.pointer += 2;
        };
        break;
      default:
        throw Error(`Unknown opcode ${opCode}`);
    }

    this.opCodeCache[opCode] = () => {
      const args = paramModes.map((paramMode, index) => {
        let pos = this.memory[this.pointer + index + 1] || 0;
        if (["1", "O1", "O0"].includes(paramMode)) {
          return pos;
        }
        if (paramMode === "O2") {
          return this.relativePointer + pos;
        }
        if (paramMode === "2") {
          pos = this.relativePointer + pos;
        }
        return this.memory[pos] || 0;
      });

      fn(...args);
    };

    return this.opCodeCache[opCode]();
  }

  run() {
    this.status = RUNNING;

    do {
      const opCode = `${this.memory[this.pointer]}`.padStart(5, "0");
      if (opCode.substr(-2) === "99") {
        this.status = TERMINATED;
        break;
      }

      this.runOpCode(opCode);
    } while (this.pointer < this.memory.length && this.status === RUNNING);
  }

  serialize() {
    return {
      status: this.status,
      lastState: this.memory,
      output: this.output,
      input: this.input,
    };
  }

  setInput(...inputs) {
    this.input = [...this.input, ...inputs];
  }

  clearOutput() {
    return this.output.splice(0, this.output.length);
  }
}

export function runOpCode(code, options = {}) {
  const program = new Program(code, options);

  const { noun = null, verb = null } = options;
  let { input = [] } = options;

  if (Number.isInteger(noun)) {
    program.memory[1] = noun;
  }
  if (Number.isInteger(verb)) {
    program.memory[2] = verb;
  }

  if (!Array.isArray(input)) {
    input = [input];
  }

  program.setInput(...input);

  program.run();

  return program.serialize();
}
