import { Program } from "./02_opcode.js";
import Map2D from "../../lib/Map2D.js";

export const printRegistration = (map) => {
  const res = map.render((val) => (val === 1 ? "░" : "▓")).reverse();

  res.unshift("Registration:");

  return res;
};

export const hullPaintingRobot = (program, initialColor = 0) => {
  const coordinates = [0, 0];

  const map = new Map2D();

  let currDirection = 0;

  const robot = new Program(program);

  map.set(0, 0, initialColor);

  do {
    const currentColor = map.get(...coordinates) || 0;

    robot.setInput(currentColor);
    robot.run();

    const [newColor, angle] = robot.output.slice(-2);

    map.set(...coordinates, newColor);

    currDirection = (currDirection + 4 + (angle === 0 ? -1 : 1)) % 4;

    if (currDirection === 0) {
      coordinates[1] += 1;
    } else if (currDirection === 1) {
      coordinates[0] += 1;
    } else if (currDirection === 2) {
      coordinates[1] -= 1;
    } else if (currDirection === 3) {
      coordinates[0] -= 1;
    }
  } while (robot.status !== "TERMINATED");

  map.maxY += 2;
  map.minY -= 1;

  return map;
};

// module.exports = { hullPaintingRobot, printRegistration };
