import { assertEquals } from "testing/asserts.ts";
import { dirname, loadIntegerIntoArray } from "../../lib/shared.js";

import {
  calculateFuelConsumption,
  calculateFuelConsumptionRecursive,
  sumFuelConsumption,
} from "./01_fuel.js";

[
  [12, 2],
  [14, 2],
  [1969, 654],
  [100756, 33583],
].forEach(([a, expected]) => {
  Deno.test({
    name: `calculateFuelConsumption(${a}) => ${expected}`,
    fn() {
      assertEquals(calculateFuelConsumption(a), expected);
    },
  });
});

[
  [12, 2],
  [14, 2],
  [1969, 966],
  [100756, 50346],
].forEach(([a, expected]) => {
  Deno.test({
    name: `calculateFuelConsumptionRecursive(${a}) => ${expected}`,
    fn() {
      assertEquals(calculateFuelConsumptionRecursive(a), expected);
    },
  });
});

Deno.test({
  name: `sumFuelConsumption`,
  async fn() {
    const inputs = await loadIntegerIntoArray(`${dirname(import.meta.url)}/../data/01.txt`);
    assertEquals(sumFuelConsumption(inputs, calculateFuelConsumption), 3256599);
    assertEquals(sumFuelConsumption(inputs, calculateFuelConsumptionRecursive), 4882038);
  },
});
