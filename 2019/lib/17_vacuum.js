import Map2D from "../../lib/Map2D.js";
import { Program } from "./02_opcode.js";

const NORTH = 1;
const SOUTH = 2;
const WEST = 3;
const EAST = 4;

const DIRECTIONS = [NORTH, EAST, SOUTH, WEST];

const getNextPos = (pos, dir) => {
  const nextPos = [...pos];

  if (dir === NORTH) {
    nextPos[1] -= 1;
  } else if (dir === SOUTH) {
    nextPos[1] += 1;
  } else if (dir === WEST) {
    nextPos[0] -= 1;
  } else if (dir === EAST) {
    nextPos[0] += 1;
  }

  return nextPos;
};

export const vacuumRobot = (program) => {
  const robot = new Program(program);
  const map = new Map2D();

  robot.run();

  const codes = robot.clearOutput();
  const pos = [0, 0];

  codes.forEach((code) => {
    if (code === 10) {
      pos[0] = 0;
      pos[1] += 1;
    } else {
      map.set(...pos, code);

      pos[0] += 1;
    }
  });

  let calibration = 0;

  map.forEach(({ value }, key) => {
    if (value === 35) {
      const pos2 = key.split(",").map((x) => parseInt(x, 10));
      const allScaffold = DIRECTIONS.every((dir) => map.get(...getNextPos(pos2, dir)) === 35);
      if (allScaffold) {
        calibration += pos2[0] * pos2[1];
      }
    }
  });

  return {
    calibration,
  };
};
