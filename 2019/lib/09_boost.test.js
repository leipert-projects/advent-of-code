import { assertEquals } from "testing/asserts.ts";

import { runOpCode } from "./02_opcode.js";

Deno.test("Self Output", () => {
  // deno-fmt-ignore
  const program = [
        109, 1, 204, -1, 1001, 100, 1, 100,
        1008, 100, 16, 101, 1006, 101, 0, 99
      ];

  assertEquals(runOpCode(program).output, program);
});

Deno.test("16-digit number", () => {
  // deno-fmt-ignore
  const program = [
        1102,34915192,34915192,7,4,7,99,0
      ];

  const result = runOpCode(program).output[0];

  assertEquals(`${result}`.length, 16);
  assertEquals(result, 1219070632396864);
});

Deno.test("Big Number", () => {
  const number = 1125899906842624;

  // deno-fmt-ignore
  const program = [
        104,number,99
      ];

  const result = runOpCode(program).output[0];

  assertEquals(result, number);
});
