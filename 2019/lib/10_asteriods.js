import Map2D from "../../lib/Map2D.js";

export const getAngle = ([x2, y2], [x1, y1]) => {
  const y = y2 - y1;
  const x = x2 - x1;

  const angle = (Math.atan2(y, x) * 180) / Math.PI;

  return (angle + 360 + 90) % 360;
};

const getDistance = ([x1, y1], [x2, y2]) => {
  const y = y2 - y1;
  const x = x2 - x1;

  return Math.sqrt(y ** 2 + x ** 2);
};

export const findStation = (map) => {
  let bestStation = { observable: 0 };

  const map2 = new Map2D();

  map.forEach((row, y) => {
    row.split("").forEach((ast, x) => {
      if (ast !== ".") {
        map2.set(x, y, [x, y]);
      }
    });
  });

  if (globalThis.logger) {
    globalThis.logger.logArray(map2.render());
  }

  map2.forEach((ast1, key1) => {
    const angles = new Set();

    const asteroidMap = map2.reduce((acc, ast2, key2) => {
      if (key1 === key2) {
        return acc;
      }

      const angle = getAngle(ast2.value, ast1.value);

      angles.add(angle);

      acc.push({
        coordinates: ast2.value,
        angle,
        distance: getDistance(ast1.value, ast2.value),
      });

      return acc;
    }, []);

    const stationCandidate = {
      coordinates: ast1.value,
      asteroidMap: asteroidMap.sort((a, b) => {
        const diff = a.angle - b.angle;
        if (diff === 0) {
          return a.distance - b.distance;
        }

        return diff;
      }),
      observable: angles.size,
      count: asteroidMap.length,
    };

    if (stationCandidate.observable > bestStation.observable) {
      if (globalThis.logger) {
        if (bestStation.coordinates) {
          globalThis.logger.writeTo(...bestStation.coordinates, "▓");
        }
        globalThis.logger.writeTo(...stationCandidate.coordinates, "S");
      }

      bestStation = stationCandidate;
    }

    return stationCandidate;
  });

  return bestStation;
};

export const destroyAsteroids = (station) => {
  const { asteroidMap, count } = station;

  const destroyed = [];

  do {
    let lastAngle = 360;

    for (let i = 0; i < asteroidMap.length; i += 1) {
      if (asteroidMap[i] && asteroidMap[i].angle !== lastAngle) {
        if (globalThis.logger) {
          globalThis.logger.writeTo(...asteroidMap[i].coordinates, " ");
        }
        destroyed.push([...asteroidMap[i].coordinates]);

        lastAngle = asteroidMap[i].angle;
        delete asteroidMap[i];
      }
    }
  } while (destroyed.length < count);

  return destroyed;
};
