import { assertEquals } from "testing/asserts.ts";

import { runOpCode } from "./02_opcode.js";

import { dirname, loadIntegerIntoArray } from "../../lib/shared.js";

const mainProgram = await loadIntegerIntoArray(`${dirname(import.meta.url)}/../data/05.txt`);

Deno.test("1002,4,3,4,33", () => {
  assertEquals(runOpCode([1002, 4, 3, 4, 33]), {
    input: [],
    lastState: [1002, 4, 3, 4, 99],
    output: [],
    status: "TERMINATED",
  });
});

Deno.test("1101,100,-1,4,0", () => {
  assertEquals(runOpCode([1101, 100, -1, 4, 0]), {
    input: [],
    lastState: [1101, 100, -1, 4, 99],
    output: [],
    status: "TERMINATED",
  });
});

Deno.test("Main Program with 1", () => {
  assertEquals(runOpCode(mainProgram, { input: 1 }).output, [0, 0, 0, 0, 0, 0, 0, 0, 0, 8332629]);
});

// Part 2

Deno.test("OpCode 08 – Position Mode: equals 8", () => {
  const program = [3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8];
  assertEquals(runOpCode(program, { input: 7 }).output, [0]);
  assertEquals(runOpCode(program, { input: 8 }).output, [1]);
  assertEquals(runOpCode(program, { input: 9 }).output, [0]);
});

Deno.test("OpCode 07 – Position Mode: less than 8", () => {
  const program = [3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8];
  assertEquals(runOpCode(program, { input: 7 }).output, [1]);
  assertEquals(runOpCode(program, { input: 8 }).output, [0]);
  assertEquals(runOpCode(program, { input: 9 }).output, [0]);
});

Deno.test("OpCode 08 – Immediate Mode: equals 8", () => {
  const program = [3, 3, 1108, -1, 8, 3, 4, 3, 99];
  assertEquals(runOpCode(program, { input: 7 }).output, [0]);
  assertEquals(runOpCode(program, { input: 8 }).output, [1]);
  assertEquals(runOpCode(program, { input: 9 }).output, [0]);
});

Deno.test("OpCode 07 – Immediate Mode: less than 8", () => {
  const program = [3, 3, 1107, -1, 8, 3, 4, 3, 99];
  assertEquals(runOpCode(program, { input: 7 }).output, [1]);
  assertEquals(runOpCode(program, { input: 8 }).output, [0]);
  assertEquals(runOpCode(program, { input: 9 }).output, [0]);
});

Deno.test("OpCode 06 – Position Mode: Jump if non-zero", () => {
  const program = [3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9];
  assertEquals(runOpCode(program, { input: -1 }).output, [1]);
  assertEquals(runOpCode(program, { input: 0 }).output, [0]);
  assertEquals(runOpCode(program, { input: 7 }).output, [1]);
});

Deno.test("OpCode 05 – Immediate Mode: Jump if zero", () => {
  const program = [3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1];
  assertEquals(runOpCode(program, { input: -1 }).output, [1]);
  assertEquals(runOpCode(program, { input: 0 }).output, [0]);
  assertEquals(runOpCode(program, { input: 7 }).output, [1]);
});

Deno.test("Below / Larger 8:", () => {
  // deno-fmt-ignore
  const program = [
    3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36, 98, 0, 0,
    1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20,
    1105, 1, 46, 98, 99,
  ];
  assertEquals(runOpCode(program, { input: -1 }).output, [999]);
  assertEquals(runOpCode(program, { input: 8 }).output, [1000]);
  assertEquals(runOpCode(program, { input: 10 }).output, [1001]);
});

Deno.test("Main Program with 5", () => {
  assertEquals(runOpCode(mainProgram, { input: 5 }).output, [8805067]);
});
