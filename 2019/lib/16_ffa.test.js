import { assertEquals } from "testing/asserts.ts";

import { getNextPhase, getSignal, runPhases } from "./16_ffa.js";

Deno.test("getNextPhase", () => {
  const one = [1, 2, 3, 4, 5, 6, 7, 8];
  const two = [4, 8, 2, 2, 6, 1, 5, 8];
  const three = [3, 4, 0, 4, 0, 4, 3, 8];
  const four = [0, 3, 4, 1, 5, 5, 1, 8];
  const five = [0, 1, 0, 2, 9, 4, 9, 8];

  assertEquals(getNextPhase(one), two);
  assertEquals(getNextPhase(two), three);
  assertEquals(getNextPhase(three), four);
  assertEquals(getNextPhase(four), five);
});

Deno.test("runPhases", () => {
  assertEquals(runPhases("80871224585914546619083218645595", 100).substr(0, 8), "24176176");
  assertEquals(runPhases("19617804207202209144916044189917", 100).substr(0, 8), "73745418");
  assertEquals(runPhases("69317163492948606335995924319873", 100).substr(0, 8), "52432133");
});

Deno.test("getSignal", () => {
  assertEquals(getSignal("03036732577212944063491565474664"), "84462026");
  assertEquals(getSignal("02935109699940807407585447034323"), "78725270");
  assertEquals(getSignal("03081770884921959731165446850517"), "53553731");
});
