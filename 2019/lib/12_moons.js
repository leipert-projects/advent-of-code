/* eslint-disable max-classes-per-file */
import { combinate, lowestCommonMultiple } from "../../lib/math.js";

class Moon {
  constructor(...pos) {
    this.pos = pos;
    this.vel = [0, 0, 0];
    this.diff = [0, 0, 0];
    this.originalPosition = [...pos];
  }

  sameX() {
    return this.pos[0] === this.originalPosition[0];
  }

  sameY() {
    return this.pos[1] === this.originalPosition[1];
  }

  sameZ() {
    return this.pos[2] === this.originalPosition[2];
  }

  potentialEnergy() {
    return this.pos.reduce((sum, x) => sum + Math.abs(x), 0);
  }

  kineticEnergy() {
    return this.vel.reduce((sum, x) => sum + Math.abs(x), 0);
  }

  totalEnergy() {
    return this.potentialEnergy() * this.kineticEnergy();
  }

  gravitate(moon2) {
    this.pos.forEach((p, i) => {
      if (p > moon2.pos[i]) {
        this.diff[i] -= 1;
        // eslint-disable-next-line no-param-reassign
        moon2.diff[i] += 1;
      } else if (p < moon2.pos[i]) {
        this.diff[i] += 1;
        // eslint-disable-next-line no-param-reassign
        moon2.diff[i] -= 1;
      }
    });
  }

  step() {
    for (let i = 0; i < 3; i += 1) {
      this.vel[i] += this.diff[i];
      this.pos[i] += this.vel[i];
      this.diff[i] = 0;
    }
  }

  toString() {
    const pos = `<x=${this.pos[0]}, y=${this.pos[1]}, z=${this.pos[2]}>`;
    const vel = `<x=${this.vel[0]}, y=${this.vel[1]}, z=${this.vel[2]}>`;
    return `Pos: ${pos}, Vel: ${vel}, Energy: ${this.totalEnergy()}`;
  }
}

export class System {
  constructor(positions) {
    this.moons = positions.map((x) => new Moon(...x));

    this.combinations = combinate(4, 2);

    this.n = 1;
    this.xCycle = 0;
    this.yCycle = 0;
    this.zCycle = 0;
  }

  step() {
    this.combinations.forEach(([i, j]) => {
      this.moons[i].gravitate(this.moons[j]);
    });

    this.moons.forEach((x) => x.step());

    this.n += 1;

    if (!this.xCycle && this.moons.every((x) => x.sameX())) {
      this.xCycle = this.n;
    }

    if (!this.yCycle && this.moons.every((x) => x.sameY())) {
      this.yCycle = this.n;
    }

    if (!this.zCycle && this.moons.every((x) => x.sameZ())) {
      this.zCycle = this.n;
    }

    if (globalThis.logger && this.n % 1000 === 0) {
      this.logData();
    }
  }

  iterate(n = 1) {
    for (let i = n; i > 0; i -= 1) {
      this.step();
    }
  }

  iterateUntilInitialState() {
    while (!this.xCycle || !this.yCycle || !this.zCycle) {
      this.step();
    }

    return lowestCommonMultiple(lowestCommonMultiple(this.zCycle, this.yCycle), this.xCycle);
  }

  totalEnergy() {
    return this.moons.reduce((sum, x) => sum + x.totalEnergy(), 0);
  }

  logData() {
    const data = [
      `Steps: ${this.n}`,
      ...this.moons.map((x) => x.toString()),
      `Total Energy: ${this.totalEnergy()}`,
    ];
    globalThis.logger.logArray(data);
  }
}
