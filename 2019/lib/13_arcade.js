import Map2D from "../../lib/Map2D.js";
import { Program } from "./02_opcode.js";

const WALL = 1;
const BLOCK = 2;
const PADDLE = 3;
const BALL = 4;

const renderTile = (tile) => {
  if (tile === BALL) {
    return "O";
  }
  if (tile === WALL) {
    return "▊";
  }
  if (tile === BLOCK) {
    return "░";
  }
  if (tile === PADDLE) {
    return "▔";
  }

  return " ";
};

export const arcadeCabinet = (program) => {
  const robot = new Program(program);
  const map = new Map2D();

  let score = null;

  let ball = [];
  let player = [];

  function step() {
    robot.run();

    const res = robot.clearOutput();

    do {
      const [x, y, val] = res.splice(0, 3);

      if (x === -1) {
        score = val;
        // eslint-disable-next-line no-continue
        continue;
      }

      map.set(x, y, val);

      if (val === BALL) {
        ball = [x, y];
      } else if (val === PADDLE) {
        player = [x, y];
      }

      globalThis.logger.writeTo(x, y, renderTile(val));
    } while (res.length > 0);

    const diff = ball[0] - player[0];

    if (diff > 0) {
      robot.setInput(1);
    } else if (diff < 0) {
      robot.setInput(-1);
    } else {
      robot.setInput(0);
    }
  }

  step();

  const stats = new Map(map.stats);

  if (robot.status !== "TERMINATED") {
    do {
      step();
    } while (ball[1] < player[1] && map.stats.get(BLOCK) > 0);
  }

  return {
    map,
    stats,
    score,
  };
};
