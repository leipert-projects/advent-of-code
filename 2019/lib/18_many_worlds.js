import Map2D from "../../lib/Map2D.js";

export const manyWorlds = (input) => {
  const map = new Map2D();

  const keyLocations = {};

  input.split("\n").forEach((line, y) => {
    line.split("").forEach((char, x) => {
      if (char !== "#") {
        if (char === "@") {
          keyLocations[char] = [x, y];
        } else if (char.match(/^[a-z]$/)) {
          keyLocations[char] = [x, y];
        }

        map.set(x, y, char);
      }
    });
  });

  const edges = {};
  const distances = {};

  Object.entries(keyLocations).forEach(([key2, from]) => {
    Object.entries(keyLocations).forEach(([key, to]) => {
      if (key < key2) {
        const path = map.breadthSearch(from, to);

        const doors = path.flatMap((pos) => {
          const val = map.get(...pos);
          if (val.match(/^[A-Z]$/)) {
            return [val];
          }
          return [];
        });

        edges[key2] ||= {};
        edges[key] ||= {};
        distances[key2] ||= {};
        distances[key] ||= {};

        edges[key][key2] = doors;
        edges[key2][key] = doors;

        distances[key][key2] = path.length - 1;
        distances[key2][key] = path.length - 1;
      }
    });
  });

  const getReachable = (from, missingKeys) => {
    const unOpened = missingKeys.map((x) => x.toUpperCase());

    return missingKeys.filter(
      (to) => edges[from][to].filter((door) => unOpened.includes(door)).length === 0,
    );
  };

  const cache = {};

  const getDistance = (key, missingKeys) => {
    if (missingKeys.length === 0) {
      return 0;
    }

    const cacheKey = `${key}${[...missingKeys.sort().join("")]}`;

    if (cache[cacheKey]) {
      return cache[cacheKey];
    }

    const reachable = getReachable(key, missingKeys);
    let result = Number.MAX_SAFE_INTEGER;

    for (let i = 0; i < reachable.length; i += 1) {
      const target = reachable[i];

      const distance = distances[key][target] +
        getDistance(
          target,
          missingKeys.filter((x) => x !== target),
        );
      result = Math.min(distance, result);
    }

    cache[cacheKey] = result;

    return result;
  };

  const maxDistance = getDistance(
    "@",
    Object.keys(keyLocations).filter((x) => x !== "@"),
  );

  return { maxDistance };
};
