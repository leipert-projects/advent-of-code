/* eslint-disable no-console */
const isTTY = globalThis?.Deno ? Deno.isatty(Deno.stdout.rid) : process.stdout.isTTY;

let escapes;
let borders;
let green;
if (isTTY) {
  escapes = (await import("ansi-escapes")).default;
  const style = (await import("ansi-styles")).default;
  green = (str) => `${style.green.open}${str}${style.green.close}`;
  borders = Array(25)
    .fill(green("*"))
    .join(`${style.red.open}*${style.red.close}`);
} else {
  borders = Array(50).fill("*").join("");
  green = (x) => x;
  escapes = {
    cursorTo() {
      return "";
    },
    clearTerminal: "",
    cursorHide: "",
    cursorShow: "",
    eraseLine: "",
  };
}

const stdoutWrite = globalThis?.Deno
  ? (x) => Deno.stdout.writeSync(new TextEncoder().encode(x))
  : (x) => process.stdout.write(x);

export class Logger {
  constructor(url) {
    const [day, year] = url.replace(".js", "").split("/").reverse();
    stdoutWrite(escapes.clearTerminal);
    stdoutWrite(escapes.cursorHide);
    stdoutWrite([
      borders,
      green(`Advent of Code ${year}, Day ${day}`),
      borders,
    ].join("\n\n"));
    this.lastResult = null;

    this.maxY = 6;
  }

  writeTo(x, y, val) {
    if (isTTY) {
      this.maxY = Math.max(y + 6, this.maxY);
      stdoutWrite(`${escapes.cursorTo(x, y + 6)}${val}`);
    }
  }

  logArray(array, force) {
    this.lastResult = array;
    if (isTTY || force) {
      stdoutWrite(escapes.cursorTo(0, 6));
      stdoutWrite(array.map((line) => escapes.eraseLine + line).join("\n"));
      this.maxY = Math.max(array.length + 5, this.maxY);
    }
  }

  writeResults(part1, part2) {
    if (this.lastResult && !isTTY) {
      this.logArray(this.lastResult, true);
    }
    stdoutWrite(`${escapes.cursorTo(0, this.maxY)}\n`);
    stdoutWrite(escapes.cursorShow);
    console.log(`\n${borders}\n`);
    console.log(`Part 1:`, part1);
    if (part2) {
      console.log(`\nPart 2:`, part2);
    }
  }
}
