export const getNextPhase = (input) => {
  const vals = [...input];

  const res = [];

  for (let row = 0; row < vals.length; row += 1) {
    let sum = 0;

    let index = row;

    const jump = (row + 1) * 2;

    let nextJump = jump - 2;

    let val = 1;

    do {
      sum += val * vals[index];

      if (index === nextJump) {
        nextJump += jump;
        index += row + 2;
        val *= -1;
      } else {
        index += 1;
      }
    } while (index < vals.length);

    res.push(Math.abs(sum % 10));
  }

  return res;
};

export const runPhases = (input, start = 100) => {
  let n = start;
  let result = input.split("").map((x) => parseInt(x, 10));

  do {
    result = getNextPhase(result);
    n -= 1;
  } while (n > 0);

  return result.join("");
};

export const getSignal = (input) => {
  const offset = parseInt(input.substr(0, 7), 10);

  const signal = input.repeat(10000).substr(offset).split("");

  for (let n = 0; n < 100; n += 1) {
    for (let index = signal.length; index >= 0; index -= 1) {
      if (n === 0) {
        signal[index] = parseInt(signal[index], 10);
      }

      signal[index] = ((signal[index + 1] || 0) + signal[index]) % 10;
    }
  }

  return signal.slice(0, 8).join("");
};
