import { manhattanDistance } from "../../lib/math.js";

const steps = {
  R: [0, 1],
  U: [1, 1],
  L: [0, -1],
  D: [1, -1],
};

function runWireCreator(input, fn) {
  const directions = input.split(",");

  const pos = [0, 0];

  let count = 0;

  const path = {};

  directions.forEach((command) => {
    const [direction, ...length] = command;

    let curr = parseInt(length.join(""), 10);

    do {
      count += 1;

      pos[steps[direction][0]] += steps[direction][1];

      const res = fn(count, [...pos]);

      if (res) {
        path[pos.join()] = res;
      }

      curr -= 1;
    } while (curr > 0);
  });
  return path;
}

export const getWireCoordinates = (wire) => runWireCreator(wire, (count) => count);

export const getWireIntersections = (wire1, wire2) => {
  const otherWire = getWireCoordinates(wire2);

  return runWireCreator(wire1, (count, pos) => {
    if (otherWire[pos.join()]) {
      return count + otherWire[pos.join()];
    }

    return false;
  });
};

export const getMinManhattanDistance = (wire1, wire2) => {
  const intersections = getWireIntersections(wire1, wire2);

  return Math.min(...Object.keys(intersections).map((x) => manhattanDistance(x.split(","))));
};

export const getMinSignalDelay = (wire1, wire2) => {
  const intersections = getWireIntersections(wire1, wire2);

  return Math.min(...Object.values(intersections));
};
