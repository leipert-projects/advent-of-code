const constructGraph = (map) => {
  const graph = {};

  map.forEach((entry) => {
    const [a, b] = entry.split(")");

    graph[b] = a;
  });

  return graph;
};

function count(graph, d) {
  if (!graph[d]) {
    return 0;
  }

  return count(graph, graph[d]) + 1;
}

export const countOrbits = (map) => {
  const graph = constructGraph(map);

  return Object.keys(graph).reduce((sum, curr) => sum + count(graph, curr), 0);
};

const getPath = (graph, d) => {
  if (!graph[d]) {
    return [d];
  }

  return [...getPath(graph, graph[d]), d];
};

export const minimalTransfers = (map) => {
  const graph = constructGraph(map);

  const you = getPath(graph, "YOU").reverse();
  const san = getPath(graph, "SAN").reverse();

  you.shift();
  san.shift();
  const diffPath = [];

  for (const x of you) {
    diffPath.push(x);
    if (san.includes(x)) {
      break;
    }
  }

  for (const x of san) {
    if (you.includes(x)) {
      break;
    }
    diffPath.push(x);
  }

  return diffPath.length - 1;
};
