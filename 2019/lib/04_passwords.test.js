import { assertEquals } from "testing/asserts.ts";
import { matchConstraints } from "./04_passwords.js";

[
  [1223, false, false],
  [1223457, false, false],
  [223450, false, false],
  [111111, true, false],
  [123444, true, false],
  [122345, true, true],
  [111122, true, true],
].forEach(([a, constraint1, constraint2]) => {
  Deno.test(`matchConstraints(${a}) => [${constraint1}, ${constraint2}]`, () => {
    assertEquals(matchConstraints(a), [constraint1, constraint2]);
  });
});
