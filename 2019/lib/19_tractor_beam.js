import { Program } from "./02_opcode.js";
import Map2D from "../../lib/Map2D.js";

let map = null;

export const deployDrone = (program) => {
  if (map) {
    return map;
  }

  map = new Map2D();

  for (let y = 0; y < 50; y += 1) {
    for (let x = 0; x < 50; x += 1) {
      const drone = new Program(program);

      drone.setInput(x, y);

      drone.run();

      if (drone.clearOutput()[0]) {
        map.set(x, y, 1);
      }
    }

    if (globalThis.logger) {
      globalThis.logger.logArray(map.render((val) => (val === 1 ? "#" : ".")));
    }
  }

  return map;
};

export const findShipSpot = (program) => {
  const mapWithDrone = deployDrone(program);

  let min = Number.MAX_SAFE_INTEGER;
  let max = Number.MIN_SAFE_INTEGER;

  let y = 49;

  for (let x = 0; x < 50; x += 1) {
    const value = mapWithDrone.get(x, y);

    if (value) {
      min = Math.min(min, x);
      max = Math.max(max, x);
    }
  }

  do {
    y += 1;

    for (let x = min - 3; x < max + 3; x += 1) {
      const drone = new Program(program);

      drone.setInput(x, y);

      drone.run();

      if (drone.clearOutput()[0]) {
        min = Math.min(min, x);
        max = Math.max(max, x);
        mapWithDrone.set(x, y, 1);
      }
    }
  } while (max - min < 100);
  globalThis.logger.logArray(mapWithDrone.render((val) => (val === 1 ? "#" : ".")));

  // logger.writeTo(0,51, [y, min,max] )
};
