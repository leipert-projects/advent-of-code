import { assertEquals } from "testing/asserts.ts";

import {
  getMinManhattanDistance,
  getMinSignalDelay,
  getWireCoordinates,
  getWireIntersections,
} from "./03_wires.js";
import { manhattanDistance } from "../../lib/math.js";

const wire1 = "R8,U5,L5,D3";
const wire2 = "U7,R6,D4,L4";

[
  [[15, 1], 16],
  [[-15, -1], 16],
  [[1, -15], 16],
  [[-15, 1], 16],
].forEach(([a, expected]) => {
  Deno.test(`manhattanDistance([${a}]) => ${expected}`, () => {
    assertEquals(manhattanDistance(a), expected);
  });
});

Deno.test(`getWireCoordinates(${wire1})`, () => {
  assertEquals(getWireCoordinates(wire1), {
    "1,0": 1,
    "2,0": 2,
    "3,0": 3,
    "3,2": 21,
    "3,3": 20,
    "3,4": 19,
    "3,5": 18,
    "4,0": 4,
    "4,5": 17,
    "5,0": 5,
    "5,5": 16,
    "6,0": 6,
    "6,5": 15,
    "7,0": 7,
    "7,5": 14,
    "8,0": 8,
    "8,1": 9,
    "8,2": 10,
    "8,3": 11,
    "8,4": 12,
    "8,5": 13,
  });
});

Deno.test(`getWireCoordinates(${wire2})`, () => {
  assertEquals(getWireCoordinates(wire2), {
    "0,1": 1,
    "0,2": 2,
    "0,3": 3,
    "0,4": 4,
    "0,5": 5,
    "0,6": 6,
    "0,7": 7,
    "1,7": 8,
    "2,3": 21,
    "2,7": 9,
    "3,3": 20,
    "3,7": 10,
    "4,3": 19,
    "4,7": 11,
    "5,3": 18,
    "5,7": 12,
    "6,3": 17,
    "6,4": 16,
    "6,5": 15,
    "6,6": 14,
    "6,7": 13,
  });
});

Deno.test("getWireIntersections", () => {
  assertEquals(getWireIntersections(wire1, wire2), {
    "3,3": 40,
    "6,5": 30,
  });
});

const fixture1 = ["R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83"];
const fixture2 = [
  "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51",
  "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7",
];

Deno.test("getMinManhattanDistance", () => {
  assertEquals(getMinManhattanDistance(wire1, wire2), 6);
  assertEquals(getMinManhattanDistance(...fixture1), 159);
  assertEquals(getMinManhattanDistance(...fixture2), 135);
});

Deno.test("getMinSignalDelay", () => {
  assertEquals(getMinSignalDelay(wire1, wire2), 30);
  assertEquals(getMinSignalDelay(...fixture1), 610);
  assertEquals(getMinSignalDelay(...fixture2), 410);
});
