export function calculateFuelConsumption(input) {
  return Math.floor(parseInt(input, 10) / 3) - 2;
}

export function calculateFuelConsumptionRecursive(input) {
  const need = calculateFuelConsumption(input);

  if (need > 0) {
    return need + calculateFuelConsumptionRecursive(need);
  }

  return 0;
}

export function sumFuelConsumption(inputs, formula) {
  return inputs.reduce((sum, input) => sum + formula(input), 0);
}
