export function matchConstraints(password) {
  if (`${password}`.length !== 6) {
    return [false, false];
  }

  const arr = `${password}`.split("").map((x) => parseInt(x, 10));

  const counts = {};

  counts[arr[0]] = 1;

  for (let i = 5; i > 0; i -= 1) {
    counts[arr[i]] = !counts[arr[i]] ? 1 : counts[arr[i]] + 1;
    if (arr[i - 1] > arr[i]) {
      return [false, false];
    }
  }

  return [Object.values(counts).length < 6, Object.values(counts).includes(2)];
}
