import { Program } from "./02_opcode.js";
import Map2D from "../../lib/Map2D.js";

const LEAK = 2;
const TRAVERSABLE = 1;
const VISITED = -5;
const WALL = 0;
const OXYGEN = 5;

const renderTile = (tile) => {
  if (tile === TRAVERSABLE) {
    return ".";
  }
  if (tile === VISITED) {
    return ":";
  }
  if (tile === WALL) {
    return "▊";
  }
  if (tile === LEAK) {
    return "L";
  }
  if (tile === OXYGEN) {
    return "O";
  }

  return " ";
};

const NORTH = 1;
const SOUTH = 2;
const WEST = 3;
const EAST = 4;

const DIRECTIONS = [NORTH, EAST, SOUTH, WEST];

const getNextPos = (pos, dir) => {
  const nextPos = [...pos];

  if (dir === NORTH) {
    nextPos[1] -= 1;
  } else if (dir === SOUTH) {
    nextPos[1] += 1;
  } else if (dir === WEST) {
    nextPos[0] -= 1;
  } else if (dir === EAST) {
    nextPos[0] += 1;
  }

  return nextPos;
};

export const oxygenSystem = (program) => {
  const robot = new Program(program);
  const map = new Map2D();

  let pos = [0, 0];

  map.set(0, 0, TRAVERSABLE);

  let currDir = NORTH;

  function getBestDir(position) {
    let max = -200;
    let nextDirection = 0;

    DIRECTIONS.forEach((newDirection) => {
      let value = map.get(...getNextPos(position, newDirection));
      value = value === undefined ? 100 : value;

      if (value > max) {
        max = value;
        nextDirection = newDirection;
      }
    });
    return nextDirection;
  }

  function inDeadEnd(position) {
    let walls = 0;

    DIRECTIONS.forEach((dir) => {
      const value = map.get(...getNextPos(position, dir));

      if ([WALL, VISITED].includes(value)) {
        walls += 1;
      }
    });

    return walls >= 3;
  }

  const leak = {};

  do {
    robot.setInput(currDir);

    robot.run();

    const [type] = robot.clearOutput();

    const nextPos = getNextPos(pos, currDir);

    map.set(...nextPos, type);

    if (type === TRAVERSABLE) {
      pos = nextPos;
    } else if (type === LEAK) {
      pos = nextPos;
      leak.pos = [...nextPos];
      leak.steps = map.stats.get(TRAVERSABLE);
    }

    if (inDeadEnd(pos)) {
      map.set(...pos, VISITED);
    }
    currDir = getBestDir(pos, currDir);

    globalThis.logger.logArray(map.render(renderTile));
    globalThis.logger.writeTo(pos[0] - map.minX, pos[1] - map.minY, "X");
  } while (map.stats.get(TRAVERSABLE) > 0);

  let oxygenBoundary = [[...leak.pos]];
  let n = 0;

  do {
    n += 1;

    const nextStep = [];

    for (let i = 0; i < oxygenBoundary.length; i += 1) {
      const position = oxygenBoundary[i];
      map.set(...position, OXYGEN);
      for (let j = 0; j < DIRECTIONS.length; j += 1) {
        const nextPos = getNextPos(position, DIRECTIONS[j]);

        const value = map.get(...nextPos);

        if ([VISITED].includes(value)) {
          nextStep.push(nextPos);
        }
      }
    }

    oxygenBoundary = nextStep;

    globalThis.logger.logArray(map.render(renderTile));
  } while (map.stats.get(VISITED) > 0);

  return {
    leak,
    filledAfter: n - 1,
  };
};
