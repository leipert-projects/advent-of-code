import { assertEquals } from "testing/asserts.ts";

import { System } from "./12_moons.js";

const firstSystem = [
  [-1, 0, 2],
  [2, -10, -7],
  [4, -8, 8],
  [3, 5, -1],
];

const secondSystem = [
  [-8, -10, 0],
  [5, 5, 10],
  [2, -7, 3],
  [9, -8, -3],
];

Deno.test("System 1", () => {
  const system = new System(firstSystem);

  system.iterate(10);

  assertEquals(system.moons[0].pos, [2, 1, -3]);
  assertEquals(system.moons[0].vel, [-3, -2, 1]);
  assertEquals(system.totalEnergy(), 179);
});

Deno.test("System 2", () => {
  const system = new System(secondSystem);

  system.iterate(100);

  assertEquals(system.moons[0].pos, [8, -12, -9]);
  assertEquals(system.moons[0].vel, [-7, 3, 0]);
  assertEquals(system.totalEnergy(), 1940);
});

Deno.test("System 1 returns", () => {
  const system = new System(firstSystem);

  assertEquals(system.iterateUntilInitialState(), 2772);
});

Deno.test("System 2 returns", () => {
  const system = new System(secondSystem);

  assertEquals(system.iterateUntilInitialState(), 4686774924);
});
