import { loadStdIn } from "../lib/shared.js";

import { matchConstraints } from "./lib/04_passwords.js";

let [start, end] = (await loadStdIn()).trim().split("-");

if (!end) {
  console.log(matchConstraints(start));
} else {
  start = parseInt(start, 10);
  end = parseInt(end, 10);
  const counts = { constraint1: 0, constraint2: 0 };
  for (start; start < end; start += 1) {
    const res = matchConstraints(start);
    if (res) {
      counts.constraint1 += res[0];
      counts.constraint2 += res[1];
    }
  }
  console.log(`Part 1: ${counts.constraint1}`);
  console.log(`Part 2: ${counts.constraint2}`);
}
