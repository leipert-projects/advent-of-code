import { loadStdInIntoArray } from "../lib/shared.js";

import {
  getMinManhattanDistance,
  getMinSignalDelay,
  getWireIntersections,
} from "./lib/03_wires.js";

const [wire1Path, wire2Path] = await loadStdInIntoArray();

console.log("Intersection:", getWireIntersections(wire2Path, wire1Path));

console.log("Minimum Manhattan:", getMinManhattanDistance(wire2Path, wire1Path));
console.log("Minimum Cable Run length:", getMinSignalDelay(wire2Path, wire1Path));
