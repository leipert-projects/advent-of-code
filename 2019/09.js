import { runOpCode } from "./lib/02_opcode.js";
import { loadStdInIntoIntegerArray } from "../lib/shared.js";

const program = await loadStdInIntoIntegerArray();

const testMode = runOpCode(program, { input: [1] }).output;

console.log(`Part 1: ${testMode}`);

const parsedDistressSignal = runOpCode(program, { input: [2] }).output;

console.log(`Part 2: ${parsedDistressSignal}`);
