import { Logger } from "./lib/Logger.js";
import { loadStdIn } from "../lib/shared.js";
import { getBOM, getMaxFuel, parseFormulas } from "./lib/14_bill_of_materials.js";

const input = await loadStdIn();

const logger = new Logger(import.meta.url);

const formulas = parseFormulas(input);

const part1 = -getBOM(formulas).ORE;

const part2 = getMaxFuel(formulas);

logger.writeResults(part1, part2);
