import { Logger } from "./lib/Logger.js";
import { loadStdInIntoIntegerArray } from "../lib/shared.js";

import { oxygenSystem } from "./lib/15_oxygen.js";

const program = await loadStdInIntoIntegerArray();

globalThis.logger = new Logger(import.meta.url);

const res = oxygenSystem(program);

globalThis.logger.writeResults(res.leak.steps, res.filledAfter);
