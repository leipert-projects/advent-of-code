import { manyWorlds } from "./lib/18_many_worlds.js";
import { loadStdIn } from "../lib/shared.js";

const map = await loadStdIn();

const res = manyWorlds(map);

console.log(`Part 1: ${res.maxDistance}`);
