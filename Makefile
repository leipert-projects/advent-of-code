.PRECIOUS: %.ts %.txt

%.ts:
	@mkdir -p "$(@D)"
	@cp template.ts $@

lib/%.js: lib/%.ts
	yarn run esbuild "$<" --outfile="$@" --target=node20 --platform=node --format=esm

lib: lib/Map2D.js

%.txt:
	@mkdir -p "$(@D)"
	touch $@

2023/bench/%.md: 2023/%.ts .tool-versions ./run.sh lib/shared.js
	@mkdir -p "$(@D)"
	@zsh ./run.sh $< "$(<D)" "$*" "$@"

2022/bench/%.md: 2022/%.js .tool-versions ./run.sh lib/shared.js lib
	@mkdir -p "$(@D)"
	@zsh ./run.sh $< "$(<D)" "$*" "$@"

2021/bench/%.md: 2021/%.js .tool-versions ./run.sh lib/shared.js lib
	@mkdir -p "$(@D)"
	@zsh ./run.sh $< "$(<D)" "$*" "$@"

2020/bench/%.md: 2020/%.js .tool-versions ./run.sh lib/shared.js lib
	@mkdir -p "$(@D)"
	@NO_STDIN=true zsh ./run.sh "$<" "$(<D)" "$*" "$@"

2019/bench/%.md: 2019/%.js .tool-versions ./run.sh lib/shared.js 2019/lib/Logger.js lib
	@mkdir -p "$(@D)"
	@zsh ./run.sh $< "$(<D)" "$*" "$@"

2015/bench/%.md: 2015/%.js .tool-versions ./run.sh lib/shared.js lib
	@mkdir -p "$(@D)"
	@zsh ./run.sh $< "$(<D)" "$*" "$@"

ex-%: 2023/%.ts 2023/data/%_example.txt 2023/data/%.txt
	@bun run --hot  "$(<D)"/"$*".ts < "$(<D)"/data/"$*"_example.txt

dev-%: 2023/%.ts 2023/data/%.txt
	@bun run --hot  "$(<D)"/"$*".ts < "$(<D)"/data/"$*".txt