export {};
interface Process {
  execArgv: Array<string>;
}

declare global {
  var hotReloadCount: number;
  var hotReloadStdIn: string;

  var process: Process;
}
