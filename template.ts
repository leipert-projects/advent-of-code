import { exit, loadStdInIntoArray } from "../lib/shared.js";
function part1Fn(data: string[]) {
  return data;
}

// function part2Fn(data: string[]) {
//   console.log(data)
// }

try {
  const stdIn = await loadStdInIntoArray();
  const part1 = part1Fn(stdIn);

  console.log(`\tPart 1: ${part1}`);

  // const part2 = part2Fn(stdIn);
  //
  // console.log(`\tPart 2: ${part2}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
