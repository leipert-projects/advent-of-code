import {
  tripDownMemoryGameArray,
  tripDownMemoryGameArrayNaive,
  tripDownMemoryGameInt32Array,
  tripDownMemoryGameMap,
  tripDownMemoryGameObject,
} from "./lib/15_memorygame.js";

const benchFactory = (fn) => () => {
  fn([10, 16, 6, 0, 1, 17], 30000);
};

Deno.bench("Int32Array", { group: "2020/15" }, benchFactory(tripDownMemoryGameInt32Array));
Deno.bench("Array", { group: "2020/15" }, benchFactory(tripDownMemoryGameArray));
Deno.bench("Map", { group: "2020/15" }, benchFactory(tripDownMemoryGameMap));
Deno.bench("Object", { group: "2020/15" }, benchFactory(tripDownMemoryGameObject));
Deno.bench("Array (naive)", { group: "2020/15" }, benchFactory(tripDownMemoryGameArrayNaive));
