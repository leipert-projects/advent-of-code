import { dirname, loadIntegerIntoArray } from "../lib/shared.js";

const example = await loadIntegerIntoArray(`${dirname(import.meta.url)}/data/09_example.txt`);
const data = await loadIntegerIntoArray(`${dirname(import.meta.url)}/data/09.txt`);

function findXMASWeakness(list, preambleLength) {
  const queue = list.slice(0, preambleLength);
  for (let i = preambleLength; i < list.length; i += 1) {
    const curr = list[i];
    if (!queue.some((x) => queue.includes(curr - x) && curr - x !== x)) {
      return curr;
    }
    queue.shift();
    queue.push(curr);
  }
  return null;
}

function findWeaknessCause(list, weakness) {
  let j;
  for (let i = 0; i < list.length; i += 1) {
    let sum = 0;
    for (j = i; j < list.length; j += 1) {
      sum += list[j];
      if (sum >= weakness) {
        break;
      }
    }
    if (sum === weakness) {
      const match = list.slice(i, j + 1).sort();
      return match[0] + match[match.length - 1];
    }
  }
  return null;
}

const exampleWeakness = findXMASWeakness(example, 5);
console.log(`Part 1 – Example: ${exampleWeakness}`);
console.log(`Part 2 – Example: ${findWeaknessCause(example, exampleWeakness)}`);

const problemWeakness = findXMASWeakness(data, 25);
console.log(`Part 1 – Data: ${problemWeakness}`);
console.log(`Part 2 – Data: ${findWeaknessCause(data, problemWeakness)}`);
