import { dirname, loadFileIntoArray } from "../lib/shared.js";

const example = await loadFileIntoArray(`${dirname(import.meta.url)}/data/16_example.txt`);
const data = await loadFileIntoArray(`${dirname(import.meta.url)}/data/16.txt`);

function createRule(line) {
  const [, name, ...nums] = line.match(/^(.+):\s(\d+)-(\d+)\sor\s(\d+)-(\d+)/);

  const [min1, max1, min2, max2] = nums.map(Number);

  return [
    name,
    (number) => (min1 <= number && number <= max1) || (min2 <= number && number <= max2),
  ];
}

function parseTickets(rules, line) {
  const checks = Object.values(rules);

  return line
    .split(",")
    .map(Number)
    .reduce(
      (acc, curr) => {
        acc.fields.push(curr);
        if (!checks.some((check) => check(curr))) {
          acc.invalid.push(curr);
        }
        return acc;
      },
      { fields: [], invalid: [] },
    );
}

function parseData(input) {
  const rules = {};
  const tickets = [];
  for (const line of input) {
    if (/\w+: \d/.test(line)) {
      const [name, rule] = createRule(line);
      rules[name] = rule;
    } else if (/^\d/.test(line)) {
      tickets.push(parseTickets(rules, line));
    }
  }
  return { tickets, rules };
}

function findErrorRate(input) {
  const { tickets } = parseData(input);
  return tickets.flatMap((x) => x.invalid).reduce((acc = 0, x) => acc + x);
}

function identifyTicketFieldOrder(ticketsCandidates, rules) {
  const fieldNames = Object.keys(rules);

  const validTickets = ticketsCandidates.filter((x) => x.invalid.length === 0);

  const candidates = Array(fieldNames.length)
    .fill(null)
    .map((_x, index) => [index, fieldNames, false]);

  const order = [];

  do {
    const [index, candidateFields, seen] = candidates.shift();

    const newFields = candidateFields.filter((fieldName) =>
      seen
        ? !order.includes(fieldName)
        : validTickets.every(({ fields }) => rules[fieldName](fields[index]))
    );
    if (newFields.length === 1) {
      // eslint-disable-next-line prefer-destructuring
      order[index] = newFields[0];
    } else {
      candidates.push([index, newFields, true]);
    }
  } while (candidates.length > 0);

  return order;
}

console.log(`Part 1 – Example: ${findErrorRate(example)}`);
console.log(`Part 1 – Data: ${findErrorRate(data)}`);

function getMyDepartureNumber(input) {
  const { tickets: ticketsCandidates, rules } = parseData(input);

  const fields = identifyTicketFieldOrder(ticketsCandidates, rules);

  return fields.reduce((acc, field, index) => {
    if (field.startsWith("departure")) {
      return ticketsCandidates[0].fields[index] * acc;
    }
    return acc;
  }, 1);
}

console.log(`Part 2 – Data: ${getMyDepartureNumber(data)}`);
