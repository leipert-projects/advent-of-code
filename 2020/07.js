import { dirname, loadFileIntoArray } from "../lib/shared.js";

function parseRules(rules) {
  const inverseGraph = {};
  const graph = {};

  rules.forEach((x) => {
    const [col, right] = x.split(/ bags contain /);

    const matches = [...right.matchAll(/(\d+) ([\w ]+?) (bags?[,.])/g)];

    graph[col] = [];

    matches.forEach((m) => {
      if (!inverseGraph[m[2]]) {
        inverseGraph[m[2]] = [];
      }
      inverseGraph[m[2]].push(col);
      graph[col].push([m[2], Number(m[1])]);
    });
  });

  return { inverseGraph, graph };
}

function canContainBag(inverseGraph, color) {
  const stack = inverseGraph[color] || [];
  const result = new Set();
  do {
    const curr = stack.pop();

    if (!result.has(curr)) {
      result.add(curr);
      stack.push(...(inverseGraph[curr] || []));
    }
  } while (stack.length > 0);

  return result;
}

function bagSize(graph, spec) {
  const [color, count] = spec;
  if (graph[color].length === 0) {
    return count;
  }
  const sum = graph[color].reduce((acc, x) => acc + bagSize(graph, x), 0);
  return count * sum + count;
}

function day7(rules, color) {
  const { inverseGraph, graph } = parseRules(rules);
  console.log(
    `\tPart 1: ${
      canContainBag(inverseGraph, color).size
    } different bags can contain a ${color} bag`,
  );
  console.log(`\tPart 2: Each ${color} bag contains ${bagSize(graph, [color, 1]) - 1} bags`);
}

console.log("Example");
day7(
  await loadFileIntoArray(`${dirname(import.meta.url)}/data/07_example.txt`),
  "shiny gold",
);
console.log("Data");
day7(await loadFileIntoArray(`${dirname(import.meta.url)}/data/07.txt`), "shiny gold");
