import { dirname, loadFile } from "../lib/shared.js";
import Map2D from "../lib/Map2D.js";

const sourceFile = await loadFile(`${dirname(import.meta.url)}/data/11_example.txt`);
const data = await loadFile(`${dirname(import.meta.url)}/data/11.txt`);

const renderer = (x) => {
  switch (x) {
    case 1:
      return "#";
    case 0:
      return "L";
    default:
      return ".";
  }
};

const playConwaysGame = (input, depth = 1, maxNeighbors = 4) => {
  let state = new Map2D();

  {
    let x = 0;
    let y = 0;

    input.split("").forEach((curr) => {
      if (curr === "\n") {
        x = 0;
        y += 1;
      } else {
        if (curr === "#") {
          state.set(x, y, 1);
        } else if (curr === "L") {
          state.set(x, y, 0);
        } else if (curr === ".") {
          state.set(x, y, null);
        }
        x += 1;
      }
    });
  }
  console.log(`${state.render(renderer).join("\n")}\n`);
  let unstable;
  do {
    unstable = false;
    const newState = new Map2D();

    // eslint-disable-next-line no-loop-func
    state.forEach((node) => {
      const { value, x, y } = node;
      if (value === null) {
        newState.set(x, y, value);
      } else {
        const neighbors = [
          [-1, -1],
          [0, -1],
          [1, -1],
          [-1, 0],
          [1, 0],
          [-1, 1],
          [0, 1],
          [1, 1],
        ]
          .map(([diffX, diffY]) => {
            let posX = x;
            let posY = y;
            let step = 0;
            let val = null;
            do {
              posX += diffX;
              posY += diffY;
              step += 1;
              val = state.get(posX, posY);
            } while (step < depth && !Number.isInteger(val));
            return val;
          })
          .filter(Boolean).length;

        const newValue =
          (value === 1 && neighbors < maxNeighbors) || (value === 0 && neighbors === 0) ? 1 : 0;

        newState.set(x, y, newValue);
        if (newValue !== value) {
          unstable = true;
        }
      }
    });

    state = newState;
  } while (unstable);

  console.log(`${state.render(renderer).join("\n")}\n`);

  return state.reduce((acc, val) => acc + Number(val.value), 0);
};

console.log(`Part 1 – Example: ${playConwaysGame(sourceFile)}`);
console.log(`Part 1 - Data: ${playConwaysGame(data)}`);
console.log(`Part 2 – Example: ${playConwaysGame(sourceFile, 1000, 5)}`);
console.log(`Part 2 – Data: ${playConwaysGame(data, 1000, 5)}`);
