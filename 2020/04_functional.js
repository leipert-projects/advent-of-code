import { dirname, loadFile } from "../lib/shared.js";

const rules = {
  byr: (str) => /^\d{4}$/.test(str) && str >= "1920" && str <= "2002",
  iyr: (str) => /^\d{4}$/.test(str) && str >= "2010" && str <= "2020",
  eyr: (str) => /^\d{4}$/.test(str) && str >= "2020" && str <= "2030",
  hgt: (str) => {
    const match = str && str.match(/^((\d{3})cm|(\d{2})in)$/);

    return (
      Boolean(match) &&
      ((match[2] >= "150" && match[2] <= "193") || (match[3] >= "59" && match[3] <= "76"))
    );
  },
  hcl: (str) => /^#[0-9a-f]{6}$/.test(str),
  ecl: (str) => ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].includes(str),
  pid: (str) => /^\d{9}$/.test(str),
};

const fields = Object.keys(rules);

const hasAllRequiredFields = (passport) => fields.every((field) => Boolean(passport[field]));

const isValid = (passport) => fields.every((field) => rules[field](passport[field]));

const parsePassport = (passport) =>
  Object.fromEntries(passport.split(/\s+/m).map((field) => field.split(":")));

const data = (await loadFile(`${dirname(import.meta.url)}/data/04.txt`))
  .trim()
  .split(/\n\s*\n+/)
  .map(parsePassport);

console.log(`Part 1: ${data.filter(hasAllRequiredFields).length}`);
console.log(`Part 2: ${data.filter(isValid).length}`);
