import { tripDownMemoryGameMap } from "./lib/15_memorygame.js";

console.log(`Part 1 – Example: ${tripDownMemoryGameMap([0, 3, 6])}`);
console.log(`Part 1 – Data: ${tripDownMemoryGameMap([10, 16, 6, 0, 1, 17])}`);
console.log(`Part 2 – Data: ${tripDownMemoryGameMap([10, 16, 6, 0, 1, 17], 30000000)}`);
