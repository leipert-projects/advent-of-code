import { dirname, loadFileIntoArray } from "../lib/shared.js";
import Console from "./lib/08_console.js";

const example = await loadFileIntoArray(`${dirname(import.meta.url)}/data/08_example.txt`);
const data = await loadFileIntoArray(`${dirname(import.meta.url)}/data/08.txt`);

const ExampleConsole = new Console(example);
const DataConsole = new Console(data);

console.log(`Part 1 – Example: ${ExampleConsole.runProgramWithLoopDetection()}`);
console.log(`Part 1 – Data: ${DataConsole.runProgramWithLoopDetection()}`);

ExampleConsole.bootstrap();
ExampleConsole.setInstruction(7, "nop");

console.log(`Part 2 – Example: ${ExampleConsole.runProgramWithLoopDetection()}`);

for (let i = 0; i < DataConsole.instructions.length; i += 1) {
  const { command } = DataConsole.instructions[i];
  if (["nop", "jmp"].includes(command)) {
    DataConsole.bootstrap();
    DataConsole.setInstruction(i, command === "jmp" ? "nop" : "jmp");
    const [halted, accumulator] = DataConsole.runProgramWithLoopDetection();
    if (halted) {
      console.log(`Part 2 – Data: ${accumulator}`);
      break;
    }
  }
}
