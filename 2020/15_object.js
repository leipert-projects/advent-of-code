import { tripDownMemoryGameObject } from "./lib/15_memorygame.js";

console.log(`Part 1 – Example: ${tripDownMemoryGameObject([0, 3, 6])}`);
console.log(`Part 1 – Data: ${tripDownMemoryGameObject([10, 16, 6, 0, 1, 17])}`);
console.log(`Part 2 – Data: ${tripDownMemoryGameObject([10, 16, 6, 0, 1, 17], 30000000)}`);
