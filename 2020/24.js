import ConwaysGame from "../lib/ConwaysGame.js";

import { dirname, loadFileIntoArray } from "../lib/shared.js";

const examples = await loadFileIntoArray(`${dirname(import.meta.url)}/data/24_example.txt`);
const data = await loadFileIntoArray(`${dirname(import.meta.url)}/data/24.txt`);

const neighbors = [
  [-1, 0, 1], // sw
  [0, -1, 1], // se
  [0, 1, -1], // nw
  [1, 0, -1], // ne
  [-1, 1, 0], // w
  [1, -1, 0], // e
];

class HexagonalConwaysGame extends ConwaysGame {
  constructor() {
    super(3);
    this.neighbors = neighbors;
  }

  clean() {
    this.forEach((x, key) => {
      if (!x.value) {
        this.map.delete(key);
      }
    });
  }

  countTiles() {
    return this.map.size;
  }

  play(steps) {
    super.play(steps, new Set([1, 2]), new Set([2]));
  }
}

function processLine(input) {
  const target = [0, 0, 0];
  let prev = "";
  input.split("").forEach((curr) => {
    let diff = [0, 0, 0];
    /* eslint-disable prefer-destructuring */
    if (prev + curr === "sw") {
      diff = neighbors[0];
    } else if (prev + curr === "se") {
      diff = neighbors[1];
    } else if (prev + curr === "nw") {
      diff = neighbors[2];
    } else if (prev + curr === "ne") {
      diff = neighbors[3];
    } else if (curr === "w") {
      diff = neighbors[4];
    } else if (curr === "e") {
      diff = neighbors[5];
    }
    /* eslint-enable prefer-destructuring */
    target[0] += diff[0];
    target[1] += diff[1];
    target[2] += diff[2];
    prev = curr;
  });

  return target;
}

function countTiles(array) {
  const game = new HexagonalConwaysGame(3);

  array.map(processLine).forEach(([x, y, z]) => {
    const val = game.get(x, y, z) || false;
    game.set(!val, x, y, z);
  });

  game.clean();

  return game;
}

const ExampleGame = countTiles(examples);
console.log(`Part 1 – Example – Black Tiles: ${ExampleGame.countTiles()}`);

const DataGame = countTiles(data);
console.log(`Part 1 – Data – Black Tiles: ${DataGame.countTiles()}`);

ExampleGame.play(100);
console.log(`Part 2 – Example – Black Tiles: ${ExampleGame.countTiles()}`);

DataGame.play(100);
console.log(`Part 2 – Data – Black Tiles: ${DataGame.countTiles()}`);
