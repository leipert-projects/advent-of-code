## 2020/16.js

```
Part 1 – Example: 71
Part 1 – Data: 26941
Part 2 – Data: 634796407951
```

| Command                                                                             |  Mean [ms] | Min [ms] | Max [ms] |    Relative |
| :---------------------------------------------------------------------------------- | ---------: | -------: | -------: | ----------: |
| `bun run 2020/16.js < /dev/null`                                                    | 18.0 ± 5.0 |     16.1 |     60.3 |        1.00 |
| `deno run --allow-read=./ --no-config --allow-net=deno.land 2020/16.js < /dev/null` | 22.6 ± 0.3 |     22.0 |     23.9 | 1.25 ± 0.35 |
| `node 2020/16.js < /dev/null`                                                       | 40.9 ± 2.7 |     39.6 |     55.5 | 2.27 ± 0.65 |

- bun 1.0.13
- deno 1.38.2 (release, aarch64-apple-darwin)
- node v21.2.0
