## 2020/15_object.js

```
Part 1 – Example: 436
Part 1 – Data: 412
Part 2 – Data: 243
```

| Command                                                                                    |        Mean [ms] | Min [ms] | Max [ms] |      Relative |
| :----------------------------------------------------------------------------------------- | ---------------: | -------: | -------: | ------------: |
| `bun run 2020/15_object.js < /dev/null`                                                    |     805.6 ± 44.0 |    774.9 |    917.0 |          1.00 |
| `deno run --allow-read=./ --no-config --allow-net=deno.land 2020/15_object.js < /dev/null` |  66604.9 ± 486.9 |  65509.1 |  67279.6 |  82.68 ± 4.55 |
| `node 2020/15_object.js < /dev/null`                                                       | 98055.5 ± 1673.6 |  96037.8 | 100572.9 | 121.72 ± 6.96 |

- bun 1.0.13
- deno 1.38.2 (release, aarch64-apple-darwin)
- node v21.2.0
