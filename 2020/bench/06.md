## 2020/06.js

```
Part 1: 11
Part 2: 6
Part 1: 6504
Part 2: 3351
```

| Command                                                                             |  Mean [ms] | Min [ms] | Max [ms] |    Relative |
| :---------------------------------------------------------------------------------- | ---------: | -------: | -------: | ----------: |
| `bun run 2020/06.js < /dev/null`                                                    | 14.5 ± 0.5 |     13.7 |     17.6 |        1.00 |
| `deno run --allow-read=./ --no-config --allow-net=deno.land 2020/06.js < /dev/null` | 20.5 ± 1.0 |     19.5 |     24.6 | 1.41 ± 0.09 |
| `node 2020/06.js < /dev/null`                                                       | 38.0 ± 0.8 |     37.2 |     42.0 | 2.61 ± 0.11 |

- bun 1.0.13
- deno 1.38.2 (release, aarch64-apple-darwin)
- node v21.2.0
