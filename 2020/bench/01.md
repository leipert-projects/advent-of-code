## 2020/01.js

```
Part 1 – Example: 514579
Part 2 – Example: 241861950
Part 1 – Data: 691771
Part 2 – Data: 232508760
```

| Command                                                                             |  Mean [ms] | Min [ms] | Max [ms] |    Relative |
| :---------------------------------------------------------------------------------- | ---------: | -------: | -------: | ----------: |
| `bun run 2020/01.js < /dev/null`                                                    | 32.8 ± 3.4 |     30.0 |     51.0 | 1.63 ± 0.17 |
| `deno run --allow-read=./ --no-config --allow-net=deno.land 2020/01.js < /dev/null` | 20.1 ± 0.3 |     19.6 |     21.9 |        1.00 |
| `node 2020/01.js < /dev/null`                                                       | 37.2 ± 0.3 |     36.6 |     37.8 | 1.85 ± 0.03 |

- bun 1.0.13
- deno 1.38.2 (release, aarch64-apple-darwin)
- node v21.2.0
