## 2020/04.js

```
Part 1 – Example – 2 / 4 passports valid
Part 1 – Data – 247 / 299 passports valid
Part 2 – Example – 2 / 4 passports valid
Part 2 – Data – 145 / 299 passports valid
```

| Command                                                                             |  Mean [ms] | Min [ms] | Max [ms] |    Relative |
| :---------------------------------------------------------------------------------- | ---------: | -------: | -------: | ----------: |
| `bun run 2020/04.js < /dev/null`                                                    | 16.2 ± 1.7 |     15.0 |     31.2 |        1.00 |
| `deno run --allow-read=./ --no-config --allow-net=deno.land 2020/04.js < /dev/null` | 20.1 ± 0.6 |     19.2 |     22.6 | 1.25 ± 0.14 |
| `node 2020/04.js < /dev/null`                                                       | 38.0 ± 0.9 |     36.9 |     40.6 | 2.35 ± 0.26 |

- bun 1.0.13
- deno 1.38.2 (release, aarch64-apple-darwin)
- node v21.2.0
