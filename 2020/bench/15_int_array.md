## 2020/15_int_array.js

```
Part 1 – Example: 436
Part 1 – Data: 412
Part 2 – Data: 243
```

| Command                                                                                       |   Mean [ms] | Min [ms] | Max [ms] |    Relative |
| :-------------------------------------------------------------------------------------------- | ----------: | -------: | -------: | ----------: |
| `bun run 2020/15_int_array.js < /dev/null`                                                    | 370.2 ± 1.9 |    365.7 |    372.1 |        1.00 |
| `deno run --allow-read=./ --no-config --allow-net=deno.land 2020/15_int_array.js < /dev/null` | 382.6 ± 1.9 |    378.6 |    384.5 | 1.03 ± 0.01 |
| `node 2020/15_int_array.js < /dev/null`                                                       | 414.9 ± 2.1 |    411.3 |    418.6 | 1.12 ± 0.01 |

- bun 1.0.13
- deno 1.38.2 (release, aarch64-apple-darwin)
- node v21.2.0
