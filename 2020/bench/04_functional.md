## 2020/04_functional.js

```
Part 1: 247
Part 2: 145
```

| Command                                                                                        |  Mean [ms] | Min [ms] | Max [ms] |    Relative |
| :--------------------------------------------------------------------------------------------- | ---------: | -------: | -------: | ----------: |
| `bun run 2020/04_functional.js < /dev/null`                                                    | 16.2 ± 0.4 |     15.3 |     17.9 |        1.00 |
| `deno run --allow-read=./ --no-config --allow-net=deno.land 2020/04_functional.js < /dev/null` | 20.5 ± 1.3 |     19.6 |     28.9 | 1.27 ± 0.08 |
| `node 2020/04_functional.js < /dev/null`                                                       | 38.4 ± 0.4 |     37.8 |     39.5 | 2.37 ± 0.07 |

- bun 1.0.13
- deno 1.38.2 (release, aarch64-apple-darwin)
- node v21.2.0
