## 2020/15_map.js

```
Part 1 – Example: 436
Part 1 – Data: 412
Part 2 – Data: 243
```

| Command                                                                                 |      Mean [s] | Min [s] | Max [s] |    Relative |
| :-------------------------------------------------------------------------------------- | ------------: | ------: | ------: | ----------: |
| `bun run 2020/15_map.js < /dev/null`                                                    | 2.377 ± 0.038 |   2.342 |   2.473 |        1.00 |
| `deno run --allow-read=./ --no-config --allow-net=deno.land 2020/15_map.js < /dev/null` | 3.669 ± 0.085 |   3.557 |   3.804 | 1.54 ± 0.04 |
| `node 2020/15_map.js < /dev/null`                                                       | 3.502 ± 0.055 |   3.447 |   3.632 | 1.47 ± 0.03 |

- bun 1.0.13
- deno 1.38.2 (release, aarch64-apple-darwin)
- node v21.2.0
