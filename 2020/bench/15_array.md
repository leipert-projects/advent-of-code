## 2020/15_array.js

```
Part 1 – Example: 436
Part 1 – Data: 412
Part 2 – Data: 243
```

| Command                                                                                   |    Mean [ms] | Min [ms] | Max [ms] |    Relative |
| :---------------------------------------------------------------------------------------- | -----------: | -------: | -------: | ----------: |
| `bun run 2020/15_array.js < /dev/null`                                                    | 546.0 ± 17.8 |    534.0 |    585.4 |        1.00 |
| `deno run --allow-read=./ --no-config --allow-net=deno.land 2020/15_array.js < /dev/null` | 560.8 ± 10.3 |    546.0 |    574.8 | 1.03 ± 0.04 |
| `node 2020/15_array.js < /dev/null`                                                       | 587.9 ± 46.7 |    561.5 |    713.7 | 1.08 ± 0.09 |

- bun 1.0.13
- deno 1.38.2 (release, aarch64-apple-darwin)
- node v21.2.0
