import { dirname, loadFileIntoArray } from "../lib/shared.js";

const parse = async (file) =>
  (await loadFileIntoArray(file))
    .flatMap((x) => x.split(","))
    .map((x) => (x === "x" ? x : Number(x)));

const [exampleEarliest, ...exampleTimes] = await parse(
  `${dirname(import.meta.url)}/data/13_example.txt`,
);

const [dataEarliest, ...dataTimes] = await parse(`${dirname(import.meta.url)}/data/13.txt`);

const findNextBus = (earliest, times) => {
  let departure = earliest;

  const t = times.filter((x) => Number.isInteger(x));

  do {
    for (const x of t) {
      if (departure % x === 0) {
        return (departure - earliest) * x;
      }
    }

    departure += 1;
  } while (departure);
  return null;
};

console.log(
  `Part 1 – Example: ${
    findNextBus(exampleEarliest, exampleTimes)
  } minutes after ${exampleEarliest}`,
);
console.log(`Part 2 – Data: ${findNextBus(dataEarliest, dataTimes)} minutes after ${dataEarliest}`);

const getRemainderPairs = (times) =>
  times.flatMap((x, index) => {
    if (!Number.isInteger(x)) {
      return [];
    }
    return [[BigInt(x - index), BigInt(x)]];
  });

/**
 * Chinese Remainder Theorem
 * Algorithm thanks to: https://www.youtube.com/watch?v=oXPUe0c-iR0
 * Funny how we had to use BigInt :mind_blown:
 */
function CRT(times) {
  const remainderPairs = getRemainderPairs(times);
  const N = remainderPairs.reduce((acc, [, m]) => acc * m, 1n);
  let sum = 0n;

  /* eslint-disable camelcase */
  for (const [a_i, m_i] of remainderPairs) {
    const N_i = N / m_i;
    for (let i = 1n; i < m_i; i += 1n) {
      if ((i * N_i) % m_i === 1n) {
        sum += i * a_i * N_i;
        break;
      }
    }
  }
  /* eslint-enable camelcase */

  return sum % N;
}

console.log(`Part 2 – Example: ${CRT(exampleTimes)}`);
console.log(`Part 2 – Example: ${CRT([17, "x", 13, 19])}`);
console.log(`Part 2 – Example: ${CRT([67, 7, 59, 61])}`);
console.log(`Part 2 – Example: ${CRT([67, "x", 7, 59, 61])}`);
console.log(`Part 2 – Example: ${CRT([67, 7, "x", 59, 61])}`);
console.log(`Part 2 – Example: ${CRT([1789, 37, 47, 1889])}`);
console.log(`Part 2 – Data: ${CRT(dataTimes)}`);
