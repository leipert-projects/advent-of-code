import ConwaysGame from "../lib/ConwaysGame.js";
import { dirname, loadFile } from "../lib/shared.js";

const example = `.#.
..#
###`;

const data = await loadFile(`${dirname(import.meta.url)}/data/17.txt`);

const conwaysGameOfCubes = (input, steps, dimension = 3) => {
  const start = input.split("\n").map((x) => x.split(""));

  const conwaysGame = new ConwaysGame(dimension);

  const coord = Array(dimension).fill(0);

  start.forEach((line, y) => {
    line.forEach((cell, x) => {
      if (cell === "#") {
        coord[0] = x;
        coord[1] = y;
        conwaysGame.set(true, ...coord);
      }
    });
  });

  if (dimension < 4) {
    console.log(conwaysGame.render().join("\n\n"));
  }

  conwaysGame.play(steps, new Set([2, 3]), new Set([3]));

  let count = 0;
  conwaysGame.forEach(() => {
    count += 1;
  });
  return count;
};

console.log(`Part 1 – Example: ${conwaysGameOfCubes(example, 6)}`);
console.log(`Part 1 – Data: ${conwaysGameOfCubes(data, 6)}`);

console.log(`Part 2 – Example: ${conwaysGameOfCubes(example, 6, 4)}`);
console.log(`Part 2 – Data: ${conwaysGameOfCubes(data, 6, 4)}`);
