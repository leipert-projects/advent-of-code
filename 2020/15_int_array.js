import { tripDownMemoryGameInt32Array } from "./lib/15_memorygame.js";

console.log(`Part 1 – Example: ${tripDownMemoryGameInt32Array([0, 3, 6])}`);
console.log(`Part 1 – Data: ${tripDownMemoryGameInt32Array([10, 16, 6, 0, 1, 17])}`);
console.log(`Part 2 – Data: ${tripDownMemoryGameInt32Array([10, 16, 6, 0, 1, 17], 30000000)}`);
