// eslint-disable-next-line max-classes-per-file
import { dirname, loadFileIntoArray } from "../lib/shared.js";

const data = await loadFileIntoArray(`${dirname(import.meta.url)}/data/21.txt`);
const example = await loadFileIntoArray(`${dirname(import.meta.url)}/data/21_example.txt`);

function parse(lines) {
  return lines.map((line) => {
    const [i, a] = line.split(/ \(contains |\)/g);
    const ingredients = i.split(" ");
    const allergens = a.split(", ");
    return { ingredients, allergens };
  });
}

function countSafeIngredients(dishes, allergenMap) {
  const allergens = allergenMap.map(([, ingredient]) => ingredient);
  return dishes.flatMap((dish) => dish.ingredients).filter((x) => !allergens.includes(x)).length;
}

function extractAllergens(dishes) {
  const allergenMap = {};
  for (const { ingredients, allergens } of dishes) {
    for (const allergeen of allergens) {
      if (!allergenMap[allergeen]) {
        allergenMap[allergeen] = ingredients;
      }
      allergenMap[allergeen] = allergenMap[allergeen].filter((x) => ingredients.includes(x));
    }
  }

  let allergenCandidates = [...Object.entries(allergenMap)];

  const defMap = [];

  do {
    const [allergen, candidates] = allergenCandidates.pop();

    if (candidates.length === 1) {
      defMap.push([allergen, ...candidates]);
      allergenCandidates = allergenCandidates.map(([a, c]) => [
        a,
        c.filter((x) => x !== candidates[0]),
      ]);
    } else {
      allergenCandidates.unshift([allergen, candidates]);
    }
  } while (allergenCandidates.length);

  return defMap.sort(([a1], [a2]) => (a1 < a2 ? -1 : 1));
}

const exampleDishes = parse(example);
const exampleAllergens = extractAllergens(exampleDishes);
console.log(`Part 1 – Example: ${countSafeIngredients(exampleDishes, exampleAllergens)}`);

const dataDishes = parse(data);
const dataAllergens = extractAllergens(dataDishes);
console.log(`Part 1 – Data: ${countSafeIngredients(dataDishes, dataAllergens)}`);

console.log(`Part 2 – Example: ${exampleAllergens.map(([, ingredient]) => ingredient)}`);
console.log(`Part 2 – Data: ${dataAllergens.map(([, ingredient]) => ingredient)}`);
