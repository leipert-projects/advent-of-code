class CrabGame {
  constructor(input, length = null) {
    this.input = input;
    this.length = length || input.length;
  }

  initialize() {
    let prev = null;
    let firstEntry;
    const entries = this.input.split("");
    this.cups = new Array(this.length + 1);

    for (let i = 1; i <= this.length; i += 1) {
      const value = Number(entries[i - 1] || i);
      const val = { value };
      if (prev) {
        prev.next = val;
      }
      prev = val;
      if (i === 1) {
        firstEntry = val;
      }
      this.cups[value] = val;
    }
    prev.next = firstEntry;
    this.currentCup = firstEntry.value;
  }

  position() {
    return this.cups.indexOf(this.currentCup);
  }

  nextStep() {
    const curr = this.cups[this.currentCup];
    const removed = curr.next;
    curr.next = removed.next.next.next;
    const removedValues = [removed.value, removed.next.value, removed.next.next.value];
    let destination = curr.value;
    do {
      destination -= 1;
      if (destination < 1) {
        destination = this.length;
      }
    } while (removedValues.includes(destination));
    const right = this.cups[destination].next;
    this.cups[destination].next = removed;
    removed.next.next.next = right;
    this.currentCup = curr.next.value;
  }

  play(steps, resultCount) {
    this.initialize();
    for (let i = 0; i < steps; i += 1) {
      this.nextStep();
    }
    let curr = this.cups["1"].next;
    const result = [];
    let count = resultCount;
    do {
      result.push(curr.value);
      curr = curr.next;
      count -= 1;
    } while (count > 0);
    return result;
  }
}

const exampleGame = new CrabGame("389125467");
console.log(`Part 1 – Example: ${exampleGame.play(100, 8).join("")}`);

const part1Game = new CrabGame("157623984");
console.log(`Part 1 – Data: ${part1Game.play(100, 8).join("")}`);

const exampleGame2 = new CrabGame("389125467", 1_000_000);
let [res1, res2] = exampleGame2.play(10_000_000, 2);
console.log(`Part 2 – Example: ${res1 * res2}`);

const part2Game = new CrabGame("157623984", 1_000_000);
[res1, res2] = part2Game.play(10_000_000, 2);
console.log(`Part 2 – Data: ${res1 * res2}`);
