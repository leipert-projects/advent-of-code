const transform = (subjectNumber, value = 1) => (subjectNumber * value) % 20201227;

const findLoopSizes = (...input) => {
  let loop = 0;
  let val = 1;
  const result = new Map();
  do {
    val = transform(7, val);
    loop += 1;
    if (input.includes(val)) {
      result.set(val, loop);
    }
  } while (result.size < input.length);

  return result;
};

const getEncryptionKey = (cardKey, doorKey) => {
  const loopSizes = findLoopSizes(cardKey, doorKey);
  const loop = loopSizes.get(doorKey);
  let val = 1;
  for (let i = 0; i < loop; i += 1) {
    val = transform(cardKey, val);
  }
  return val;
};

console.log(`Example ${getEncryptionKey(5764801, 17807724)}`);
console.log(`Data ${getEncryptionKey(11404017, 13768789)}`);
