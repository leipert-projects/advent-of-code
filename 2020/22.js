// eslint-disable-next-line max-classes-per-file
import { dirname, loadFileIntoArray } from "../lib/shared.js";

const data = await loadFileIntoArray(`${dirname(import.meta.url)}/data/22.txt`);
const example = await loadFileIntoArray(`${dirname(import.meta.url)}/data/22_example.txt`);

function buildDecks(lines) {
  let c = -1;
  const decks = [];
  lines.forEach((line) => {
    if (line.includes("Player")) {
      c += 1;
      decks[c] = [];
    } else if (/\d/.test(line)) {
      decks[c].push(Number(line));
    }
  });
  return decks;
}

function play([p1, p2], recursiveCombat) {
  const player1 = [...p1];
  const player2 = [...p2];
  const prevRounds = [];
  do {
    const [card1, card2] = [player1.shift(), player2.shift()];

    let didPlayerOneWin = card1 > card2;

    if (recursiveCombat) {
      const key = [player1.join(), player2.join()].join("|");
      if (prevRounds.includes(key)) {
        return [true, false, 0];
      }
      prevRounds.push(key);
      if (card1 <= player1.length && card2 <= player2.length) {
        [didPlayerOneWin] = play([player1.slice(0, card1), player2.slice(0, card2)], true);
      }
    }

    if (didPlayerOneWin) {
      player1.push(card1, card2);
    } else {
      player2.push(card2, card1);
    }
  } while (player1.length > 0 && player2.length > 0);

  const score = [...player1, ...player2].reduce(
    (acc, curr, index, arr) => acc + curr * (arr.length - index),
    0,
  );

  return [player1.length > 0, player2.length > 0, score];
}

const exampleDeck = buildDecks(example);
console.log(`Part 1 – Example: ${play(exampleDeck)[2]}`);

const dataDeck = buildDecks(data);
console.log(`Part 1 – Data: ${play(dataDeck)[2]}`);

const loopDeck = buildDecks(["Player 1:", "43", "19", "Player 2:", "2", "29", "14"]);
console.log(`Part 2 – Example: ${play(loopDeck, true)}`);

console.log(`Part 2 – Example: ${play(exampleDeck, true)[2]}`);
console.log(`Part 2 – Data: ${play(dataDeck, true)[2]}`);
