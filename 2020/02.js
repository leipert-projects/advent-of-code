import { dirname, loadFileIntoArray } from "../lib/shared.js";

const data = await loadFileIntoArray(`${dirname(import.meta.url)}/data/02.txt`);

const example = ["1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"];

const ruleExtractor = /(\d+)-(\d+)\s+([a-z]):\s([a-z]+)/;

const oldPasswordRule = (input) => {
  const [, min, max, letter, pw] = input.match(ruleExtractor);

  const matcher = new RegExp(letter, "g");

  const count = pw.length - pw.replace(matcher, "").length;

  return min <= count && count <= max;
};

const newPasswordRule = (input) => {
  const [, pos1, pos2, letter, pw] = input.match(ruleExtractor);

  return (
    (pw[pos1 - 1] === letter && pw[pos2 - 1] !== letter) ||
    (pw[pos1 - 1] !== letter && pw[pos2 - 1] === letter)
  );
};

try {
  console.log(`Part 1 – Example – Valid Passwords: ${example.filter(oldPasswordRule).length}`);
  console.log(`Part 1 – Data – Valid Passwords: ${data.filter(oldPasswordRule).length}`);
  console.log(`Part 2 – Example – Valid Passwords: ${example.filter(newPasswordRule).length}`);
  console.log(`Part 2 – Data – Valid Passwords: ${data.filter(newPasswordRule).length}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  process.exit(1);
}
