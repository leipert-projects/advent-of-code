import { dirname, loadIntegerIntoArray } from "../lib/shared.js";

const example = await loadIntegerIntoArray(`${dirname(import.meta.url)}/data/10_example.txt`);
const data = await loadIntegerIntoArray(`${dirname(import.meta.url)}/data/10.txt`);

const joltage = (input) => {
  const stack = input.sort((a, b) => b - a);

  const diff = [0, 0, 0, 1];

  for (let i = 0; i < stack.length; i += 1) {
    const elem = stack[i];
    const prev = stack[i + 1] || 0;
    diff[elem - prev] += 1;
  }

  return diff;
};

console.log(`Part 1 – Example: ${joltage(example)}`);
console.log(`Part 1 – Data: ${joltage(data)}`);

function findAllPathsInGraph(graph, to, from, path = []) {
  if (to === from) {
    return [...path, from];
  }

  return graph.get(from).flatMap((x) => {
    const allPaths = findAllPathsInGraph(graph, to, x, [...path, from]);
    return Array.isArray(allPaths[0]) ? allPaths : [allPaths];
  });
}

function splitIntoSubgraphsAndPermutate(input) {
  const stack = input.sort((a, b) => b - a);

  stack.unshift(stack[0] + 3);
  stack.push(0);

  const graph = new Map();

  const subGraphs = [];

  stack.reverse().forEach((key) => {
    const targets = [1, 2, 3].flatMap((x) => (stack.includes(x + key) ? [x + key] : []));
    graph.set(key, targets);
    if (targets.length === 1) {
      if (graph.size > 1) {
        subGraphs.push(findAllPathsInGraph(graph, Math.max(...targets), Math.min(...graph.keys())));
      }
      graph.clear();
    }
  });

  return subGraphs;
}

console.log(
  `Part 2 – Example: ${
    splitIntoSubgraphsAndPermutate(example).reduce(
      (acc, curr) => acc * curr.length,
      1,
    )
  }`,
);
console.log(
  `Part 2 – Data: ${
    splitIntoSubgraphsAndPermutate(data).reduce(
      (acc, curr) => acc * curr.length,
      1,
    )
  }`,
);
