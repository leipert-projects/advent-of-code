// eslint-disable-next-line max-classes-per-file
import Map2D from "../lib/Map2D.js";
import { dirname, loadFileIntoArray } from "../lib/shared.js";

const data = await loadFileIntoArray(`${dirname(import.meta.url)}/data/20.txt`);
const example = await loadFileIntoArray(`${dirname(import.meta.url)}/data/20_example.txt`);

const TITLE = /Tile (\d+):/;

class Tile {
  constructor(id) {
    this.id = Number(id);
    this.lines = [];
  }

  addLine(line) {
    this.lines.push(line.split(""));
  }

  edges() {
    const [left, right] = this.lines.reduce(
      (acc, line) => {
        acc[0] = line[0] + acc[0];
        acc[1] += line[line.length - 1];
        return acc;
      },
      ["", ""],
    );

    return [this.lines[0].join(""), right, this.lines[this.lines.length - 1].join(""), left];
  }

  setNeighbors(edgeMap) {
    const edges = this.edges();
    this.neighbors = edges.map((edge) => edgeMap.get(edge).filter((x) => x !== this.id)[0] || -1);
    this.neighborCount = this.neighbors.filter((x) => x > 0).length;
  }

  toString() {
    return this.lines.map((x) => x.join("")).join("\n");
  }

  rotate() {
    this.lines = this.lines.map((row, i) =>
      row.map((_val, j) => this.lines[this.lines.length - 1 - j][i])
    );
  }

  flip() {
    this.lines = this.lines.map((x) => x.reverse());
  }

  rotateIntoPosition({ top = null, left = null, right = null, bottom = null }) {
    let count = 0;
    do {
      this.rotate();
      this.neighbors.unshift(this.neighbors.pop());
      count += 1;
      if (count % 4 === 0) {
        this.flip();
        this.neighbors = [
          this.neighbors[0],
          this.neighbors[3],
          this.neighbors[2],
          this.neighbors[1],
        ];
      }
    } while (
      count < 8 &&
      !(
        (top === null || this.neighbors[0] === top) &&
        (left === null || this.neighbors[3] === left) &&
        (bottom === null || this.neighbors[2] === bottom) &&
        (right === null || this.neighbors[1] === right)
      )
    );
  }
}

class Image extends Tile {
  // deno-fmt-ignore
  static SEA_MONSTER = [
    [-1, 18],
    [0, 0], [0, 5], [0, 6], [0, 5], [0, 6],
    [0, 11], [0, 12], [0, 17], [0, 18], [0, 19],
    [1, 1], [1, 4], [1, 7], [1, 10], [1, 13], [1, 16],
  ];

  constructor() {
    super(1);
    delete this.id;
  }

  commit() {
    this.tempLines.forEach((x) => this.addLine(x.join("")));
    this.tempLines = null;
  }

  addRight(lines) {
    if (!this.tempLines) {
      this.tempLines = Array(lines.length - 2)
        .fill("")
        .map(() => []);
    }

    for (let x = 0; x < lines.length - 2; x += 1) {
      this.tempLines[x] = [...this.tempLines[x], ...lines[x + 1].slice(1, -1)];
    }
  }

  isSeaMonster(i, j) {
    return Image.SEA_MONSTER.every(([x, y]) => this.lines[i + x][j + y] === "#");
  }

  findSeaMonsters() {
    const { length } = this.lines[0];

    const seaMonsters = [];

    for (let i = 1; i < this.lines.length - 1; i += 1) {
      for (let j = 0; j < length - 20; j += 1) {
        if (this.isSeaMonster(i, j)) {
          seaMonsters.push([i, j]);
        }
      }
    }

    return seaMonsters;
  }

  findAndMarkSeeMonsters() {
    let count = 0;

    let seaMonsters;

    do {
      seaMonsters = this.findSeaMonsters();
      if (seaMonsters.length > 0) {
        break;
      }
      count += 1;
      this.rotate();
      if (count === 4) {
        this.flip();
      }
    } while (count < 8);

    seaMonsters.forEach(([x, y]) => {
      Image.SEA_MONSTER.forEach(([i, j]) => {
        this.lines[i + x][j + y] = "O";
      });
    });
  }
}

function buildEdgeMap(tiles) {
  const edgeMap = new Map();

  tiles.forEach((tile) => {
    tile.edges().forEach((edge) => {
      let candidates = edgeMap.get(edge) || [];
      candidates.push(tile.id);
      edgeMap.set(edge, candidates);
      const reverse = edge.split("").reverse().join("");
      candidates = edgeMap.get(reverse) || [];
      candidates.push(tile.id);
      edgeMap.set(reverse, candidates);
    });
  });

  return edgeMap;
}

function parseTiles(input) {
  let tile = null;
  const tiles = new Map();
  for (const line of input) {
    if (TITLE.test(line)) {
      if (tile) {
        tiles.set(tile.id, tile);
      }
      const [, id] = line.match(TITLE);
      tile = new Tile(id);
    } else if (line.length) {
      tile.addLine(line);
    }
  }
  tiles.set(tile.id, tile);
  const edgeMap = buildEdgeMap(tiles);
  tiles.forEach((t) => t.setNeighbors(edgeMap));

  return tiles;
}

function findCorners(tiles) {
  return [...tiles.values()].filter((tile) => tile.neighborCount === 2).map((x) => x.id);
}

function calculateRoughness(tiles) {
  const map = new Map2D();

  let x = 0;
  let y = 0;

  let { id } = [...tiles.values()].find((x) => x.neighborCount === 2);
  let c = 0;
  let curr;
  const result = new Image();
  do {
    map.set(x, y, id);
    curr = tiles.get(id);
    curr.rotateIntoPosition({ top: map.get(x, y - 1) || -1, left: map.get(x - 1, y) || -1 });
    result.addRight(curr.lines);
    // eslint-disable-next-line prefer-destructuring
    id = curr.neighbors[1];
    if (id === -1) {
      result.commit();
      x = 0;
      // eslint-disable-next-line prefer-destructuring
      id = tiles.get(map.get(x, y)).neighbors[2];
      y += 1;
    } else {
      x += 1;
    }
    c += 1;
  } while (c < tiles.size);

  result.findAndMarkSeeMonsters();

  return result.lines.reduce((acc, line) => acc + line.filter((char) => char === "#").length, 0);
}

const exampleTiles = parseTiles(example);
console.log(`Part 1 – Example: ${findCorners(exampleTiles).reduce((x = 1, y) => x * y, 1)}`);

const dataTiles = parseTiles(data);
console.log(`Part 1 - Data: ${findCorners(dataTiles).reduce((x = 1, y) => x * y, 1)}`);
console.log(`Part 2 – Example: ${calculateRoughness(exampleTiles)}`);
console.log(`Part 2 – Data: ${calculateRoughness(dataTiles)}`);
