import { dirname, loadFileIntoArray } from "../lib/shared.js";

const data = await loadFileIntoArray(`${dirname(import.meta.url)}/data/12.txt`);

const example = ["F10", "N3", "F7", "R90", "F11"];

const manhattanDistance = ([x2, y2], [x1, y1] = [0, 0]) => Math.abs(x2 - x1) + Math.abs(y2 - y1);

function boat(instructions) {
  // East
  let direction = 90;
  let x = 0;
  let y = 0;

  instructions.forEach((inst) => {
    const command = inst[0];
    const value = Number(inst.substring(1));
    switch (command) {
      case "N":
        y += value;
        break;
      case "S":
        y -= value;
        break;
      case "E":
        x += value;
        break;
      case "W":
        x -= value;
        break;
      case "L":
        direction -= value;
        break;
      case "R":
        direction += value;
        break;
      case "F":
        x += Math.round(Math.sin(direction * (Math.PI / 180))) * value;
        y += Math.round(Math.cos(direction * (Math.PI / 180))) * value;
        break;
      default:
        console.log(`Unknown command ${command}`);
    }
  });

  return manhattanDistance([x, y]);
}

function boatWithWaypoint(instructions) {
  let x = 0;
  let y = 0;

  let waypoint = [10, 1];

  instructions.forEach((inst) => {
    const command = inst[0];
    let value = Number(inst.substring(1));
    switch (command) {
      case "N":
        waypoint[1] += value;
        break;
      case "S":
        waypoint[1] -= value;
        break;
      case "E":
        waypoint[0] += value;
        break;
      case "W":
        waypoint[0] -= value;
        break;
      case "R":
        value = (360 - value) % 360;
      // eslint-disable-next-line no-fallthrough
      case "L":
        if (value === 90) {
          waypoint = waypoint.reverse();
          waypoint[0] *= -1;
        } else if (value === 180) {
          waypoint[0] *= -1;
          waypoint[1] *= -1;
        } else if (value === 270) {
          waypoint = waypoint.reverse();
          waypoint[1] *= -1;
        }

        break;
      case "F":
        x += value * waypoint[0];
        y += value * waypoint[1];
        break;
      default:
        console.log(`Unknown command ${command}`);
    }
  });

  return manhattanDistance([x, y]);
}

console.log(`Part 1 – Example: ${boat(example)}`);
console.log(`Part 1 – Data: ${boat(data)}`);
console.log(`Part 2 – Example: ${boatWithWaypoint(example)}`);
console.log(`Part 2 – Data: ${boatWithWaypoint(data)}`);
