import { dirname, loadFileIntoArray, toInteger } from "../lib/shared.js";

const data = (await loadFileIntoArray(`${dirname(import.meta.url)}/data/01.txt`)).map(toInteger);

const example = [1721, 979, 366, 299, 675, 1456];

const findPair = (sum, a) => {
  const input = a.sort();

  for (let i = 0; i < input.length; i += 1) {
    const rest = sum - input[i];

    if (input.includes(rest)) {
      return [input[i], rest];
    }
  }

  return false;
};

const findTriple = (sum, input) => {
  for (let i = 0; i < input.length; i += 1) {
    const rest = sum - input[i];

    const restArray = [...input];

    restArray.splice(i, 1);

    const pair = findPair(rest, restArray);

    if (pair) {
      return [input[i], ...pair];
    }
  }

  return false;
};

try {
  const example1 = findPair(2020, example);
  console.log(`Part 1 – Example: ${example1[0] * example1[1]}`);

  const example2 = findTriple(2020, example);
  console.log(`Part 2 – Example: ${example2[0] * example2[1] * example2[2]}`);

  const part1 = findPair(2020, data);
  console.log(`Part 1 – Data: ${part1[0] * part1[1]}`);

  const part2 = findTriple(2020, data);
  console.log(`Part 2 – Data: ${part2[0] * part2[1] * part2[2]}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  process.exit(1);
}
