import { dirname, loadFileIntoArray } from "../lib/shared.js";

const data = await loadFileIntoArray(`${dirname(import.meta.url)}/data/18.txt`);

const createShuntingYard = (precedence) => (input) => {
  const stack = [];
  const output = [];

  const operators = ["*", "+"];

  input.split("").forEach((token) => {
    if (/\d+/.test(token)) {
      output.push(Number(token));
    } else if (operators.includes(token)) {
      if (operators.includes(stack[0]) && precedence(token) <= precedence(stack[0])) {
        output.push(stack.shift());
      }
      stack.unshift(token);
    } else if (token === "(") {
      stack.unshift(token);
    } else if (token === ")") {
      while (stack[0] !== "(") {
        output.push(stack.shift());
      }
      stack.shift();
    }
  });

  return [...output, ...stack];
};

function evaluate(RPN) {
  return RPN.reduce((stack, curr) => {
    if (Number.isInteger(curr)) {
      stack.push(curr);
    } else {
      const right = stack.pop();
      const left = stack.pop();
      if (curr === "+") {
        stack.push(left + right);
      } else if (curr === "*") {
        stack.push(left * right);
      }
    }

    return stack;
  }, [])[0];
}

console.log(
  `Part 1 – Data: ${
    data
      .map(createShuntingYard(() => 1))
      .reduce((sum, curr) => sum + evaluate(curr), 0)
  }`,
);
console.log(
  `Part 2 – Data: ${
    data
      .map(createShuntingYard((token) => (token === "+" ? 2 : 1)))
      .reduce((sum, curr) => sum + evaluate(curr), 0)
  }`,
);
