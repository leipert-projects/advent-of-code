import Hill from "./lib/03_hill.js";

import { dirname, loadFileIntoArray } from "../lib/shared.js";

const example = (await loadFileIntoArray(`${dirname(import.meta.url)}/data/03_example.txt`)).map((
  x,
) => x.split(""));
const data = (await loadFileIntoArray(`${dirname(import.meta.url)}/data/03.txt`)).map((x) =>
  x.split("")
);

try {
  const ExampleHill = new Hill(example);
  const DataHill = new Hill(data);

  console.log(`Part 1 – Example – ${ExampleHill.walkAndHitTrees(3, 1)}`);
  console.log(`Part 1 – Data – ${DataHill.walkAndHitTrees(3, 1)}`);

  const slopes = [
    [1, 1],
    [3, 1],
    [5, 1],
    [7, 1],
    [1, 2],
  ];

  const exampleHill = slopes.reduce(
    (product, curr) => product * ExampleHill.walkAndHitTrees(...curr),
    1,
  );
  const dataHill = slopes.reduce((product, curr) => product * DataHill.walkAndHitTrees(...curr), 1);

  console.log(`Part 2 – Example – ${exampleHill}`);
  console.log(`Part 2 – Data – ${dataHill}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  process.exit(1);
}
