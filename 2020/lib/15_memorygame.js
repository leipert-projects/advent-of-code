/**
 * Keeping track of the last position with an Array(n)
 *
 node 2020/15_array.js   1.22s  user 0.23s system 100% cpu 1.431 total
 avg shared (code):         0 KB
 avg unshared (data/stack): 0 KB
 total (sum):               0 KB
 max memory:                256132 KB
 page faults from disk:     0
 other page faults:         126453
 *
 */
export const tripDownMemoryGameArray = (initial = [], playUntil = 2020) => {
  let lastNumber = 0;

  const lastIndexOf = Array(playUntil);

  let i = 0;

  for (i; i < initial.length; i += 1) {
    lastNumber = initial[i];
    lastIndexOf[lastNumber] = i + 1;
  }

  for (i; i < playUntil; i += 1) {
    const lastIndex = lastIndexOf[lastNumber];
    lastIndexOf[lastNumber] = i;
    lastNumber = lastIndex ? i - lastIndex : 0;
  }

  return lastNumber;
};
/**
 * Keeping track of the last position with a Map
 *
 node 2020/15_map.js   5.82s  user 0.24s system 100% cpu 6.045 total
 avg shared (code):         0 KB
 avg unshared (data/stack): 0 KB
 total (sum):               0 KB
 max memory:                253320 KB
 page faults from disk:     0
 other page faults:         125631
 *
 */
export const tripDownMemoryGameMap = (initial = [], playUntil = 2020) => {
  let lastNumber = 0;

  const lastIndexOf = new Map();

  let i = 0;

  for (i; i < initial.length; i += 1) {
    lastNumber = initial[i];
    lastIndexOf.set(lastNumber, i + 1);
  }

  for (i; i < playUntil; i += 1) {
    const lastIndex = lastIndexOf.get(lastNumber);
    lastIndexOf.set(lastNumber, i);
    lastNumber = lastIndex ? i - lastIndex : 0;
  }

  return lastNumber;
};
/**
 * Keeping track of the last position with an Array(n)
 *
 node 2020/15_object.js   191.32s  user 6.58s system 103% cpu 3:10.41 total
 avg shared (code):         0 KB
 avg unshared (data/stack): 0 KB
 total (sum):               0 KB
 max memory:                4295904 KB
 page faults from disk:     0
 other page faults:         4291335
 *
 */
export const tripDownMemoryGameObject = (initial = [], playUntil = 2020) => {
  let lastNumber = 0;

  const lastIndexOf = {};

  let i = 0;

  for (i; i < initial.length; i += 1) {
    lastNumber = initial[i];
    lastIndexOf[lastNumber] = i + 1;
  }

  for (i; i < playUntil; i += 1) {
    const lastIndex = lastIndexOf[lastNumber];
    lastIndexOf[lastNumber] = i;
    lastNumber = lastIndex ? i - lastIndex : 0;
  }

  return lastNumber;
};
/**
 * Keeping track of the whole array –––––– sloooooow
 */
export const tripDownMemoryGameArrayNaive = (initial = [], playUntil = 2020) => {
  let lastNumber = 0;

  const lastIndexOf = [];

  let i = 0;

  for (i; i < initial.length; i += 1) {
    lastNumber = initial[i];
    lastIndexOf[i + 1] = lastNumber;
  }

  for (i; i < playUntil; i += 1) {
    const lastIndex = lastIndexOf.lastIndexOf(lastNumber);
    lastIndexOf[i] = lastNumber;
    lastNumber = lastIndex === -1 ? 0 : i - lastIndex;
  }

  return lastNumber;
};
/**
 * Keeping track of the last position with an Int32Array(n)
 *
 node 2020/15_int_array.js   1.00s  user 0.21s system 101% cpu 1.196 total
 avg shared (code):         0 KB
 avg unshared (data/stack): 0 KB
 total (sum):               0 KB
 max memory:                131864 KB
 page faults from disk:     0
 *
 */
export const tripDownMemoryGameInt32Array = (initial = [], playUntil = 2020) => {
  let lastNumber = 0;

  const lastIndexOf = new Int32Array(playUntil);

  let i = 0;

  for (i; i < initial.length; i += 1) {
    lastNumber = initial[i];
    lastIndexOf[lastNumber] = i + 1;
  }

  for (i; i < playUntil; i += 1) {
    const lastIndex = lastIndexOf[lastNumber];
    lastIndexOf[lastNumber] = i;
    lastNumber = lastIndex ? i - lastIndex : 0;
  }

  return lastNumber;
};
