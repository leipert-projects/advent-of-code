const INSTRUCTION = /(\w{3})\s+([+-]\d+)/;

export default class Console {
  constructor(instructions) {
    this.instructionsRaw = instructions;
    this.bootstrap();
  }

  bootstrap() {
    this.halted = false;
    this.accumulator = 0;
    this.pointer = 0;
    this.instructions = this.instructionsRaw.map((line) => {
      const [, command, param] = line.match(INSTRUCTION);
      return {
        command,
        param: Number(param),
      };
    });
    this.visits = new Array(this.instructions.length).fill(0);
  }

  setInstruction(int, command) {
    this.instructions[int].command = command;
  }

  *execute() {
    do {
      const { pointer } = this;
      const curr = this.instructions[pointer];
      this.visits[pointer] += 1;
      switch (curr.command) {
        case "acc":
          this.pointer += 1;
          this.accumulator += curr.param;
          break;
        case "nop":
          this.pointer += 1;
          break;
        case "jmp":
          this.pointer += curr.param;
          break;
        default:
          throw new Error("yay");
      }

      yield {
        instruction: curr,
        pointer,
        accumulator: this.accumulator,
        visits: this.visits[pointer],
      };

      if (this.pointer >= this.instructions.length) {
        this.halted = true;
      }
    } while (!this.halted);
  }

  runProgramWithLoopDetection() {
    let curr = null;
    for (const value of this.execute()) {
      const { accumulator, visits } = value;
      if (visits >= 2) {
        break;
      }
      curr = accumulator;
    }
    return [this.halted, curr];
  }
}
