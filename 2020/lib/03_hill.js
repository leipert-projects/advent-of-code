export default class Hill {
  constructor(layout) {
    this.trees = {};

    this.width = layout[0].length;
    this.length = layout.length;

    layout.forEach((row, y) => {
      row.forEach((column, x) => {
        if (column === "#") {
          this.addTree(x, y);
        }
      });
    });
  }

  addTree(x, y) {
    if (!this.trees[y]) {
      this.trees[y] = [];
    }
    this.trees[y].push(x);
  }

  isTree(x, y) {
    const curr = this.trees[y] || [];
    return curr.includes(x % this.width);
  }

  walkAndHitTrees(diffX, diffY) {
    let trees = 0;
    let posX = 0;
    let posY = 0;
    do {
      trees += this.isTree(posX, posY);
      posX += diffX;
      posY += diffY;
    } while (posY < this.length);

    return trees;
  }
}
