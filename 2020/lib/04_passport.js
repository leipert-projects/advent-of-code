const HEIGHT_REGEX = /^((\d{3})cm|(\d{2})in)$/;

export default class Passport {
  static requiredFields = {
    byr: (str) => /^\d{4}$/.test(str) && str >= "1920" && str <= "2002",
    iyr: (str) => /^\d{4}$/.test(str) && str >= "2010" && str <= "2020",
    eyr: (str) => /^\d{4}$/.test(str) && str >= "2020" && str <= "2030",
    hgt: (str) => {
      const match = str && str.match(HEIGHT_REGEX);

      return (
        Boolean(match) &&
        ((match[2] >= "150" && match[2] <= "193") || (match[3] >= "59" && match[3] <= "76"))
      );
    },
    hcl: (str) => /^#[0-9a-f]{6}$/.test(str),
    ecl: (str) => ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].includes(str),
    pid: (str) => /^\d{9}$/.test(str),
  };

  static optionalFields = ["cid"];

  constructor(input) {
    const fields = input.split(/\s+/m).map((x) => x.split(":"));
    Object.assign(this, Object.fromEntries([...fields]));
  }

  hasAllRequiredFields() {
    return Object.keys(Passport.requiredFields).every((field) => Boolean(this[field]));
  }

  isValid() {
    return Object.entries(Passport.requiredFields).every(([field, check]) => check(this[field]));
  }
}
