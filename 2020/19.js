import { dirname, loadFileIntoArray } from "../lib/shared.js";

const example = await loadFileIntoArray(`${dirname(import.meta.url)}/data/19_example.txt`);
const data = await loadFileIntoArray(`${dirname(import.meta.url)}/data/19.txt`);

const RULE_REGEX = /^(\d+): (.+)/;

function processFile(input) {
  const rules = [];
  const messages = [];
  for (const line of input) {
    if (RULE_REGEX.test(line)) {
      const [, ordinal, rule] = line.match(RULE_REGEX);
      rules[Number(ordinal)] = rule.split(" ").map((x) => {
        const tryNumber = Number(x);
        return Number.isInteger(tryNumber) ? tryNumber : x.replace(/"/g, "");
      });
    } else if (line.length > 0) {
      messages.push(line);
    }
  }
  return { rules, messages };
}

function checkMessages({ rules, messages }, withFix = false) {
  const ruleCache = {};

  function buildRule(num) {
    if (ruleCache[num]) {
      return ruleCache[num];
    }
    let curr = rules[num];

    if (curr.includes("|")) {
      curr.unshift("(");
      curr.push(")");
    }

    if (withFix) {
      if (withFix && num === 8) {
        curr = ["(", 42, ")+"];
      } else if (withFix && num === 11) {
        // Dirty hack, doesn't work for looooonger strings
        // n=4 seems enough, added n=5 for good measure
        // deno-fmt-ignore
        curr = [
          "(",
          42, 31,
          "|",
          42, 42,
          31, 31,
          "|",
          42, 42, 42,
          31, 31, 31,
          "|",
          42, 42, 42, 42,
          31, 31, 31, 31,
          "|",
          42, 42, 42, 42, 42,
          31, 31, 31, 31, 31,
          ")",
        ];
      }
    }

    ruleCache[num] = curr
      .flatMap((x) => {
        if (Number.isInteger(x)) {
          return buildRule(x);
        }
        return x;
      })
      .join("");

    if (["(a|b)", "(b|a)"].includes(ruleCache[num])) {
      ruleCache[num] = ["[ab]"];
    }

    return ruleCache[num];
  }

  const rule = buildRule(0);

  const regex = new RegExp(`^${rule}$`);

  return messages.filter((x) => regex.test(x)).length;
}

const exampleParsed = processFile(example);
const dataParsed = processFile(data);

console.log(`Part 1 – Example: ${checkMessages(exampleParsed)}`);
console.log(`Part 1 – Data: ${checkMessages(dataParsed)}`);

console.log(`Part 2 – Example: ${checkMessages(exampleParsed, true)}`);
console.log(`Part 2 – Data: ${checkMessages(dataParsed, true)}`);
