import { dirname, loadFileIntoArray } from "../lib/shared.js";

const input = await loadFileIntoArray(`${dirname(import.meta.url)}/data/14.txt`);
const examplePart2 = await loadFileIntoArray(`${dirname(import.meta.url)}/data/14_example_2.txt`);

const createBitMask = (mask) => {
  const ONE_MASK = BigInt(`0b${mask.replace(/X/g, "0")}`);
  const ZERO_MASK = BigInt(`0b${mask.replace(/X/g, "1")}`);

  // eslint-disable-next-line no-bitwise
  return (biginit) => ZERO_MASK & (biginit | ONE_MASK);
};

const loadIntoMemoryV1 = (data) => {
  let mask = (x) => x;

  const memory = {};

  for (const line of data) {
    const [cmd, val] = line.split(" = ");
    if (cmd === "mask") {
      mask = createBitMask(val);
    } else if (cmd.startsWith("mem")) {
      const [, address] = cmd.split(/[[\]]/g);

      memory[BigInt(address)] = mask(BigInt(val));
    }
  }

  return memory;
};

console.log(
  `Part 1 – Data: ${Object.values(loadIntoMemoryV1(input)).reduce((acc, x) => acc + x, 0n)}`,
);

const createMemoryAddressDecoder = (mask) => {
  const Xses = mask.split("").flatMap((x, index) => (x === "X" ? [index] : []));
  const permutations = 2 ** Xses.length;

  const maskArray = [];

  for (let i = 0; i < permutations; i += 1) {
    const bits = i.toString(2).padStart(Xses.length, "0").split("");
    const masks = [new Array(36).fill("1"), mask.replace(/X/g, "0").split("")];

    for (let j = 0; j < bits.length; j += 1) {
      masks[bits[j]][Xses[j]] = bits[j];
    }

    maskArray.push(masks.map((x) => BigInt(`0b${x.join("")}`)));
  }

  // eslint-disable-next-line no-bitwise
  return (address) => maskArray.map(([ZERO_MASK, ONE_MASK]) => ZERO_MASK & (address | ONE_MASK));
};

const loadIntoMemoryV2 = (data) => {
  let memoryAddressDecoder = (x) => x;
  const memory = {};

  for (const line of data) {
    const [cmd, val] = line.split(" = ");
    if (cmd === "mask") {
      memoryAddressDecoder = createMemoryAddressDecoder(val);
    } else if (cmd.startsWith("mem")) {
      const [, address] = cmd.split(/[[\]]/g);

      const addresses = memoryAddressDecoder(BigInt(address));

      addresses.forEach((x) => {
        memory[x] = BigInt(val);
      });
    }
  }

  return memory;
};

console.log(
  `Part 2 – Example: ${
    Object.values(loadIntoMemoryV2(examplePart2)).reduce(
      (acc, x) => acc + x,
      0n,
    )
  }`,
);
console.log(
  `Part 2 – Data: ${Object.values(loadIntoMemoryV2(input)).reduce((acc, x) => acc + x, 0n)}`,
);
