import { A_MINOR, Z_MINOR } from "../lib/ASCII.js";
import { dirname, loadFile } from "../lib/shared.js";

function parseGroupAnswers(group) {
  const answers = {};
  let count = 1;
  for (let index = 0; index < group.length; index += 1) {
    const charCode = group.charCodeAt(index);
    if (charCode >= A_MINOR && charCode <= Z_MINOR) {
      answers[charCode] = (answers[charCode] || 0) + 1;
    } else if (charCode === 10) {
      count += 1;
    }
  }
  return { answers, count };
}

const day6 = async (file) => {
  const data = (await loadFile(file))
    .trim()
    .split(/\n\s*\n+/)
    .map(parseGroupAnswers);

  console.log(`Part 1: ${data.reduce((agg, { answers }) => agg + Object.keys(answers).length, 0)}`);
  console.log(
    `Part 2: ${
      data.reduce(
        (agg, { answers, count }) =>
          agg + Object.values(answers).filter((answer) => answer === count).length,
        0,
      )
    }`,
  );
};

await day6(`${dirname(import.meta.url)}/data/06_example.txt`);
await day6(`${dirname(import.meta.url)}/data/06.txt`);
