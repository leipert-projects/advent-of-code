import { dirname, loadFileIntoArray } from "../lib/shared.js";

function parseSeat(partitioning) {
  // Bit confusing, but we need to assign inverse, e.g. when B we need to change the lower value,
  // thus B and F and R and L are switched
  const row = { B: 0, F: 128 };
  const column = { R: 0, L: 8 };

  for (const letter of partitioning) {
    if (["F", "B"].includes(letter)) {
      row[letter] = (row.F + row.B) / 2;
    } else if (["L", "R"].includes(letter)) {
      column[letter] = (column.L + column.R) / 2;
    }
  }

  return {
    seat: partitioning,
    row: row.B,
    column: column.R,
    id: row.B * 8 + column.R,
  };
}

const example = (await loadFileIntoArray(`${dirname(import.meta.url)}/data/05_example.txt`)).map(
  parseSeat,
);
const data = (await loadFileIntoArray(`${dirname(import.meta.url)}/data/05.txt`)).map(parseSeat);
const takenIDs = data.map((x) => x.id);

console.log(["Part 1 – Example:", ...example.map(JSON.stringify)].join("\n\t"));
console.log(`Part 1 – Data: ${Math.max(...takenIDs)}`);

for (let i = Math.min(...takenIDs); i <= Math.max(...takenIDs); i += 1) {
  if (!takenIDs.includes(i)) {
    console.log(`Part 2: Seat ${i} not taken`);
    break;
  }
}
