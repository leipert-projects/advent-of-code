import Passport from "./lib/04_passport.js";
import { dirname, loadFile } from "../lib/shared.js";

const example = (await loadFile(`${dirname(import.meta.url)}/data/04_example.txt`))
  .trim()
  .split(/\n\s*\n+/)
  .map((x) => new Passport(x));

const data = (await loadFile(`${dirname(import.meta.url)}/data/04.txt`))
  .trim()
  .split(/\n\s*\n+/)
  .map((x) => new Passport(x));

try {
  console.log(
    `Part 1 – Example – ${
      example.filter((x) => x.hasAllRequiredFields()).length
    } / ${example.length} passports valid`,
  );
  console.log(
    `Part 1 – Data – ${
      data.filter((x) => x.hasAllRequiredFields()).length
    } / ${data.length} passports valid`,
  );

  console.log(
    `Part 2 – Example – ${
      example.filter((x) => x.isValid()).length
    } / ${example.length} passports valid`,
  );
  console.log(
    `Part 2 – Data – ${data.filter((x) => x.isValid()).length} / ${data.length} passports valid`,
  );
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  process.exit(1);
}
