export const A_MINOR = 97;
export const Z_MINOR = 122;
export const A_MAJOR = 65;
export const Z_MAJOR = 90;
export const NEWLINE = 10;
