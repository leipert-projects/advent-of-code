/* eslint-disable max-classes-per-file */
/**
 * @param {*} val
 * @return {string}
 */
const defaultRenderer = (val) => (val ? "#" : ".");

/**
 * @param {number} n
 * @return {Array<Array<number>>}
 */
function createNeighbors(n) {
  if (n === 0) {
    return [[]];
  }
  return createNeighbors(n - 1).flatMap((x) => [
    [...x, 0],
    [...x, 1],
    [...x, -1],
  ]);
}

/** @template T */
class MapND {
  /**
   * @param {number} dimensions
   */
  constructor(dimensions) {
    this.dimensions = dimensions;
    this.map = new Map();
    this.minCoordinates = Array(dimensions).fill(0);
    this.maxCoordinates = Array(dimensions).fill(0);
    const [, ...neighbors] = createNeighbors(dimensions);
    this.neighbors = neighbors;
  }

  /**
   * @param {T} value
   * @param {...number} coordinates
   */
  set(value, ...coordinates) {
    coordinates.forEach((val, i) => {
      this.minCoordinates[i] = Math.min(this.minCoordinates[i], val);
      this.maxCoordinates[i] = Math.max(this.maxCoordinates[i], val);
    });

    this.map.set(coordinates.join(), { coordinates, value });
  }

  /**
   * @param {...number} coordinates
   * @return {*|null}
   */
  get(...coordinates) {
    const node = this.getNode(...coordinates);
    return (node && node.value) || null;
  }

  /**
   * @param {...number} coordinates
   * @return {{coordinates: number[], value: any}|null}
   */
  getNode(...coordinates) {
    return this.map.get(coordinates.join());
  }

  /**
   * @param {...number} coordinates
   * @return {boolean}
   */
  outOfBounds(...coordinates) {
    return coordinates.some((value, i) =>
      value < this.minCoordinates[i] || value > this.maxCoordinates[i]
    );
  }

  render(renderer = defaultRenderer) {
    if (this.dimensions > 3 || this.dimensions < 2) {
      return [];
    }
    const layers = [];
    for (let z = this.minCoordinates[2] || 0; z <= (this.maxCoordinates[2] || 0); z += 1) {
      const res = [];
      for (let y = this.minCoordinates[1]; y <= this.maxCoordinates[1]; y += 1) {
        let line = "";
        for (let x = this.minCoordinates[0]; x <= this.maxCoordinates[0]; x += 1) {
          if (this.dimensions === 2) {
            line += renderer(this.get(x, y));
          } else {
            line += renderer(this.get(x, y, z));
          }
        }
        res.push(line);
      }
      if (this.dimensions === 2) {
        return res;
      }
      layers.push(res.join("\n"));
    }

    return layers;
  }

  /**
   * @deprecated Use for of loop with iterator
   * @param {(val: *, key: string) => void} fn
   */
  forEach(fn) {
    for (const [key, val] of this.map.entries()) {
      fn(val, key);
    }
  }

  *[Symbol.iterator]() {
    for (const val of this.map.values()) {
      yield val;
    }
  }

  /**
   * @param {number[]} coordinates
   * @return {{coordinates: *, value: *}[]}
   */
  getNeighbors(coordinates) {
    return this.neighbors.map((diff) => {
      const newCoordinates = coordinates.map((x, i) => x + diff[i]);
      return {
        value: this.get(...newCoordinates),
        coordinates: newCoordinates,
      };
    });
  }
}

export default MapND;
