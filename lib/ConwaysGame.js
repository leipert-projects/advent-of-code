import MapND from "./MapND.js";

/**
 * @extends {MapND<boolean>}
 */
export default class ConwaysGame extends MapND {
  constructor(dimension = 2) {
    super(dimension);
    this.next = new MapND(dimension);
  }

  /**
   * @param {boolean} value
   * @param {...number} coordinates
   */
  setNext(value, ...coordinates) {
    this.next.set(value, ...coordinates);
  }

  commit() {
    this.minCoordinates = this.next.minCoordinates;
    this.maxCoordinates = this.next.maxCoordinates;
    this.map = this.next.map;
    this.next = new MapND(this.dimensions);
  }

  /**
   * @param {number} steps
   * @param {Set<number>} stayAlive
   * @param {Set<number>} becomeAlive
   */
  play(steps, stayAlive, becomeAlive) {
    for (let i = 0; i < steps; i += 1) {
      const candidates = new MapND(this.dimensions);

      for (const node of this) {
        let active = 0;

        for (const { coordinates, value } of this.getNeighbors(node.coordinates)) {
          if (value) {
            active += 1;
          }
          candidates.set((candidates.get(...coordinates) || 0) + 1, ...coordinates);
        }

        if (stayAlive.has(active)) {
          this.setNext(true, ...node.coordinates);
        }
      }

      candidates.forEach(({ coordinates, value }) => {
        if (becomeAlive.has(value)) {
          this.setNext(true, ...coordinates);
        }
      });
      this.commit();
    }
  }
}
