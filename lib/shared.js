import { readFile } from "node:fs/promises";

/**
 * Parse a string as an Integer
 *
 * @param {string} x
 * @returns {number}
 */
export const toInteger = (x) => parseInt(x, 10);

/**
 * Whether we are in hot reload mode
 * @return {boolean}
 */
const isHot = () => Boolean(globalThis?.process?.execArgv?.includes("--hot"));

/**
 * @param {string} path
 * @returns {Promise<string>}
 */
export const loadFile = async (path) => {
  if (path !== "/dev/stdin" || !isHot()) {
    return readFile(path, "utf8");
  }
  if (!globalThis.hotReloadStdIn) {
    globalThis.hotReloadStdIn = await readFile("/dev/stdin", "utf-8");
  }

  /** @type {number} */
  globalThis.hotReloadCount ??= 0;
  if (globalThis.hotReloadCount > 0) {
    // console.clear();
    console.log(`Hot Reloaded ${globalThis.hotReloadCount} times`);
  }
  globalThis.hotReloadCount += 1;
  return globalThis.hotReloadStdIn;
};

/**
 * @param {string} path
 * @returns {Promise<string[]>}
 */
export const loadFileIntoArray = async (path) =>
  (await loadFile(path)).split(/[\r\n]+/).filter((x) => x.trim().length > 0);

/**
 * @param {string} path
 * @returns {Promise<number[]>}
 */
export const loadIntegerIntoArray = async (path) =>
  (await loadFileIntoArray(path)).flatMap((x) => x.split(",")).map(toInteger);

export const loadStdIn = () => loadFile("/dev/stdin");
export const loadStdInIntoArray = () => loadFileIntoArray("/dev/stdin");
export const loadStdInIntoIntegerArray = () => loadIntegerIntoArray("/dev/stdin");

/**
 * @template T
 * @param {(line: string) => T} mapFn
 * @returns {Promise<Array<Array<T>>>}
 */
export const loadStdInIntoChunks = async (mapFn) => {
  const raw = (await loadStdIn()).split(/\r|\n|\r\n/);

  const chunks = [];
  let chunk = [];

  for (const line of raw) {
    if (line.length) {
      chunk.push(mapFn(line));
    } else {
      chunks.push(chunk);
      chunk = [];
    }
  }
  chunks.push(chunk);

  return chunks;
};

/**
 * Sums an array of numbers
 *
 * @param {Array<number>} array
 * @returns {number}
 */
export const sumArray = (array) => array.reduce((sum, x) => sum + x, 0);

/**
 * Returns pathname of a file url
 *
 * @param {string} url
 * @returns {string}
 */
export const dirname = (url) => new URL(".", url).pathname;

/**
 * Exit process
 *
 * @param {number} code
 */
export const exit = (code) => {
  if (isHot()) {
    return;
  }
  if (globalThis.Deno) {
    Deno.exit(code);
  } else {
    import("node:process").then((process) => process.exit(code));
  }
};

/**
 * Copy an object
 *
 * @param {any} obj
 * @returns {any}
 */
export const copyObject = (obj) => JSON.parse(JSON.stringify(obj));
