/** @template T */
export default class PriorityQueue {
  /**
   * @param {(val: T) => number} priority
   */
  constructor(priority) {
    /** @type {Array<T>} */
    this.arr = [];
    this.priority = priority;
  }

  pull() {
    return this.arr.shift();
  }

  /**
   * @param {T} node
   * @return {boolean}
   */
  has(node) {
    return this.arr.includes(node);
  }

  size() {
    return this.arr.length;
  }

  /**
   * @param {T} entry
   * @param {number=} p
   * @return {void}
   */
  insert(entry, p) {
    const prio = p || this.priority(entry);
    for (let i = 0; i < this.arr.length; i += 1) {
      if (prio < this.priority(this.arr[i])) {
        this.arr.splice(i, 0, entry);
        return;
      }
    }
    this.arr.push(entry);
  }
}
