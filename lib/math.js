import { sumArray } from "./shared.js";

/**
 * @param {number} n
 * @param {number} r
 * @return {Array<Array<number>>}
 */
export const combinate = (n, r) => {
  const res = [];
  const a = [];
  for (let i = 0; i < r; i += 1) {
    a[i] = i;
  }
  let i = r - 1;

  while (a[0] < n - r + 1) {
    while (i > 0 && a[i] === n - r + i) {
      i -= 1;
    }
    res.push([...a]);
    a[i] += 1;
    while (i < r - 1) {
      a[i + 1] = a[i] + 1;
      i += 1;
    }
  }
  return res;
};

/**
 * @param {number} a
 * @param {number} b
 * @return {number}
 */
export const greatestCommonDivisor = (a, b) => {
  if (a < 0 || b < 0) return -1;

  if (a === 0) {
    return b;
  }
  if (b === 0) {
    return a;
  }
  return greatestCommonDivisor(b, a % b);
};

/**
 * @param {number} a
 * @param {number} b
 * @return {number}
 */
export const lowestCommonMultiple = (a, b) => (a * b) / greatestCommonDivisor(a, b);

/**
 * @param {number[]} arr
 * @return {number}
 */
export const calculateMedian = (arr) => {
  const sorted = arr.sort((a, b) => a - b);
  if (arr.length % 2 === 0) {
    return (sorted[Math.floor(arr.length / 2)] + sorted[Math.ceil(arr.length / 2)]) / 2;
  }
  return sorted[Math.floor(arr.length / 2)];
};

/**
 * @param {number[]} arr
 * @return {number}
 */
export const calculateMean = (arr) => Math.floor(sumArray(arr) / arr.length);

/**
 * bitCount returns number of 1 bit in a 32 digit
 * It's fast, it's from here:
 *
 * https://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetParallel
 * @param {number} n
 * @returns {number}
 */
export function bitCount(n) {
  /* eslint-disable no-bitwise,no-param-reassign */
  n -= (n >> 1) & 0x55555555;
  n = (n & 0x33333333) + ((n >> 2) & 0x33333333);
  return ((n + (n >> 4) & 0xF0F0F0F) * 0x1010101) >> 24;
  /* eslint-enable no-bitwise,no-param-reassign */
}

/**
 * @param {number[]} array
 * @return {number}
 */
export const multiplyArray = (array) => array.reduce((prod, x) => prod * x, 1);

/**
 * @param {(n: number) => number} fn
 * @param {number} start
 * @param {number} end
 * @return {number}
 */
export function bisect(fn, start, end, tolerance = 0) {
  let a = start;
  let b = end;
  let n = 1;
  while (n < 1000) {
    const c = Math.round((a + b) / 2);
    const currDiff = fn(c);
    if (currDiff === 0 || (b - a) / 2 < tolerance) {
      return c;
    }
    n += 1;
    if (Math.sign(currDiff) === Math.sign(fn(a))) {
      a = c;
    } else {
      b = c;
    }
  }
  throw new Error("Bisect didn't work");
}

/**
 * @param {[number, number]} target
 * @param {[number, number]} start
 * @return {number}
 */
export function manhattanDistance(target, start = [0, 0]) {
  return Math.abs(target[0] - start[0]) + Math.abs(target[1] - start[1]);
}
