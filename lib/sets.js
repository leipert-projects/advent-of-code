// copied from MDN
/**
 * @template T
 * @param {Set<T>} set
 * @param {Set<T>} subset
 * @return {boolean}
 */
export function isSuperset(set, subset) {
  for (const elem of subset) {
    if (!set.has(elem)) {
      return false;
    }
  }
  return true;
}

/**
 * @template T
 * @param {Set<T>} setA
 * @param {Set<T>} setB
 * @return {boolean}
 */
export function isSameSet(setA, setB) {
  return setA.size === setB.size && isSuperset(setA, setB);
}

/**
 * @template T
 * @param {Array<T>} array
 * @param {number} i
 * @param {number} j
 * @returns {Array<T>}
 */
const swap = (array, i, j) => {
  const arr = [...array];
  const temp = arr[i];
  arr[i] = arr[j];
  arr[j] = temp;
  return arr;
};
/**
 * Returns all permutations of elements of an array
 * @template T
 * @param {Array<T>} arr
 * @returns {Array<Array<T>>}
 */
export const permutate = (arr) => {
  let array = [...arr];

  /** @type {number[]} */
  const c = [];
  for (let j = 0; j < array.length; j += 1) {
    c[j] = 0;
  }

  const res = [];

  res.push([...array]);

  let i = 0;
  while (i < array.length) {
    if (c[i] < i) {
      array = swap(array, i, i % 2 === 0 ? 0 : c[i]);

      res.push([...array]);
      c[i] += 1;
      i = 0;
    } else {
      c[i] = 0;
      i += 1;
    }
  }

  return res;
};
