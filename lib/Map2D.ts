import PriorityQueue from "./PriorityQueue.js";

// deno-lint-ignore no-explicit-any
const defaultRenderer = (val: any): string => (val ? "▓" : " ");

interface MapNode<T> {
  new (x: number, y: number, value: T): Node<T>;
}

class Node<T> {
  public value: T;
  public x: number;
  public y: number;
  constructor(x: number, y: number, value: T) {
    this.x = x;
    this.y = y;
    this.value = value;
  }
}
export type { Node };

function backtrace<T>(endNode: Node<T>, parents: Map<Node<T>, Node<T>>): number[][] {
  let node: Node<T> | undefined = endNode;
  const path = [[node.x, node.y]];
  while (parents.get(node)) {
    node = parents.get(node);
    if (node === undefined) {
      break;
    }

    path.unshift([node.x, node.y]);
  }
  return path;
}
export default class Map2D<T, U extends Node<T>> {
  private map: Map<string, U>;
  public minX: number;
  minY: number;
  maxX: number;
  maxY: number;
  stats: Map<T, number>;

  constructor() {
    this.map = new Map();
    this.minX = 0;
    this.minY = 0;
    this.maxX = 0;
    this.maxY = 0;
    this.stats = new Map();
  }

  set(x: number, y: number, value: T, NodeClass: MapNode<T> = Node<T>) {
    this.minX = Math.min(this.minX, x);
    this.maxX = Math.max(this.maxX, x);
    this.minY = Math.min(this.minY, y);
    this.maxY = Math.max(this.maxY, y);

    const currValue = this.get(x, y);

    if (currValue !== undefined) {
      this.stats.set(currValue, (this.stats.get(currValue) || 0) - 1);
    }

    const node = new NodeClass(x, y, value);
    this.map.set([x, y].join(), node as U);
    this.stats.set(value, (this.stats.get(value) || 0) + 1);
    return node;
  }

  get(x: number, y: number): T | undefined {
    return (this.map.get([x, y].join()) || {}).value;
  }

  getNode(x: number, y: number): U | undefined {
    return this.map.get([x, y].join());
  }

  forEach(fn: (val: U, key: string) => void): void {
    for (const [key, val] of this.map.entries()) {
      fn(val, key);
    }
  }

  *[Symbol.iterator]() {
    for (const val of this.map.values()) {
      yield val;
    }
  }

  // deno-lint-ignore no-explicit-any
  reduce(fn: (acc: any, val: U, key: string) => any, initial: any): any {
    let acc = initial;
    for (const [key, val] of this.map.entries()) {
      acc = fn(acc, val, key);
    }
    return acc;
  }

  render(renderer = defaultRenderer): string[] {
    const res = [];

    for (let y = this.minY; y <= this.maxY; y += 1) {
      let line = "";
      for (let x = this.minX; x <= this.maxX; x += 1) {
        line += renderer(this.get(x, y));
      }
      res.push(line);
    }

    return res;
  }

  renderNodes(renderer = defaultRenderer): string[] {
    const res = [];

    for (let y = this.minY; y <= this.maxY; y += 1) {
      let line = "";
      for (let x = this.minX; x <= this.maxX; x += 1) {
        line += renderer(this.getNode(x, y));
      }
      res.push(line);
    }

    return res;
  }

  breadthSearch(start: [number, number], end: [number, number]) {
    const openList = [];
    const startNode = this.getNode(...start);
    const endNode = this.getNode(...end);
    if (!endNode) {
      throw new Error(`endNode ${end} unknown`);
    }
    let neighbors;
    let node;
    const opened = [];
    const closed = [];
    const parents = new Map();

    // push the start pos into the queue
    openList.push(startNode);
    opened.push(start);

    // while the queue is not empty
    while (openList.length) {
      // take the front node from the queue
      node = openList.shift();
      if (!node) {
        continue;
      }
      closed.push(node);

      // reached the end position
      if (node === endNode) {
        return backtrace(endNode, parents);
      }

      neighbors = this.getNeighbors(node);
      for (let i = 0, l = neighbors.length; i < l; i += 1) {
        const neighbor = neighbors[i];

        // skip this neighbor if it has been inspected before
        if (closed.includes(neighbor) || opened.includes(neighbor)) {
          // eslint-disable-next-line no-continue
          continue;
        }

        openList.push(neighbor);
        opened.push(neighbor);
        parents.set(neighbor, node);
      }
    }

    // fail to find the path
    return [];
  }

  a_star(start: [number, number], end: [number, number]) {
    const startNode = this.getNode(...start);
    if (!startNode) {
      throw new Error(`startNode ${start} not found`);
    }
    const endNode = this.getNode(...end);

    const cameFrom = new Map();
    const gScore = new Map();
    gScore.set(startNode, 0);
    const fScore = new Map();
    fScore.set(startNode, 0);
    const queue = new PriorityQueue<U>((node) => fScore.get(node) || Number.MAX_SAFE_INTEGER);
    queue.insert(startNode, 0);
    while (queue.size) {
      const current = queue.pull();
      if (current === endNode) {
        break;
      }
      if (!current) {
        throw new Error("No path found");
      }
      for (const neighbor of this.getNeighbors(current)) {
        const alternative = gScore.get(current) + neighbor.value;
        const previous = gScore.get(neighbor) || Number.MAX_SAFE_INTEGER;
        if (alternative < previous) {
          cameFrom.set(neighbor, current);
          gScore.set(neighbor, alternative);
          const fs = alternative + neighbor.value;
          fScore.set(neighbor, fs);
          if (!queue.has(neighbor)) {
            queue.insert(neighbor, fs);
          }
        }
      }
    }
    const path = [endNode];
    let current = endNode;
    while (current !== startNode) {
      current = cameFrom.get(current);
      path.unshift(current);
    }
    return path;
  }

  getNeighbors(node: U, includeDiagonal = false): U[] {
    const res = [
      this.getNode(node.x - 1, node.y),
      this.getNode(node.x + 1, node.y),
      this.getNode(node.x, node.y + 1),
      this.getNode(node.x, node.y - 1),
      ...(includeDiagonal
        ? [
          this.getNode(node.x - 1, node.y + 1),
          this.getNode(node.x + 1, node.y + 1),
          this.getNode(node.x + 1, node.y - 1),
          this.getNode(node.x - 1, node.y - 1),
        ]
        : []),
    ].filter((x: U | undefined) => x !== undefined);
    return res as U[];
  }

  delete(x: number, y: number) {
    const currValue = this.get(x, y);
    if (currValue !== undefined) {
      this.stats.set(currValue, (this.stats.get(currValue) || 0) - 1);
    }
    this.map.delete([x, y].join());
  }
}
