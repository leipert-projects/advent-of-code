export function transposeStringArray(strings: string[]): string[] {
  const res: string[] = Array(strings[0].length).fill("");
  for (let i = 0; i < strings.length; i += 1) {
    for (let j = 0; j < strings[i].length; j += 1) {
      res[j] += strings[i][j];
    }
  }
  return res;
}
