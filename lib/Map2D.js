import PriorityQueue from "./PriorityQueue.js";
const defaultRenderer = (val) => val ? "\u2593" : " ";
class Node {
  value;
  x;
  y;
  constructor(x, y, value) {
    this.x = x;
    this.y = y;
    this.value = value;
  }
}
function backtrace(endNode, parents) {
  let node = endNode;
  const path = [[node.x, node.y]];
  while (parents.get(node)) {
    node = parents.get(node);
    if (node === void 0) {
      break;
    }
    path.unshift([node.x, node.y]);
  }
  return path;
}
class Map2D {
  map;
  minX;
  minY;
  maxX;
  maxY;
  stats;
  constructor() {
    this.map = /* @__PURE__ */ new Map();
    this.minX = 0;
    this.minY = 0;
    this.maxX = 0;
    this.maxY = 0;
    this.stats = /* @__PURE__ */ new Map();
  }
  set(x, y, value, NodeClass = Node) {
    this.minX = Math.min(this.minX, x);
    this.maxX = Math.max(this.maxX, x);
    this.minY = Math.min(this.minY, y);
    this.maxY = Math.max(this.maxY, y);
    const currValue = this.get(x, y);
    if (currValue !== void 0) {
      this.stats.set(currValue, (this.stats.get(currValue) || 0) - 1);
    }
    const node = new NodeClass(x, y, value);
    this.map.set([x, y].join(), node);
    this.stats.set(value, (this.stats.get(value) || 0) + 1);
    return node;
  }
  get(x, y) {
    return (this.map.get([x, y].join()) || {}).value;
  }
  getNode(x, y) {
    return this.map.get([x, y].join());
  }
  forEach(fn) {
    for (const [key, val] of this.map.entries()) {
      fn(val, key);
    }
  }
  *[Symbol.iterator]() {
    for (const val of this.map.values()) {
      yield val;
    }
  }
  reduce(fn, initial) {
    let acc = initial;
    for (const [key, val] of this.map.entries()) {
      acc = fn(acc, val, key);
    }
    return acc;
  }
  render(renderer = defaultRenderer) {
    const res = [];
    for (let y = this.minY; y <= this.maxY; y += 1) {
      let line = "";
      for (let x = this.minX; x <= this.maxX; x += 1) {
        line += renderer(this.get(x, y));
      }
      res.push(line);
    }
    return res;
  }
  breadthSearch(start, end) {
    const openList = [];
    const startNode = this.getNode(...start);
    const endNode = this.getNode(...end);
    if (!endNode) {
      throw new Error(`endNode ${end} unknown`);
    }
    let neighbors;
    let node;
    const opened = [];
    const closed = [];
    const parents = /* @__PURE__ */ new Map();
    openList.push(startNode);
    opened.push(start);
    while (openList.length) {
      node = openList.shift();
      if (!node) {
        continue;
      }
      closed.push(node);
      if (node === endNode) {
        return backtrace(endNode, parents);
      }
      neighbors = this.getNeighbors(node);
      for (let i = 0, l = neighbors.length; i < l; i += 1) {
        const neighbor = neighbors[i];
        if (closed.includes(neighbor) || opened.includes(neighbor)) {
          continue;
        }
        openList.push(neighbor);
        opened.push(neighbor);
        parents.set(neighbor, node);
      }
    }
    return [];
  }
  a_star(start, end) {
    const startNode = this.getNode(...start);
    if (!startNode) {
      throw new Error(`startNode ${start} not found`);
    }
    const endNode = this.getNode(...end);
    const cameFrom = /* @__PURE__ */ new Map();
    const gScore = /* @__PURE__ */ new Map();
    gScore.set(startNode, 0);
    const fScore = /* @__PURE__ */ new Map();
    fScore.set(startNode, 0);
    const queue = new PriorityQueue((node) => fScore.get(node) || Number.MAX_SAFE_INTEGER);
    queue.insert(startNode, 0);
    while (queue.size) {
      const current2 = queue.pull();
      if (current2 === endNode) {
        break;
      }
      if (!current2) {
        throw new Error("No path found");
      }
      for (const neighbor of this.getNeighbors(current2)) {
        const alternative = gScore.get(current2) + neighbor.value;
        const previous = gScore.get(neighbor) || Number.MAX_SAFE_INTEGER;
        if (alternative < previous) {
          cameFrom.set(neighbor, current2);
          gScore.set(neighbor, alternative);
          const fs = alternative + neighbor.value;
          fScore.set(neighbor, fs);
          if (!queue.has(neighbor)) {
            queue.insert(neighbor, fs);
          }
        }
      }
    }
    const path = [endNode];
    let current = endNode;
    while (current !== startNode) {
      current = cameFrom.get(current);
      path.unshift(current);
    }
    return path;
  }
  getNeighbors(node, includeDiagonal = false) {
    const res = [
      this.getNode(node.x - 1, node.y),
      this.getNode(node.x + 1, node.y),
      this.getNode(node.x, node.y + 1),
      this.getNode(node.x, node.y - 1),
      ...includeDiagonal
        ? [
          this.getNode(node.x - 1, node.y + 1),
          this.getNode(node.x + 1, node.y + 1),
          this.getNode(node.x + 1, node.y - 1),
          this.getNode(node.x - 1, node.y - 1),
        ]
        : [],
    ].filter((x) => x !== void 0);
    return res;
  }
  delete(x, y) {
    const currValue = this.get(x, y);
    if (currValue !== void 0) {
      this.stats.set(currValue, (this.stats.get(currValue) || 0) - 1);
    }
    this.map.delete([x, y].join());
  }
}
export { Map2D as default, Node };
