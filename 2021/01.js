import { exit, loadStdInIntoArray, sumArray, toInteger } from "../lib/shared.js";

const data = (await loadStdInIntoArray()).map(toInteger);

const slidingWindowComparison = (arr, windowSize) => {
  let count = 0;
  let prev = Number.MAX_VALUE;
  for (let i = 0; i <= arr.length - windowSize; i += 1) {
    const window1 = [...arr].slice(i, windowSize + i);

    const val = sumArray(window1);

    if (val > prev) {
      count += 1;
    }

    prev = val;
  }

  return count;
};

const example = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263];

try {
  console.log(`\tPart 1 – Example: ${slidingWindowComparison(example, 1)}`);
  console.log(`\tPart 1 – Data: ${slidingWindowComparison(data, 1)}`);

  console.log(`\tPart 2 – Example: ${slidingWindowComparison(example, 3)}`);
  console.log(`\tPart 2 – Data: ${slidingWindowComparison(data, 3)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
