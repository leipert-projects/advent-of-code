import { exit, loadStdInIntoArray, sumArray } from "../lib/shared.js";
import { isSameSet, isSuperset } from "../lib/sets.js";

const data = await loadStdInIntoArray();

const KNOWN_DIGITS = [2, 3, 4, 7];

const countSimpleDigits = (arr) =>
  arr.flatMap((x) =>
    x
      .split("|")[1]
      .trim()
      .split(" ")
      .filter((y) => KNOWN_DIGITS.includes(y.length))
  ).length;

const toSet = (val) => new Set(val.split("").sort());

const getDigitFactory = (mapping) => {
  const ONE = toSet(mapping.find((x) => x.length === 2));
  const FOUR = toSet(mapping.find((x) => x.length === 4));
  const NINE = toSet(mapping.find((x) => x.length === 6 && isSuperset(toSet(x), FOUR)));
  return (digit) => {
    if (digit.length === 2) {
      return 1;
    }
    if (digit.length === 3) {
      return 7;
    }
    if (digit.length === 4) {
      return 4;
    }
    if (digit.length === 7) {
      return 8;
    }
    const digitSet = toSet(digit);
    if (digit.length === 6) {
      if (isSameSet(digitSet, NINE)) {
        return 9;
      }
      return isSuperset(digitSet, ONE) ? 0 : 6;
    }

    if (digit.length === 5) {
      if (isSuperset(NINE, digitSet)) {
        return isSuperset(digitSet, ONE) ? 3 : 5;
      }

      return 2;
    }
    throw new Error("WHAT");
  };
};

const getDisplayValue = (line) => {
  const [mapping, digits] = line.split("|").map((x) => x.trim().split(" "));

  const getDigit = getDigitFactory(mapping);

  const mappedDigits = digits.map(getDigit).join("");

  return parseInt(mappedDigits, 10);
};

const sumDisplayValues = (lines) => sumArray(lines.map(getDisplayValue));

try {
  console.log(`\tPart 1: ${countSimpleDigits(data)}`);

  console.log(
    `\tDisplay Value ${
      getDisplayValue(
        `acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf`,
      )
    }`,
  );

  console.log(`\tPart 2: ${sumDisplayValues(data)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
