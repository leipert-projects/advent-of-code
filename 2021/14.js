import { exit, loadStdInIntoArray } from "../lib/shared.js";

const stdin = await loadStdInIntoArray();

function polymerizationScore(input, iterations = 1) {
  const letterCounts = {};
  const pairCount = {};

  const [polymer, ...rulesRaw] = input;

  for (const char of polymer) {
    letterCounts[char] ||= 0;
    letterCounts[char] += 1;
  }

  const rules = Object.fromEntries(
    rulesRaw.map((rule) => {
      const [pair, char] = rule.split("->").map((chunk) => chunk.trim());
      pairCount[pair] ||= 0;
      letterCounts[char] ||= 0;
      return [pair, char];
    }),
  );

  for (let i = 0; i < polymer.length - 1; i += 1) {
    pairCount[polymer.substr(i, 2)] ||= 0;
    pairCount[polymer.substr(i, 2)] += 1;
  }

  for (let i = 0; i < iterations; i += 1) {
    Object.entries({ ...pairCount }).forEach(([pair, count]) => {
      const newChar = rules[pair];
      letterCounts[newChar] += count;
      pairCount[pair] -= count;
      pairCount[pair[0] + newChar] += count;
      pairCount[newChar + pair[1]] += count;
    });
  }

  return Math.max(...Object.values(letterCounts)) - Math.min(...Object.values(letterCounts));
}

try {
  console.log(`\tPart 1: ${polymerizationScore(stdin, 10)}`);
  console.log(`\tPart 2: ${polymerizationScore(stdin, 40)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
