import { exit, loadStdInIntoArray } from "../lib/shared.js";

import UndirectedGraph from "./lib/UndirectedGraph.js";

const stdin = await loadStdInIntoArray();

class CavePath {
  constructor(arr, hasDuplicates) {
    this.path = arr;
    if (hasDuplicates) {
      this.hasDuplicates = true;
    } else {
      const array = this.path.filter((x) => x.toLowerCase() === x);
      this.hasDuplicates = new Set(array).size !== array.length;
    }
  }

  append(item) {
    return new CavePath([...this.path, item], this.hasDuplicates);
  }

  includes(item) {
    return this.path.includes(item);
  }
}

function findPaths(input, visitSmallCaveTwice = false) {
  const graph = new UndirectedGraph();
  for (const line of input) {
    graph.addEdge(...line.split("-"));
  }
  const paths = [];
  const findPathsRecursively = (node, targetNode, connectionPath = []) => {
    graph.getAdjacentNodes(node).forEach((nextNode) => {
      if (nextNode === targetNode) {
        paths.push([...connectionPath.path, targetNode]);
        return;
      }
      if (nextNode === connectionPath.path[0]) {
        return;
      }
      const isLargeCave = nextNode.toUpperCase() === nextNode;
      // smol
      if (!isLargeCave && connectionPath.includes(nextNode) && connectionPath.hasDuplicates) {
        return;
      }

      findPathsRecursively(nextNode, targetNode, connectionPath.append(nextNode));
    });
  };
  findPathsRecursively("start", "end", new CavePath(["start"], !visitSmallCaveTwice));

  return paths;
}

try {
  console.log(`\tPart 1 – Stdin: ${findPaths(stdin).length}`);
  console.log(`\tPart 2 – Stdin: ${findPaths(stdin, true).length}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
