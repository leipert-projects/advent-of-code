import { exit, loadStdInIntoArray, sumArray, toInteger } from "../lib/shared.js";
import Map2D from "../lib/Map2D.js";

const data = await loadStdInIntoArray();

function getLowPoints(map) {
  const lowPoints = [];
  map.forEach((node) => {
    const { value } = node;
    if (map.getNeighbors(node).every((neighbor) => neighbor.value > value)) {
      lowPoints.push(node);
    }
  });
  return lowPoints;
}

function buildMap(mapInput) {
  const map = new Map2D();
  for (let y = 0; y < mapInput.length; y += 1) {
    for (let x = 0; x < mapInput[y].length; x += 1) {
      map.set(x, y, toInteger(mapInput[y][x]));
    }
  }
  return map;
}

function getRiskFactor(map) {
  const lowPoints = getLowPoints(map);

  return sumArray(lowPoints.map(({ value }) => value + 1));
}

function getBasin(map, point) {
  const stack = [point];
  let size = 0;
  do {
    const curr = stack.shift();
    if (curr.visited) {
      // eslint-disable-next-line no-continue
      continue;
    }
    size += 1;
    curr.visited = true;
    for (const neighbor of map.getNeighbors(curr)) {
      if (!neighbor.visited && neighbor.value < 9) {
        stack.push(neighbor);
      }
    }
  } while (stack.length > 0);
  return size;
}

function getThreeLargestBasins(map) {
  const lowPoints = getLowPoints(map);

  const basins = lowPoints.map((point) => getBasin(map, point));

  return basins
    .sort((a, b) => b - a)
    .slice(0, 3)
    .reduce((acc, n) => acc * n, 1);
}

try {
  const map = buildMap(data);

  console.log(`\tPart 1: ${getRiskFactor(map)}`);
  console.log(`\tPart 2: ${getThreeLargestBasins(map)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
