import { exit, loadStdInIntoIntegerArray, sumArray } from "../lib/shared.js";
import { calculateMean, calculateMedian } from "../lib/math.js";

const data = await loadStdInIntoIntegerArray();
const example = [16, 1, 2, 0, 4, 2, 7, 1, 2, 14];

const simpleCost = (arr) => {
  const median = calculateMedian(arr);

  return `${median} => ${sumArray(arr.map((n) => Math.abs(n - median)))}`;
};

const gaußianCost = (arr) => {
  const mean = calculateMean(arr);

  let min = [Number.MAX_SAFE_INTEGER, Number.MAX_SAFE_INTEGER];

  for (let i = mean - 10; i < mean + 10; i += 1) {
    const cost = sumArray(
      arr.map((n) => {
        const diff = Math.abs(n - i);
        // Gauß'ian sum:
        return (diff / 2) * (1 + diff);
      }),
    );

    if (cost < min[1]) {
      min = [i, cost];
    }
  }

  return `${min[0]} => ${min[1]}`;
};

try {
  console.log(`\tPart 1 – Example: ${simpleCost(example)}`);
  console.log(`\tPart 1 – Data: ${simpleCost(data)}`);

  console.log(`\tPart 2 – Example: ${gaußianCost(example)}`);
  console.log(`\tPart 2 – Data: ${gaußianCost(data)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
