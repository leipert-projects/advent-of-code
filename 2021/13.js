import { exit, loadStdInIntoArray, toInteger } from "../lib/shared.js";

import Map2D from "../lib/Map2D.js";

const stdin = await loadStdInIntoArray();

function mapAndFold(input, n = 0) {
  const map = new Map2D();
  const foldInstructions = [];
  for (const line of input) {
    if (line.startsWith("fold along")) {
      const [key, val] = line.replace("fold along", "").trim().split("=");
      foldInstructions.push({ [key]: toInteger(val) });
    } else if (line.includes(",")) {
      const [x, y] = line.split(",").map(toInteger);
      map.set(x, y, 1);
    }
  }

  for (let i = 0; i < (n || foldInstructions.length); i += 1) {
    const instruction = foldInstructions[i];
    const [dimension, value] = Object.entries(instruction)[0];
    map.forEach((node) => {
      if (node[dimension] >= value) {
        const coord = { x: node.x, y: node.y };
        coord[dimension] = 2 * value - coord[dimension];
        map.set(coord.x, coord.y, 1);
        map.delete(node.x, node.y);
      }
    });
    map[`max${dimension.toUpperCase()}`] = value;
  }
  if (n === 0) {
    return `\n${map.render().join("\n")}`;
  }
  return map.stats.get(1);
}

try {
  console.log(`\tPart 1: ${mapAndFold(stdin, 1)}`);
  console.log(`\tPart 2: ${mapAndFold(stdin)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
