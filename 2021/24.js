import { exit, loadStdInIntoArray, toInteger } from "../lib/shared.js";

const stdin = await loadStdInIntoArray();

const program = (instructions) => (...args) => {
  const memory = {
    w: 0,
    x: 0,
    y: 0,
    z: 0,
  };

  const val = (x) => (["w", "x", "y", "z"].includes(x) ? memory[x] : toInteger(x));

  const input = [...args];
  for (const i of instructions) {
    const [cmd, a, b] = i.split(" ");
    switch (cmd) {
      case "inp":
        if (!input.length) {
          throw new Error("No input elements left");
        }
        memory[a] = toInteger(input.shift());
        break;
      case "add":
        memory[a] += val(b);
        break;
      case "mul":
        memory[a] *= val(b);
        break;
      case "div": {
        const d = val(b);
        if (d === 0) {
          throw new Error("Division by zero");
        }
        memory[a] = Math.floor(memory[a] / d);
        break;
      }
      case "mod": {
        const e = val(b);
        if (memory[a] < 0 || e <= 0) {
          throw new Error("Modulo by zero");
        }
        memory[a] %= e;
        break;
      }
      case "eql":
        memory[a] = memory[a] === val(b) ? 1 : 0;
        break;
      default:
        throw new Error(`Unknown command '${cmd}'`);
    }
  }
  return memory;
};

const findModelNumber = (input) => {
  const p = program(input);

  const instructions = [];
  const vars = [];
  const map = [];

  let c = 0;

  for (let i = 0; i < input.length; i += 1) {
    if (input[i] === "div z 1") {
      const x = toInteger(input[i + 11].split(" ").reverse()[0]);
      instructions.push(x);
      vars.push(c);
      c += 1;
    } else if (input[i] === "div z 26") {
      const x = toInteger(input[i + 1].split(" ").reverse()[0]);
      map.push([c, vars.pop(), x + instructions.pop()]);
      c += 1;
    }
  }

  const pos = [];

  map.forEach(([pos0, pos1, abs]) => {
    console.log(pos0, pos1, abs);
    pos[pos0] = [];
    pos[pos1] = [];
    for (let i = 1; i < 10; i += 1) {
      if (i + abs > 0 && i + abs < 10) {
        pos[pos0].push(i + abs);
        pos[pos1].push(i);
      }
    }
  });

  const max = pos.map((x) => Math.max(...x));

  console.log(`${max.join("")} => ${p(...max).z}`);

  const min = pos.map((x) => Math.min(...x));

  console.log(`${min.join("")} => ${p(...min).z}`);
};

try {
  const negate = program(["inp x", "mul x -1"]);

  console.assert(negate(2).x === -2);

  const checkThrice = program(["inp z", "inp x", "mul z 3", "eql z x"]);
  console.assert(checkThrice(3, 9).z === 1);
  console.assert(checkThrice(3, 10).z === 0);

  findModelNumber(stdin);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
