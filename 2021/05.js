import { exit, loadStdInIntoArray, toInteger } from "../lib/shared.js";
import Map2D from "../lib/Map2D.js";

const data = await loadStdInIntoArray();

const mapHydrothermalVents = (list, considerDiagonals = false) => {
  const map = new Map2D();

  for (const line of list) {
    const [start, end] = line.split(" -> ");
    let [x, y] = start.split(",").map(toInteger);
    const [endX, endY] = end.split(",").map(toInteger);
    const diffX = Math.sign(endX - x);
    const diffY = Math.sign(endY - y);
    if (considerDiagonals || x === endX || y === endY) {
      while (y !== endY || x !== endX) {
        map.set(x, y, (map.get(x, y) || 0) + 1);
        x += diffX;
        y += diffY;
      }
      map.set(x, y, (map.get(x, y) || 0) + 1);
    }
  }
  return map;
};

const countDangerousSpots = (list, considerDiagonals) => {
  const map = mapHydrothermalVents(list, considerDiagonals);

  if (list.length < 20) {
    console.log(map.render((x) => (Number.isInteger(x) ? x : ".")).join("\n"));
  }

  return map.reduce((acc, val) => (val.value > 1 ? acc + 1 : acc), 0);
};

try {
  console.log(`\tPart 1: ${countDangerousSpots(data)}`);
  console.log(`\tPart 2: ${countDangerousSpots(data, true)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
