import { exit } from "../lib/shared.js";

class DeterministicDie {
  constructor() {
    this.c = 0;
  }

  roll() {
    this.c += 1;

    return this.c % 100 === 0 ? 100 : this.c % 100;
  }
}

function playGame(playersPositions, die) {
  const positions = [...playersPositions];
  const scores = new Array(playersPositions.length).fill(0);
  let currPlayer = 0;

  do {
    const rolls = [die.roll(), die.roll(), die.roll()];
    positions[currPlayer] = (positions[currPlayer] + rolls[0] + rolls[1] + rolls[2]) % 10;
    if (positions[currPlayer] === 0) {
      positions[currPlayer] = 10;
    }
    scores[currPlayer] += positions[currPlayer];

    currPlayer = currPlayer ? 0 : 1;
  } while (scores[0] < 1000 && scores[1] < 1000);
  return die.c * Math.min(...scores);
}

try {
  const example = [4, 8];
  const data = [6, 3];
  console.log(`\tPart 1 - Example: ${playGame(example, new DeterministicDie())}`);
  console.log(`\tPart 2 - Data: ${playGame(data, new DeterministicDie())}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
