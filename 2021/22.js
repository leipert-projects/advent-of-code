import { exit, loadStdInIntoArray, toInteger } from "../lib/shared.js";
import ConwaysGame from "../lib/ConwaysGame.js";

const stdin = await loadStdInIntoArray();

function runRebootInstructions(input, bounds = 1) {
  const image = new ConwaysGame(3);
  for (const line of input) {
    const [, onOff, ...coordinates] = line.match(
      /(on|off) x=(-?\d+)..(-?\d+),y=(-?\d+)..(-?\d+),z=(-?\d+)..(-?\d+)/,
    );
    console.log(onOff, coordinates);
    const [x1, x2, y1, y2, z1, z2] = coordinates.map(toInteger);
    if (Math.abs(x1) > bounds && Math.abs(x2) > bounds) {
      // eslint-disable-next-line no-continue
      continue;
    }
    for (let x = x1; x <= x2; x += 1) {
      for (let y = y1; y <= y2; y += 1) {
        for (let z = z1; z <= z2; z += 1) {
          image.set(onOff === "on", x, y, z);
        }
      }
    }
  }

  let c = 0;
  for (const node of image) {
    if (node.value) {
      c += 1;
    }
  }
  return c;
}

try {
  console.log(`\tPart 1: ${runRebootInstructions(stdin, 50)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
