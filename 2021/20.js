import { exit, loadStdInIntoArray } from "../lib/shared.js";
import ConwaysGame from "../lib/ConwaysGame.js";

class Image extends ConwaysGame {
  constructor(enhancementAlgorithm) {
    super(2);
    this.enhancementAlgorithm = enhancementAlgorithm;
    this.swipSwap = enhancementAlgorithm[0] === "#" &&
      enhancementAlgorithm[enhancementAlgorithm.length - 1] === ".";
  }

  play(steps) {
    for (let i = 0; i < steps; i += 1) {
      for (let x = this.minCoordinates[0] - 1; x <= this.maxCoordinates[0] + 1; x += 1) {
        for (let y = this.minCoordinates[1] - 1; y <= this.maxCoordinates[1] + 1; y += 1) {
          const curr = this.getNode(x, y) || { value: null, coordinates: [x, y] };
          const int = [...this.getNeighbors([x, y]), curr]
            .sort((a, b) => {
              const [x1, y1] = a.coordinates;
              const [x2, y2] = b.coordinates;

              return y1 - y2 || x1 - x2;
            })
            .map(({ value }) => {
              if (this.swipSwap && value === null) {
                return i % 2 ? 1 : 0;
              }
              return value === "#" ? "1" : "0";
            })
            .join("");

          this.setNext(this.enhancementAlgorithm[parseInt(int, 2)], ...curr.coordinates);
        }
      }

      this.commit();
    }
  }
}

const stdin = await loadStdInIntoArray();

function enhanceImage(input, steps = 1) {
  const [enhancementAlgorithm, ...imageInput] = input;
  const image = new Image(enhancementAlgorithm);
  for (let y = 0; y < imageInput.length; y += 1) {
    const cols = imageInput[y].length;
    for (let x = 0; x < cols; x += 1) {
      image.set(imageInput[x][y], y, x);
    }
  }
  image.play(steps);
  let c = 0;
  for (const node of image) {
    if (node.value === "#") {
      c += 1;
    }
  }
  return c;
}

try {
  console.log(`\tPart 1: ${enhanceImage(stdin, 2)}`);
  console.log(`\tPart 2: ${enhanceImage(stdin, 50)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
