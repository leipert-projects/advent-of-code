import { exit, loadStdInIntoArray } from "../lib/shared.js";

const stdin = (await loadStdInIntoArray()).join("");

const bitArray = (arr) => parseInt(arr.join(""), 2);

const getValue = (type, subpackets = []) => {
  const vals = subpackets.map((x) => x.value);
  switch (type) {
    case 0:
      return vals.reduce((sum, p) => p + sum, 0);
    case 1:
      return vals.reduce((prod, p) => p * prod, 1);
    case 2:
      return Math.min(...vals);
    case 3:
      return Math.max(...vals);
    case 5:
      return vals[0] > vals[1] ? 1 : 0;
    case 6:
      return vals[0] < vals[1] ? 1 : 0;
    case 7:
      return vals[0] === vals[1] ? 1 : 0;
    default:
      throw new Error(`Unknown type ${type}`);
  }
};

const parsePackets = (bits, maxPackets = 0) => {
  const packets = [];

  while (bits.length) {
    const packet = {
      version: bitArray(bits.splice(0, 3)),
      type: bitArray(bits.splice(0, 3)),
    };

    packet.versionSum = packet.version;

    // Value type
    if (packet.type === 4) {
      let value = [];
      let flag;
      do {
        flag = bits.shift();
        const data = bits.splice(0, 4);
        value = [...value, ...data];
      } while (flag === "1");
      packet.value = bitArray(value);
    } else {
      const operatorType = bits.shift();
      let subpackets = [];
      if (operatorType === "0") {
        const length = bitArray(bits.splice(0, 15));
        subpackets = parsePackets(bits.splice(0, length));
      } else {
        const length = bitArray(bits.splice(0, 11));
        subpackets = parsePackets(bits, length);
      }
      packet.value = getValue(packet.type, subpackets);
      packet.versionSum = subpackets.reduce((s, p) => s + p.versionSum, packet.versionSum);
      packet.subpackets = subpackets;
    }

    packets.push(packet);

    if (maxPackets && packets.length >= maxPackets) {
      break;
    }

    if (bits.every((bit) => bit === "0")) {
      break;
    }
  }

  return packets;
};

const packetDecoder = (input) => {
  const bits = input
    .split("")
    .flatMap((x) => parseInt(x, 16).toString(2).padStart(4, "0").split(""));

  return parsePackets(bits)[0];
};

try {
  console.assert(packetDecoder("8A004A801A8002F478").versionSum === 16);
  console.assert(packetDecoder("620080001611562C8802118E34").versionSum === 12);
  console.assert(packetDecoder("C0015000016115A2E0802F182340").versionSum === 23);
  console.assert(packetDecoder("A0016C880162017C3686B18A3D4780").versionSum === 31);

  const packet = packetDecoder(stdin);
  console.log(`\tPart 1: ${packet.versionSum}`);

  console.assert(packetDecoder("C200B40A82").value === 3);
  console.assert(packetDecoder("04005AC33890").value === 54);
  console.assert(packetDecoder("880086C3E88112").value === 7);
  console.assert(packetDecoder("CE00C43D881120").value === 9);
  console.assert(packetDecoder("D8005AC2A8F0").value === 1);
  console.assert(packetDecoder("F600BC2D8F").value === 0);
  console.assert(packetDecoder("9C005AC2F8F0").value === 0);
  console.assert(packetDecoder("9C0141080250320F1802104A08").value === 1);

  console.log(`\tPart 2: ${packet.value}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
