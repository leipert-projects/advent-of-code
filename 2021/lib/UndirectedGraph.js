/** @typedef {{from: string, to: string, weight: number}} Edge */
export default class UndirectedGraph {
  constructor() {
    this.nodes = new Set();
    /** @type {Edge[]} */
    this.edges = [];
    /** @type {{[key: string]: Set<string>}} */
    this.neighbors = {};
  }

  /**
   * @param {string} a
   * @param {string} b
   * @param {number} weight
   */
  addEdge(a, b, weight = 1) {
    this.neighbors[a] ||= new Set();
    this.neighbors[b] ||= new Set();
    this.nodes.add(a);
    this.nodes.add(b);
    this.neighbors[a].add(b);
    this.neighbors[b].add(a);
    this.edges.push({ from: a, to: b, weight });
  }

  /**
   * @param {string} node
   * @return {Edge[]}
   */
  getAdjacentEdges(node) {
    return this.edges.filter((x) => x.from === node || x.to === node);
  }

  /**
   * @param {string} node
   * @return {string[]}
   */
  getAdjacentNodes(node) {
    return [...this.neighbors[node]];
  }
}
