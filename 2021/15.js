import { exit, loadStdInIntoArray, sumArray, toInteger } from "../lib/shared.js";

import Map2D from "../lib/Map2D.js";

const stdin = await loadStdInIntoArray();

function buildMap(mapInput, enlargeByFactor = 1) {
  const map = new Map2D();
  const rows = mapInput.length;
  for (let y = 0; y < rows; y += 1) {
    const cols = mapInput[y].length;
    for (let x = 0; x < cols; x += 1) {
      for (let xFactor = 0; xFactor < enlargeByFactor; xFactor += 1) {
        for (let yFactor = 0; yFactor < enlargeByFactor; yFactor += 1) {
          const newVal = (toInteger(mapInput[y][x]) + xFactor + yFactor) % 9;
          map.set(
            xFactor * cols + x,
            yFactor * rows + y,
            newVal === 0 ? 9 : newVal,
          );
        }
      }
    }
  }
  return map;
}

function safestPath(input, enlargeByFactor = 1) {
  const map = buildMap(input, enlargeByFactor);

  const [, ...path] = map.a_star([0, 0], [map.maxX, map.maxY]);

  return sumArray(path.map((x) => x.value));
}

try {
  console.log(`\tPart 1: ${safestPath(stdin)}`);
  console.log(`\tPart 2: ${safestPath(stdin, 5)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
