import { exit, loadStdInIntoIntegerArray, sumArray } from "../lib/shared.js";

const data = await loadStdInIntoIntegerArray();
const example = [3, 4, 3, 1, 2];

const simulateLanternFish = (arr, days) => {
  const generation = new Array(9).fill(0);

  for (const age of arr) {
    generation[age] += 1;
  }

  for (let i = 0; i < days; i += 1) {
    const oldZeros = generation[0];
    for (let j = 0; j < 8; j += 1) {
      generation[j] = generation[j + 1];
    }
    generation[6] += oldZeros;
    generation[8] = oldZeros;
  }
  return generation;
};

try {
  console.log(`\tPart 1 – Example: ${sumArray(simulateLanternFish(example, 80))}`);
  console.log(`\tPart 1 – Data: ${sumArray(simulateLanternFish(data, 80))}`);

  console.log(`\tPart 2 – Example: ${sumArray(simulateLanternFish(example, 256))}`);
  console.log(`\tPart 2 – Data: ${sumArray(simulateLanternFish(data, 256))}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
