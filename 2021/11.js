import { exit, loadStdInIntoArray, toInteger } from "../lib/shared.js";
import Map2D from "../lib/Map2D.js";

const stdin = await loadStdInIntoArray();

function buildMap(mapInput) {
  const map = new Map2D();
  for (let y = 0; y < mapInput.length; y += 1) {
    for (let x = 0; x < mapInput[y].length; x += 1) {
      map.set(x, y, toInteger(mapInput[y][x]));
    }
  }
  return map;
}

function flash(map) {
  const needsToFlash = [];
  const inc = ({ x, y, value }) => {
    const newVal = value + 1;
    map.set(x, y, newVal);
    if (newVal > 9) {
      needsToFlash.push([x, y]);
    }
  };
  map.forEach(inc);
  let flashed = 0;
  while (needsToFlash.length) {
    const current = needsToFlash.pop();
    const node = map.getNode(...current);
    if (node.value === 0) {
      // eslint-disable-next-line no-continue
      continue;
    }
    flashed += 1;
    map.set(node.x, node.y, 0);
    map.getNeighbors(node, true).forEach((neighbor) => {
      if (neighbor.value > 0) {
        inc(neighbor);
      }
    });
  }
  return flashed;
}

function runCaveSimulation(cave, n = 10) {
  let flashed = 0;
  for (let i = 0; i < n; i += 1) {
    const curr = flash(cave);
    flashed += curr;
  }
  return flashed;
}

function findSyncFlash(cave) {
  let n = 100;
  // eslint-disable-next-line no-constant-condition
  while (true) {
    n += 1;
    if (flash(cave) === 100) {
      break;
    }
  }
  return n;
}

try {
  const cave = buildMap(stdin);

  console.log(`\tPart 1: ${runCaveSimulation(cave, 100)}`);
  console.log(`\tPart 2: ${findSyncFlash(cave)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
