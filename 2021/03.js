import { exit, loadStdInIntoArray } from "../lib/shared.js";

const data = await loadStdInIntoArray();

const example = [
  "00100",
  "11110",
  "10110",
  "10111",
  "10101",
  "01111",
  "00111",
  "11100",
  "10000",
  "11001",
  "00010",
  "01010",
];

const powerConsumption = (arr) => {
  const res = new Array(arr[0].length).fill(0);
  for (const e of arr) {
    e.split("").forEach((digit, idx) => {
      if (digit === "1") {
        res[idx] += 1;
      }
    });
  }

  const mostCommon = res.map((x) => (x > arr.length / 2 ? "1" : "0")).join("");
  const leastCommon = res.map((x) => (x < arr.length / 2 ? "1" : "0")).join("");

  return parseInt(mostCommon, 2) * parseInt(leastCommon, 2);
};

const search = (arr, mostCommon = true, pos = 0) => {
  if (arr.length === 1) {
    return arr[0];
  }
  const reduced = arr.reduce(
    (acc, x) => {
      acc[x[pos]].push(x);
      return acc;
    },
    { 0: [], 1: [] },
  );

  if (reduced[1].length >= reduced[0].length) {
    return search(reduced[mostCommon ? 1 : 0], mostCommon, pos + 1);
  }
  return search(reduced[mostCommon ? 0 : 1], mostCommon, pos + 1);
};

const lifeSupportRating = (arr) => {
  const oxygenGeneratorRating = parseInt(search(arr, true), 2);
  const co2ScrubberRating = parseInt(search(arr, false), 2);

  console.log(co2ScrubberRating);
  console.log(oxygenGeneratorRating);

  return oxygenGeneratorRating * co2ScrubberRating;
};

try {
  console.log(`\tPart 1 – Example: ${powerConsumption(example)}`);
  console.log(`\tPart 1 – Data: ${powerConsumption(data)}`);

  console.log(`\tPart 2 – Example: ${lifeSupportRating(example)}`);
  console.log(`\tPart 2 – Data: ${lifeSupportRating(data)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
