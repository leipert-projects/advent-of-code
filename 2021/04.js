import { exit, loadStdIn, sumArray, toInteger } from "../lib/shared.js";

const data = await loadStdIn();

function transpose(matrix) {
  return matrix[0].map((_col, i) => matrix.map((row) => row[i]));
}

const loadBingoGame = (raw) => {
  const [rawMoves, ...rawBoards] = raw.split(/\n\n/);
  const moves = rawMoves.split(",").map(toInteger);

  const boards = rawBoards
    .map((line) =>
      line
        .trim()
        .split("\n")
        .map((cell) => cell.trim().split(/ +/))
    )
    .flatMap((matrix) => [matrix, transpose(matrix)])
    .map((matrix) => matrix.map((line) => `|${line.join("|")}|`).join("\n"));

  return [moves, boards];
};

const getScore = (board, move) => {
  const score = board
    .split(/[|x\n]+/g)
    .filter(Boolean)
    .map(toInteger);

  return sumArray(score) * move;
};

const playBingo = (raw) => {
  const [moves, boards] = loadBingoGame(raw);
  let move = 0;
  const finishedBoards = [];
  for (let i = 0; i < moves.length; i += 1) {
    move = moves[i];
    for (let j = 0; j < boards.length; j += 2) {
      boards[j] = boards[j].replace(`|${move}|`, "|x|");
      boards[j + 1] = boards[j + 1].replace(`|${move}|`, "|x|");
      if (boards[j].includes(`|x|x|x|x|x|`)) {
        finishedBoards.push(getScore(boards[j], move));
        boards[j] = "";
        boards[j + 1] = "";
      } else if (boards[j + 1].includes(`|x|x|x|x|x|`)) {
        finishedBoards.push(getScore(boards[j + 1], move));
        boards[j] = "";
        boards[j + 1] = "";
      }
    }
  }

  return [finishedBoards[0], finishedBoards[finishedBoards.length - 1]];
};

try {
  console.log(`\t${playBingo(data)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
