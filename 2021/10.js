import { exit, loadStdInIntoArray, sumArray } from "../lib/shared.js";
import { calculateMedian } from "../lib/math.js";

const data = await loadStdInIntoArray();

const OPEN = ["(", "[", "{", "<"];
const CLOSE = [")", "]", "}", ">"];

const parseSyntax = (line) => {
  const stack = [];

  for (const char of line) {
    if (OPEN.includes(char)) {
      stack.unshift(char);
    } else {
      const lastOpened = stack.shift();
      const lastIndex = OPEN.indexOf(lastOpened);
      if (lastIndex !== CLOSE.indexOf(char)) {
        return { invalid: true, firstIllegalChar: char, line };
      }
    }
  }

  if (stack.length > 0) {
    return { incomplete: true, stack, line };
  }

  return { valid: true, line };
};

const illegalCharScore = (input) => {
  const corruptLines = input
    .map(parseSyntax)
    .filter((x) => x.invalid)
    .map(({ firstIllegalChar }) => ({ ")": 3, "]": 57, "}": 1197, ">": 25137 }[firstIllegalChar]));

  return sumArray(corruptLines);
};

const calculateAutocompleteScore = ({ stack }) => {
  let score = 0;

  for (const char of stack) {
    score = score * 5 + { "(": 1, "[": 2, "{": 3, "<": 4 }[char];
  }

  return score;
};

const incompleteCharScore = (input) => {
  const incompleteLines = input
    .map(parseSyntax)
    .filter((x) => x.incomplete)
    .map(calculateAutocompleteScore);

  return calculateMedian(incompleteLines);
};

try {
  console.log(`\tPart 1 – Data: ${illegalCharScore(data)}`);
  console.log(`\tPart 2 – Data: ${incompleteCharScore(data)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
