import { exit, loadStdInIntoArray } from "../lib/shared.js";

const data = await loadStdInIntoArray();

const position = (arr, considerAim = false) => {
  let x = 0;
  let depth = 0;
  let aim = 0;
  for (const e of arr) {
    // eslint-disable-next-line prefer-const
    let [direction, amount] = e.split(" ");
    amount = parseInt(amount, 10);

    if (direction === "forward") {
      x += amount;
      depth += considerAim ? amount * aim : 0;
      // eslint-disable-next-line no-continue
      continue;
    }
    const dir = direction === "up" ? -1 : 1;
    if (considerAim) {
      aim += dir * amount;
    } else {
      depth += dir * amount;
    }
  }
  return depth * x;
};

try {
  console.log(`\tPart 1: ${position(data)}`);
  console.log(`\tPart 2: ${position(data, true)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
