import { exit, loadStdInIntoArray, toInteger } from "../lib/shared.js";

const stdin = (await loadStdInIntoArray()).join("");

function trajectory(xVel, yVel, targetArea) {
  const pos = [0, 0];
  const vel = [xVel, yVel];
  const path = [];

  while (trajectory.length < 100) {
    pos[0] += vel[0];
    pos[1] += vel[1];
    path.push([...pos]);
    vel[0] -= Math.sign(vel[0]);
    vel[1] -= 1;
    if (
      targetArea[0] <= pos[0] &&
      pos[0] <= targetArea[1] &&
      targetArea[2] <= pos[1] &&
      pos[1] <= targetArea[3]
    ) {
      return path;
    }
    if (pos[1] < targetArea[2]) {
      return null;
    }
    if (pos[0] > targetArea[1]) {
      return null;
    }
  }
  return null;
}

function allTrajectories(targetArea) {
  const trajectories = [];
  for (let y = targetArea[2]; y < -targetArea[2]; y += 1) {
    for (let x = 1; x <= targetArea[1]; x += 1) {
      const t = trajectory(x, y, targetArea);
      if (t) {
        trajectories.push([Math.max(...t.map((v) => v[1])), x, y]);
      }
    }
  }
  return trajectories;
}

function findHeighestThrow(trajectories) {
  return trajectories.sort(([a], [b]) => b - a)[0];
}

try {
  const exampleTarget = [20, 30, -10, -5];
  console.assert(allTrajectories(exampleTarget).length === 112);
  console.assert(trajectory(7, 2, exampleTarget).length === 7);
  console.assert(trajectory(6, 3, exampleTarget).length === 9);
  console.assert(trajectory(9, 0, exampleTarget).length === 4);
  console.assert(trajectory(17, -4, exampleTarget) === null);

  const [, ...target] = stdin.match(/x=([-\d]+)..([-\d]+), y=([-\d]+)..([-\d]+)/);
  const targetArea = target.map(toInteger);
  const trajectories = allTrajectories(targetArea);

  console.log("Part 1", findHeighestThrow(trajectories));
  console.log("Part 2", trajectories.length);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
