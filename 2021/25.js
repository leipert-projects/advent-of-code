import { exit, loadStdInIntoArray } from "../lib/shared.js";

const stdin = await loadStdInIntoArray();

const moveSeaCucumbers = (input) => {
  let arr = input.flatMap((x) => x.split(""));

  let c = 0;
  let copy;

  const lines = input.length;
  const cols = input[0].length;
  let moved;

  do {
    if (c % 2 === 0) {
      moved = false;
    }

    copy = new Array(arr.length).fill(".");

    for (let i = 0; i < arr.length; i += 1) {
      const line = Math.floor(i / cols);
      const pos = i % cols;
      if (arr[i] === ">") {
        const next = line * cols + ((pos + 1) % cols);
        if (c % 2 === 1 || arr[next] !== ".") {
          copy[i] = ">";
        } else {
          copy[next] = ">";
          moved = c % 2 === 0;
        }
      } else if (arr[i] === "v") {
        const next = ((line + 1) % lines) * cols + pos;
        if (c % 2 === 0 || arr[next] !== ".") {
          copy[i] = "v";
        } else {
          copy[next] = "v";
          moved = c % 2 === 1;
        }
      }
    }
    if (!moved) {
      return c / 2 + 1;
    }
    arr = [...copy];
    c += 1;
  } while (c);

  throw new Error("This should never be reached");
};

try {
  console.log(`Result: ${moveSeaCucumbers(stdin)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
