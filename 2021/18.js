import { exit, loadStdInIntoArray, toInteger } from "../lib/shared.js";

const stdin = await loadStdInIntoArray();

const reduce = (snailNum) => {
  let levels = 0;
  const split = snailNum.split(/([[\],]|\d+)/).filter((x) => x.length);
  const result = [];
  const nums = [];
  let exploded = null;
  do {
    const char = split.shift();
    if (char === "[") {
      levels += 1;
      if (levels >= 5 && exploded === null) {
        const [a, , b] = split.splice(0, split.indexOf("]") + 1);

        if (nums.length) {
          nums[nums.length - 1] += toInteger(a);
        }
        exploded = toInteger(b);
        result.push("0");
      } else {
        result.push(char);
      }
    } else if (char === "]") {
      levels -= 1;
      result.push(char);
    } else if (char === ",") {
      result.push(char);
    } else {
      const num = toInteger(char);
      if (exploded) {
        nums.push(num + exploded);
        exploded = 0;
      } else {
        nums.push(num);
      }
      result.push("n");
    }
  } while (split.length);
  let res = result.join("") + split.join("");

  let splitted = exploded !== null;

  for (let n of nums) {
    if (!splitted && n > 9) {
      n = `[${Math.floor(n / 2)},${Math.ceil(n / 2)}]`;
      splitted = true;
    }
    res = res.replace("n", n);
  }

  if (res === snailNum) {
    return res;
  }

  return reduce(res);
};

function addNums(a, b) {
  return reduce(`[${a},${b}]`);
}

function addList(first, ...nums) {
  let res = first;

  for (const n of nums) {
    res = addNums(res, n);
  }

  return res;
}

function magnitude(s) {
  if (Array.isArray(s)) {
    return 3 * magnitude(s[0]) + 2 * magnitude(s[1]);
  }

  if (Number.isInteger(s)) {
    return s;
  }

  return magnitude(JSON.parse(s));
}

function largestMagnitudeFromPairs(arr) {
  let max = 0;
  for (let i = 0; i < arr.length; i += 1) {
    for (let j = 0; j < arr.length; j += 1) {
      if (i !== j) {
        max = Math.max(max, magnitude(addNums(arr[i], arr[j])), magnitude(addNums(arr[j], arr[i])));
      }
    }
  }
  return max;
}

try {
  console.assert(reduce("[[[[[9,8],1],2],3],4]") === "[[[[0,9],2],3],4]");
  console.assert(reduce("[7,[6,[5,[4,[3,2]]]]]") === "[7,[6,[5,[7,0]]]]");
  console.assert(reduce("[[6,[5,[4,[3,2]]]],1]") === "[[6,[5,[7,0]]],3]");
  console.assert(
    reduce("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]") === "[[3,[2,[8,0]]],[9,[5,[7,0]]]]",
  );
  // //
  console.assert(
    addNums("[[[[4,3],4],4],[7,[[8,4],9]]]", "[1,1]") === "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]",
  );
  console.assert(
    addNums("[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]", "[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]") ===
      "[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]",
  );

  console.assert(addList("[1,1]", "[2,2]", "[3,3]", "[4,4]") === "[[[[1,1],[2,2]],[3,3]],[4,4]]");
  console.assert(
    addList("[1,1]", "[2,2]", "[3,3]", "[4,4]", "[5,5]") === "[[[[3,0],[5,3]],[4,4]],[5,5]]",
  );
  console.assert(
    addList("[1,1]", "[2,2]", "[3,3]", "[4,4]", "[5,5]", "[6,6]") ===
      "[[[[5,0],[7,4]],[5,5]],[6,6]]",
  );

  console.assert(magnitude("[[1,2],[[3,4],5]]") === 143);

  console.log(`Part 1 - Magnitude: ${magnitude(addList(...stdin))}`);
  console.log(
    `Part 2 - Largest Magnitude possible from pairs: ${largestMagnitudeFromPairs(stdin)}`,
  );
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
