## 2021/data/12_example.txt

```
Part 1 – Stdin: 10
Part 2 – Stdin: 36
```

| Command                                                                                              |  Mean [ms] | Min [ms] | Max [ms] |    Relative |
| :--------------------------------------------------------------------------------------------------- | ---------: | -------: | -------: | ----------: |
| `bun run 2021/12.js < ./2021/data/12_example.txt`                                                    | 14.0 ± 2.1 |     12.5 |     20.9 |        1.00 |
| `deno run --allow-read=./ --no-config --allow-net=deno.land 2021/12.js < ./2021/data/12_example.txt` | 20.4 ± 1.5 |     19.3 |     27.1 | 1.45 ± 0.25 |
| `node 2021/12.js < ./2021/data/12_example.txt`                                                       | 35.1 ± 0.5 |     34.4 |     38.3 | 2.50 ± 0.38 |

## 2021/data/12.txt

```
Part 1 – Stdin: 5333
Part 2 – Stdin: 146553
```

| Command                                                                                      |   Mean [ms] | Min [ms] | Max [ms] |    Relative |
| :------------------------------------------------------------------------------------------- | ----------: | -------: | -------: | ----------: |
| `bun run 2021/12.js < ./2021/data/12.txt`                                                    | 187.7 ± 5.9 |    183.0 |    202.4 | 1.20 ± 0.05 |
| `deno run --allow-read=./ --no-config --allow-net=deno.land 2021/12.js < ./2021/data/12.txt` | 156.5 ± 4.2 |    152.3 |    170.1 |        1.00 |
| `node 2021/12.js < ./2021/data/12.txt`                                                       | 162.7 ± 2.3 |    160.6 |    171.0 | 1.04 ± 0.03 |

- bun 1.0.13
- deno 1.38.2 (release, aarch64-apple-darwin)
- node v21.2.0
