## 2021/data/10_example.txt

```
Part 1 – Data: 26397
Part 2 – Data: 288957
```

| Command                                                                                              |  Mean [ms] | Min [ms] | Max [ms] |    Relative |
| :--------------------------------------------------------------------------------------------------- | ---------: | -------: | -------: | ----------: |
| `bun run 2021/10.js < ./2021/data/10_example.txt`                                                    | 13.4 ± 0.3 |     12.9 |     16.1 |        1.00 |
| `deno run --allow-read=./ --no-config --allow-net=deno.land 2021/10.js < ./2021/data/10_example.txt` | 19.7 ± 0.4 |     19.2 |     21.1 | 1.47 ± 0.05 |
| `node 2021/10.js < ./2021/data/10_example.txt`                                                       | 38.5 ± 9.4 |     34.6 |    111.3 | 2.87 ± 0.71 |

## 2021/data/10.txt

```
Part 1 – Data: 374061
Part 2 – Data: 2116639949
```

| Command                                                                                      |  Mean [ms] | Min [ms] | Max [ms] |    Relative |
| :------------------------------------------------------------------------------------------- | ---------: | -------: | -------: | ----------: |
| `bun run 2021/10.js < ./2021/data/10.txt`                                                    | 16.8 ± 3.7 |     14.8 |     39.6 |        1.00 |
| `deno run --allow-read=./ --no-config --allow-net=deno.land 2021/10.js < ./2021/data/10.txt` | 21.7 ± 2.6 |     20.6 |     45.3 | 1.29 ± 0.32 |
| `node 2021/10.js < ./2021/data/10.txt`                                                       | 35.9 ± 0.2 |     35.2 |     36.7 | 2.14 ± 0.47 |

- bun 1.0.13
- deno 1.38.2 (release, aarch64-apple-darwin)
- node v21.2.0
