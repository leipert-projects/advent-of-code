import { combinate } from "../lib/math.js";

const rings = [
  [25, 1, 0],
  [50, 2, 0],
  [100, 3, 0],
  [20, 0, 1],
  [40, 0, 2],
  [80, 0, 3],
];

const armorOptions = [
  [0, 0, 0],
  [13, 0, 1],
  [31, 0, 2],
  [53, 0, 3],
  [75, 0, 4],
  [102, 0, 5],
];

const weapon = [
  [8, 4, 0],
  [10, 5, 0],
  [25, 6, 0],
  [40, 7, 0],
  [74, 8, 0],
];

const reducer = ([c = 0, d = 0, a = 0] = [], [c1 = 0, d1 = 0, a1 = 0]) => [c + c1, d1 + d, a + a1];

const ringCombinations = [[], ...combinate(6, 1), ...combinate(6, 2)]
  .map((x) => x.map((i) => rings[i]).reduce(reducer, [0, 0, 0]))
  .sort(([a], [b]) => a - b);

function battle(player, boss) {
  const [, damagePlayer, armorPlayer] = player;
  const [, damageBoss, armorBoss] = boss;
  let [hpPlayer] = player;
  let [hpBoss] = boss;

  // eslint-disable-next-line no-constant-condition
  while (true) {
    hpBoss -= damagePlayer - armorBoss;
    if (hpBoss <= 0) {
      return true;
    }
    hpPlayer -= damageBoss - armorPlayer;
    if (hpPlayer <= 0) {
      return false;
    }
  }
}

let minCostAndWin = Number.MAX_SAFE_INTEGER;
let maxCostAndDefeat = 0;

for (const w of weapon) {
  const [costW, damageW, armorW] = w;
  for (const a of armorOptions) {
    const [costA, damageA, armorA] = a;
    for (const r of ringCombinations) {
      const [costR, damageR, armorR] = r;

      const cost = costA + costR + costW;
      const damage = damageA + damageR + damageW;
      const armor = armorA + armorR + armorW;
      const result = battle([100, damage, armor], [104, 8, 1]);
      if (result && cost < minCostAndWin) {
        minCostAndWin = Math.min(cost, minCostAndWin);
      }
      if (!result && cost > maxCostAndDefeat) {
        maxCostAndDefeat = Math.max(cost, maxCostAndDefeat);
      }
    }
  }
}

console.log(minCostAndWin, maxCostAndDefeat);
