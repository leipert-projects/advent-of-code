import { dirname, loadFile } from "../lib/shared.js";

const input = JSON.parse(await loadFile(`${dirname(import.meta.url)}/data/12.json`));

const recurse = (obj, ignoreRed = false) => {
  if (Number.isFinite(obj)) {
    return obj;
  }
  if (Array.isArray(obj)) {
    return obj.reduce((sum, x) => sum + recurse(x, ignoreRed), 0);
  }

  if (typeof obj === "string" || obj instanceof String) {
    return 0;
  }

  const values = Object.values(obj);

  if (ignoreRed && values.includes("red")) {
    return 0;
  }

  return recurse(values, ignoreRed);
};

console.warn(`Part 1: ${recurse(input)}`);
console.warn(`Part 2: ${recurse(input, true)}`);
