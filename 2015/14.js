/* eslint-disable max-classes-per-file */

class Reindeer {
  constructor(name, speed, travelTime, restTime) {
    this.name = name;
    this.speed = speed;
    this.travelTime = travelTime;
    this.restTime = restTime;
    this.distance = 0;
    this.points = 0;
    this.traveling = true;
    this.currTime = travelTime;
  }

  step() {
    if (this.traveling) {
      this.distance += this.speed;
    }

    this.currTime -= 1;

    if (this.currTime === 0) {
      this.traveling = !this.traveling;
      this.currTime = this.traveling ? this.travelTime : this.restTime;
    }
  }

  stats() {
    return `${this.name}: ${this.distance}km : ${this.points} Points`;
  }

  award() {
    this.points += 1;
  }
}

class Flock {
  constructor(reindeers) {
    this.reindeers = reindeers;
  }

  race(length = 1) {
    let max = 0;
    let seconds = length;

    const step = (reindeer) => {
      reindeer.step();
      max = Math.max(max, reindeer.distance);
    };

    const award = (reindeer) => {
      if (reindeer.distance === max) {
        reindeer.award();
      }
    };

    do {
      this.reindeers.forEach(step);

      this.reindeers.forEach(award);

      seconds -= 1;
    } while (seconds > 0);
  }

  resultsDistance() {
    return this.reindeers
      .sort((b, a) => a.distance - b.distance)
      .map((x) => x.stats())
      .join("\n");
  }

  resultsPoints() {
    return this.reindeers
      .sort((b, a) => a.points - b.points)
      .map((x) => x.stats())
      .join("\n");
  }
}

const reindeers = [
  new Reindeer("Rudolph", 22, 8, 165),
  new Reindeer("Cupid", 8, 17, 114),
  new Reindeer("Prancer", 18, 6, 103),
  new Reindeer("Donner", 25, 6, 145),
  new Reindeer("Dasher", 11, 12, 125),
  new Reindeer("Comet", 21, 6, 121),
  new Reindeer("Blitzen", 18, 3, 50),
  new Reindeer("Vixen", 20, 4, 75),
  new Reindeer("Dancer", 7, 20, 119),
];

const flock = new Flock(reindeers);

flock.race(2503);

console.log("Part 1:");

console.log(flock.resultsDistance());

console.log("\nPart 2:");

console.log(flock.resultsPoints());
