import { dirname, loadFileIntoArray } from "../lib/shared.js";

const COMMAND = /(toggle|turn off|turn on)\s+(\d+),(\d+)\s+through\s+(\d+),(\d+)/;

const commands = (await loadFileIntoArray(`${dirname(import.meta.url)}/data/06.txt`)).map((x) =>
  x.match(COMMAND)
);
const lights = [];
const lights2 = [];

commands.forEach((c) => {
  const [, command, x1, y1, x2, y2] = c;

  for (let i = parseInt(x1, 10); i <= parseInt(x2, 10); i += 1) {
    for (let j = parseInt(y1, 10); j <= parseInt(y2, 10); j += 1) {
      const key = i * 1000 + j;
      let val = lights2[key] || 0;
      if (command === "turn off") {
        lights[key] = false;
        val -= 1;
        if (val < 0) {
          val = 0;
        }
      } else if (command === "turn on") {
        lights[key] = true;
        val += 1;
      } else if (command === "toggle") {
        lights[key] = !lights[key];
        val += 2;
      }
      lights2[key] = val;
    }
  }
});

console.warn("Part 1:", lights.filter((x) => x).length);
console.warn(
  "Part 2:",
  lights2.reduce((sum, x) => sum + x, 0),
);
