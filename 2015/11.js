function toArr(input) {
  return input
    .split("")
    .map((s) => s.charCodeAt(0) - 97)
    .reverse();
}

function toStr(arr) {
  return String.fromCharCode(...arr.reverse().map((i) => i + 97));
}

function constraintConsecutive(arr) {
  for (let i = 2; i < arr.length; i += 1) {
    if (arr[i] + 1 === arr[i - 1] && arr[i] + 2 === arr[i - 2]) {
      return true;
    }
  }

  return false;
}

function constraintDoubles(arr) {
  let lastDouble = false;

  for (let i = 1; i < arr.length; i += 1) {
    if (arr[i] === arr[i - 1]) {
      if (lastDouble !== false && arr[i] !== lastDouble) {
        return true;
      }

      lastDouble = arr[i];
    }
  }

  return false;
}

const blacklisted = [8, 11, 14];

function increment(arr) {
  const next = arr.map((x) => {
    if (blacklisted.includes(x)) {
      return x + 1;
    }
    return x;
  });
  let index = 0;

  do {
    next[index] += 1;

    if (blacklisted.includes(next[index])) {
      index = 0;
    } else if (next[index] > 25) {
      next[index] = 0;
      index += 1;
    } else {
      index = 0;
    }
  } while (!constraintConsecutive(next) || !constraintDoubles(next));

  return next;
}

const nextPassword = (input) => toStr(increment(toArr(input)));

const next = nextPassword("cqjxjnds");

console.warn(`Part 1: ${next}`);
console.warn(`Part 2: ${nextPassword(next)}`);
