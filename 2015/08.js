import { dirname, loadFileIntoArray } from "../lib/shared.js";

const commands = await loadFileIntoArray(`${dirname(import.meta.url)}/data/08.txt`);

function parse(string) {
  const chars = string.length;
  const realChars = string
    .replace(/(^"|"$)/g, "")
    .replace(/\\"/g, '"')
    .replace(/\\x([a-f\d]{2})/g, "x")
    .replace(/\\\\/g, "\\").length;
  return chars - realChars;
}

console.warn(`Part 1: ${commands.reduce((sum, x) => sum + parse(x), 0)}`);

function parse2(string) {
  const chars = string.length;
  const encodedChars = `"${string.replace(/([\\"])/g, "\\$1")}"`.length;
  return encodedChars - chars;
}

console.warn(`Part 2: ${commands.reduce((sum, x) => sum + parse2(x), 0)}`);
