import { dirname, loadFileIntoArray } from "../lib/shared.js";
import { permutate } from "../lib/sets.js";

const guests = new Set();

const happiness = { YOU: {} };

(await loadFileIntoArray(`${dirname(import.meta.url)}/data/13.txt`))
  .map((x) => x.match(/^(\S+).+(gain|lose)\s+(\d+).+?(\S+)\.$/))
  .forEach(([, place1, mod, distanceString, place2]) => {
    const modifier = mod === "gain" ? 1 : -1;

    guests.add(place1);
    guests.add(place2);

    const distance = modifier * parseInt(distanceString, 10);

    if (!happiness[place1]) {
      happiness[place1] = {};
    }

    happiness[place1][place2] = distance;
    happiness.YOU[place1] = 0;
    happiness[place1].YOU = 0;
  });

function getPathLength(p) {
  let sum = 0;
  const path = [...p, p[0]];
  for (let i = 1; i < path.length; i += 1) {
    sum += happiness[path[i - 1]][path[i]];
    sum += happiness[path[i]][path[i - 1]];
  }
  return sum;
}

function getMaxHappiness(places) {
  const permutations = permutate(Array.from(places));

  let max = 0;

  for (const path of permutations) {
    max = Math.max(max, getPathLength(path));
  }

  return max;
}

console.warn("Part 1", getMaxHappiness(guests));

guests.add("YOU");

console.warn("Part 2", getMaxHappiness(guests));
