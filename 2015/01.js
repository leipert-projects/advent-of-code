import { dirname, loadFile } from "../lib/shared.js";

const input = await loadFile(`${dirname(import.meta.url)}/data/01.txt`);

let firstBaseChar = 0;

const floor = input.split("").reduce((acc, char, index) => {
  if (!firstBaseChar && acc < 0) {
    firstBaseChar = index;
  }

  if (char === "(") {
    return acc + 1;
  }
  if (char === ")") {
    return acc - 1;
  }

  return acc;
}, 0);

console.warn("Part 1 ", floor);
console.warn("Part 2 ", firstBaseChar);
