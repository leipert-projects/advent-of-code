import { dirname, loadFileIntoArray } from "../lib/shared.js";

const data = (await loadFileIntoArray(`${dirname(import.meta.url)}/data/19.txt`)).reverse();

const rules = data.filter((x) => x.includes("=>"));

const medicine = data[0];

const replace = (input, substitutions) => {
  const res = new Set();

  substitutions.forEach(([x, y]) => {
    const chunks = input.split(x);

    for (let i = 1; i < chunks.length; i += 1) {
      res.add(chunks.slice(0, i).join(x) + y + chunks.slice(i).join(x));
    }
  });

  return res;
};

const res = replace(
  medicine,
  rules.map((x) => x.split(" => ")),
);

console.warn(`Part 1: `, res.size);

const REPLACEMENTS = rules.reduce(
  (map, r) => map.set(r.split(" => ")[1], r.split(" => ")[0]),
  new Map(),
);

let MOLECULE = medicine;
let count = 0;

const filter = (x) => MOLECULE.includes(x);

const replacer = (match) => {
  count += 1;
  return REPLACEMENTS.get(match);
};

const keys = [...REPLACEMENTS.keys()];

while (MOLECULE !== "e") {
  const matchingMolecules = keys.filter(filter);
  if (!matchingMolecules.length) {
    MOLECULE = medicine;
    count = 0;
    // eslint-disable-next-line no-continue
    continue;
  }
  const randomMolecule = matchingMolecules[Math.round(Math.random() * matchingMolecules.length)];

  MOLECULE = MOLECULE.replace(randomMolecule, replacer);
}

console.log(`Part 2: e -> Medicine in: ${count} steps`);
