## 2015/data/11.txt

```
Part 1: cqjxxyzz
Part 2: cqkaabcc
```

| Command                                                                             |  Mean [ms] | Min [ms] | Max [ms] |    Relative |
| :---------------------------------------------------------------------------------- | ---------: | -------: | -------: | ----------: |
| `bun run 2015/11.js < /dev/null`                                                    | 31.8 ± 1.5 |     29.9 |     43.6 |        1.00 |
| `deno run --allow-read=./ --no-config --allow-net=deno.land 2015/11.js < /dev/null` | 37.8 ± 0.4 |     37.1 |     38.8 | 1.19 ± 0.06 |
| `node 2015/11.js < /dev/null`                                                       | 56.4 ± 0.4 |     55.4 |     57.7 | 1.77 ± 0.09 |

- bun 1.0.13
- deno 1.38.2 (release, aarch64-apple-darwin)
- node v21.2.0
