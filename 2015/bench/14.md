## 2015/data/14.txt

```
Part 1:
Cupid: 2696km : 838 Points
Rudolph: 2640km : 1084 Points
Blitzen: 2592km : 0 Points
Vixen: 2560km : 13 Points
Donner: 2550km : 277 Points
Dancer: 2527km : 199 Points
Comet: 2520km : 121 Points
Dasher: 2508km : 0 Points
Prancer: 2484km : 24 Points

Part 2:
Rudolph: 2640km : 1084 Points
Cupid: 2696km : 838 Points
Donner: 2550km : 277 Points
Dancer: 2527km : 199 Points
Comet: 2520km : 121 Points
Prancer: 2484km : 24 Points
Vixen: 2560km : 13 Points
Blitzen: 2592km : 0 Points
Dasher: 2508km : 0 Points
```

| Command                                                                             |  Mean [ms] | Min [ms] | Max [ms] |    Relative |
| :---------------------------------------------------------------------------------- | ---------: | -------: | -------: | ----------: |
| `bun run 2015/14.js < /dev/null`                                                    | 11.6 ± 0.4 |     11.0 |     15.8 |        1.00 |
| `deno run --allow-read=./ --no-config --allow-net=deno.land 2015/14.js < /dev/null` | 17.4 ± 0.7 |     16.8 |     23.6 | 1.50 ± 0.08 |
| `node 2015/14.js < /dev/null`                                                       | 35.3 ± 0.7 |     34.4 |     37.6 | 3.04 ± 0.12 |

- bun 1.0.13
- deno 1.38.2 (release, aarch64-apple-darwin)
- node v21.2.0
