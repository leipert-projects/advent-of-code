import { dirname, loadIntegerIntoArray } from "../lib/shared.js";

const containerSizes = loadIntegerIntoArray(`${dirname(import.meta.url)}/data/17.txt`);

const res = [];

let minContainerCount = Number.MAX_SAFE_INTEGER;

function recurse(stack, sum, usedContainers = []) {
  for (let index = usedContainers[0] || 0; index < stack.length; index += 1) {
    if (usedContainers.includes(index)) {
      // eslint-disable-next-line no-continue
      continue;
    }

    const eggnogLeft = sum - stack[index];
    if (eggnogLeft < 0) {
      // eslint-disable-next-line no-continue
      continue;
    }

    const containers = [index, ...usedContainers];

    if (eggnogLeft === 0) {
      res.push(containers);
      minContainerCount = Math.min(minContainerCount, containers.length);
      // eslint-disable-next-line no-continue
      continue;
    }

    recurse(stack, eggnogLeft, containers);
  }
}

recurse(containerSizes, 150);
console.warn("Part 1:", res.length);

const minContainers = res.filter((config) => config.length === minContainerCount);

console.warn("Part 2:", minContainers.length);
