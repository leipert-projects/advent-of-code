import { dirname, loadFile } from "../lib/shared.js";

const input = await loadFile(`${dirname(import.meta.url)}/data/03.txt`);

const path = (string, santas) => {
  const arr = string.split("");

  const pos = [
    [0, 0],
    [0, 0],
  ];

  const res = new Set();

  res.add("0,0");

  arr.forEach((x, index) => {
    const i = (index + 1) % santas;

    if (x === "^") {
      pos[i][0] += 1;
    } else if (x === "v") {
      pos[i][0] -= 1;
    } else if (x === "<") {
      pos[i][1] += 1;
    } else if (x === ">") {
      pos[i][1] -= 1;
    }
    const key = pos[i].join();
    res.add(key);
  });

  return res;
};

console.warn("Path 1", path(input, 1).size);
console.warn("Path 2", path(input, 2).size);
