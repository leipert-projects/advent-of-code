const regex1 = /(\d)\1*/g;

function lookAndSay(str1) {
  let res = "";

  let array1;

  do {
    array1 = regex1.exec(str1);
    if (array1) {
      res += array1[0].length;
      res += array1[0][0];
    }
  } while (array1 !== null);
  return res;
}

let res = "1321131112";

for (let i = 0; i < 40; i += 1) {
  res = lookAndSay(res);
}

console.warn(`Part 1: ${res.length}`);

for (let i = 0; i < 10; i += 1) {
  res = lookAndSay(res);
}

console.warn(`Part 2: ${res.length}`);
