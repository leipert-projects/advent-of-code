import { dirname, loadFileIntoArray } from "../lib/shared.js";

const input = await loadFileIntoArray(`${dirname(import.meta.url)}/data/02.txt`);

function measurePaper(curr) {
  const [l, w, h] = curr.split("x").map((x) => parseInt(x, 10));

  const area = [l * w, w * h, h * l];

  return 2 * (area[0] + area[1] + area[2]) + Math.min(...area);
}

const wrappingPaperSum = input.reduce((sum, curr) => sum + measurePaper(curr), 0);

console.warn("Part 1: ", wrappingPaperSum);

function measureRibbon(curr) {
  const [l, w, h] = curr.split("x").map((x) => parseInt(x, 10));

  const area = [l, w, h];

  return l * h * w + 2 * (l + h + w - Math.max(...area));
}

const ribbonSum = input.reduce((sum, curr) => sum + measureRibbon(curr), 0);

console.warn("Part 2: ", ribbonSum);
