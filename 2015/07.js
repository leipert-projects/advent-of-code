import { dirname, loadFileIntoArray } from "../lib/shared.js";

const commands = await loadFileIntoArray(`${dirname(import.meta.url)}/data/07.txt`);

const target = /(.+)\s+->\s+([a-z]+)/;

const circuit = commands.reduce((acc, entry) => {
  const [, command, key] = entry.match(target);

  acc[key] = command;

  return acc;
}, {});

const UNARY = /^(NOT)\s+([a-z\d]+)$/;
const BINARY = /^([a-z\d]+)\s+(AND|OR|LSHIFT|RSHIFT)\s+([a-z\d]+)$/;

function evaluate(acc, key) {
  if (key.match(/^\d+$/)) {
    return parseInt(key, 10);
  }

  const command = acc[key];

  if (Number.isFinite(command)) {
    return command;
  }

  if (command.match(/^[a-z\d]+$/)) {
    acc[key] = evaluate(acc, command);
    return acc[key];
  }

  const binary = command.match(BINARY);

  /* eslint-disable no-bitwise */
  if (binary) {
    if (binary[2] === "OR") {
      acc[key] = evaluate(acc, binary[1]) | evaluate(acc, binary[3]);
    } else if (binary[2] === "AND") {
      acc[key] = evaluate(acc, binary[1]) & evaluate(acc, binary[3]);
    } else if (binary[2] === "LSHIFT") {
      acc[key] = evaluate(acc, binary[1]) << evaluate(acc, binary[3]);
    } else if (binary[2] === "RSHIFT") {
      acc[key] = evaluate(acc, binary[1]) >> evaluate(acc, binary[3]);
    }
    return acc[key];
  }

  const unary = command.match(UNARY);
  if (unary) {
    acc[key] = ~evaluate(acc, unary[2]);
    return acc[key];
  }
  /* eslint-enable no-bitwise */

  throw new Error(command);
}

const result = evaluate({ ...circuit }, "a");

console.warn("Part 1:", result);
console.warn("Part 2:", evaluate({ ...circuit, b: `${result}` }, "a"));
