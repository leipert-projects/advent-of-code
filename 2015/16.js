import { dirname, loadFileIntoArray } from "../lib/shared.js";

const analysisResult = {
  children: 3,
  cats: 7,
  samoyeds: 2,
  pomeranians: 3,
  akitas: 0,
  vizslas: 0,
  goldfish: 5,
  trees: 3,
  cars: 2,
  perfumes: 1,
};

const susans = await loadFileIntoArray(`${dirname(import.meta.url)}/data/16.txt`);

const matchSusan = ([key, value]) => analysisResult[key] === parseInt(value, 10);

const matchSusan2 = ([key, value]) => {
  const comp = parseInt(value, 10);

  if (["cats", "trees"].includes(key)) {
    return analysisResult[key] < comp;
  }

  if (["pomeranians", "goldfish"].includes(key)) {
    return analysisResult[key] > comp;
  }

  return analysisResult[key] === comp;
};

for (const sue of susans) {
  const [name, ...rest] = sue.split(":");

  const props = rest
    .join(":")
    .split(",")
    .map((x) => x.trim().split(":"));

  if (props.every(matchSusan)) {
    console.log(`Part '1': ${name}`);
  }

  if (props.every(matchSusan2)) {
    console.log(`Part '2': ${name}`);
  }
}
