import { dirname, loadFileIntoArray } from "../lib/shared.js";

const passwords = await loadFileIntoArray(`${dirname(import.meta.url)}/data/05.txt`);

const DOUBLE_MATCH = /([a-z])\1/;
const VOWEL_MATCH = /[aeiou].*[aeiou].*[aeiou]/;

function matchesConstraint(input) {
  if (
    input.includes("ab") ||
    input.includes("cd") ||
    input.includes("pq") ||
    input.includes("xy")
  ) {
    return false;
  }

  if (!DOUBLE_MATCH.test(input)) {
    return false;
  }

  return VOWEL_MATCH.test(input);
}

const DOUBLE_DOUBLE_MATCH = /([a-z][a-z]).*\1/;
const DOUBLE_WITH_ONE_MATCH = /([a-z]).\1/;

function matchesConstraint2(input) {
  if (!DOUBLE_DOUBLE_MATCH.test(input)) {
    return false;
  }

  return DOUBLE_WITH_ONE_MATCH.test(input);
}

console.log("Part 1:", passwords.filter(matchesConstraint).length);
console.log("Part 2:", passwords.filter(matchesConstraint2).length);
