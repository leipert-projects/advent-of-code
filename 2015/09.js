import { dirname, loadFileIntoArray } from "../lib/shared.js";
import { permutate } from "../lib/sets.js";

const places = new Set();

const distances = {};

(await loadFileIntoArray(`${dirname(import.meta.url)}/data/09.txt`))
  .map((x) => x.match(/(.+)\s+to\s+(.+)\s+=\s+(\d+)/))
  .forEach(([, place1, place2, distanceString]) => {
    places.add(place1);
    places.add(place2);

    const distance = parseInt(distanceString, 10);

    if (!distances[place1]) {
      distances[place1] = {};
    }
    if (!distances[place2]) {
      distances[place2] = {};
    }

    distances[place1][place2] = distance;
    distances[place2][place1] = distance;
  });

function getPathLength(path) {
  let sum = 0;
  for (let i = 1; i < path.length; i += 1) {
    sum += distances[path[i - 1]][path[i]];
  }
  return sum;
}

const permutations = permutate(Array.from(places)).map(getPathLength);

console.warn("Part 1", Math.min(...permutations));
console.warn("Part 2", Math.max(...permutations));
