import { dirname, loadFileIntoArray, sumArray, toInteger } from "../lib/shared.js";

const loadIngredientStats = (input) => {
  const ingredients = [
    [], // capacity
    [], // durability
    [], // texture
    [], // capacity
    [], // calories
  ];

  input.forEach((x) => {
    const match = x.match(
      /capacity (-?\d+), durability (-?\d+), flavor (-?\d+), texture (-?\d+), calories (-?\d+)/,
    );

    if (match) {
      for (let i = 1; i < 6; i += 1) {
        ingredients[i - 1].push(toInteger(match[i]));
      }
    }
  });

  return ingredients;
};

const createScoreFunction = (ingredientStats, calorieConstraint) => {
  const getSum = (stats, counts) => {
    const sum = sumArray(stats.map((x, index) => counts[index] * x));

    return sum > 0 ? sum : 0;
  };

  return (counts) => {
    if (calorieConstraint > 0 && getSum(ingredientStats[4], counts) !== calorieConstraint) {
      return 0;
    }

    let res = 1;

    for (let i = 0; i < 4; i += 1) {
      res *= getSum(ingredientStats[i], counts);
      if (res === 0) {
        return 0;
      }
    }

    return res;
  };
};

const findBestMix = (input, calorieConstraint = 0) => {
  const getScore = createScoreFunction(loadIngredientStats(input), calorieConstraint);
  const searchSpace = 100 ** input.length / 2 + 1;
  const stringLength = 2 * input.length;

  let max = 0;

  for (let i = 199; i < searchSpace; i += 99) {
    const inputArray = i.toString().padStart(stringLength, "0").match(/\d{2}/g).map(toInteger);
    if (sumArray(inputArray) === 100) {
      max = Math.max(max, getScore(inputArray), getScore(inputArray.reverse()));
    }
  }
  return max;
};

const input = await loadFileIntoArray(`${dirname(import.meta.url)}/data/15.txt`);

console.warn("Part 1:", findBestMix(input));

console.warn("Part 2:", findBestMix(input, 500));
