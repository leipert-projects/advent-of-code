import { dirname, loadFile, sumArray } from "../lib/shared.js";

const sourceFile = (await loadFile(`${dirname(import.meta.url)}/data/18.txt`))
  .replace(/\s+/gm, "")
  .split("")
  .map((x) => (x === "#" ? 1 : 0));

const playConwaysGame = (startState, gridWidth, steps, fixed = []) => {
  const splitter = new RegExp(`.{${gridWidth}}`, "g");

  const print = (state) => {
    const formatted = state
      .join("")
      .match(splitter)
      .join("\n")
      .replace(/1/g, "#")
      .replace(/0/g, ".");

    console.log(`\n${formatted}\n`);
  };

  const getState = (state, i) => (fixed.includes(i) ? 1 : state[i] || 0);

  let state = [...startState];

  for (let i = 0; i < steps; i += 1) {
    const newState = [];

    for (let pos = 0; pos < state.length; pos += 1) {
      let neighbors = 0;

      neighbors += getState(state, pos - gridWidth);
      neighbors += getState(state, pos + gridWidth);

      if (pos % gridWidth !== 0) {
        neighbors += getState(state, pos - 1 - gridWidth);
        neighbors += getState(state, pos - 1);
        neighbors += getState(state, pos - 1 + gridWidth);
      }
      if (pos % gridWidth !== gridWidth - 1) {
        neighbors += getState(state, pos + 1 - gridWidth);
        neighbors += getState(state, pos + 1);
        neighbors += getState(state, pos + 1 + gridWidth);
      }

      newState[pos] =
        fixed.includes(pos) || (state[pos] === 1 && neighbors === 2) || neighbors === 3 ? 1 : 0;
    }

    print(newState);
    state = newState;
  }

  return state;
};

console.log(`Part 1: ${sumArray(playConwaysGame(sourceFile, 100, 100))}`);
console.log(`Part 2: ${sumArray(playConwaysGame(sourceFile, 100, 100, [0, 99, 9900, 9999]))}`);
