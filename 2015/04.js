import crypto from "node:crypto";

function md5(encryptString) {
  return crypto.createHash("md5").update(encryptString).digest("hex");
}

function find(prefix) {
  let hash = "";

  let c = -1;

  do {
    c += 1;

    hash = md5(`bgvyzdsv${c}`);
  } while (!hash.startsWith(prefix));
  return c;
}

console.warn(`Part 1: ${find("00000")}`);
console.warn(`Part 2: ${find("000000")}`);
