const PACKAGE_COUNT = 29000000;

function day20() {
  const MAX = Math.floor(PACKAGE_COUNT / 11);
  const counts = new Array(MAX).fill(0);
  const counts2 = new Array(MAX).fill(0);
  let found1 = false;
  let found2 = false;
  for (let house = 1; house < MAX; house += 1) {
    for (let elf = 1; elf < MAX / house; elf += 1) {
      counts[elf * house - 1] += house * 10;
      if (elf <= 50) {
        counts2[elf * house - 1] += house * 11;
      }
    }
    if (!found1 && counts[house - 1] >= PACKAGE_COUNT) {
      console.log(`Part 1: ${house}`);
      found1 = true;
    }
    if (!found2 && counts2[house - 1] >= PACKAGE_COUNT) {
      console.log(`Part 2: ${house}`);
      found2 = true;
    }
    if (found1 && found2) {
      break;
    }
  }
}

day20();
