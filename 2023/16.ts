import { exit, loadStdInIntoArray, sumArray } from "../lib/shared.js";

interface Node {
  value: string; // '.' | '\\' | '/' | '|' | '-'
  visited: Array<Direction>;
  x: number;
  y: number;
}
function buildMap(data: string[]) {
  const map: Node[][] = [];
  for (let y = 0; y < data.length; y += 1) {
    const row = data[y];
    for (let x = 0; x < row.length; x += 1) {
      map[x] ||= [];
      map[x][y] = { value: row[x], visited: [], x, y };
    }
  }
  return map;
}

enum Direction {
  Right = 3,
  Left = 5,
  Top = 7,
  Bottom = 11,
  Sideways = Right * Left,
  UpDown = Top * Bottom,
}

function pushNodeIfNecessary(
  map: Node[][],
  node: Node,
  dir: Direction,
  stack: Array<[Node, Direction]>,
) {
  let nextNode;
  if (dir === Direction.Left) {
    nextNode = map[node.x - 1]?.[node.y];
  } else if (dir === Direction.Top) {
    nextNode = map[node.x]?.[node.y - 1];
  } else if (dir === Direction.Bottom) {
    nextNode = map[node.x]?.[node.y + 1];
  } else if (dir === Direction.Right) {
    nextNode = map[node.x + 1]?.[node.y];
  }
  if (nextNode) {
    stack.push([nextNode, dir]);
  }
}

function countEnergizedTiles(data: string[], x: number, y: number, d: Direction): number {
  const map = buildMap(data);
  const stack: Array<[Node, Direction]> = [[map[x][y]!, d]];
  while (stack.length) {
    const [node, dir] = stack.shift()!;
    node.visited ||= [];
    if (node.visited.includes(dir)) {
      continue;
    }
    node.visited.push(dir);
    if (node.value === ".") {
      pushNodeIfNecessary(map, node, dir, stack);
    } else if (node.value === "|") {
      if (Direction.Sideways % dir === 0) {
        pushNodeIfNecessary(map, node, Direction.Top, stack);
        pushNodeIfNecessary(map, node, Direction.Bottom, stack);
      } else {
        pushNodeIfNecessary(map, node, dir, stack);
      }
    } else if (node.value === "-") {
      if (Direction.UpDown % dir === 0) {
        pushNodeIfNecessary(map, node, Direction.Left, stack);
        pushNodeIfNecessary(map, node, Direction.Right, stack);
      } else {
        pushNodeIfNecessary(map, node, dir, stack);
      }
    } else if (node.value === "/") {
      switch (dir) {
        case Direction.Right:
          pushNodeIfNecessary(map, node, Direction.Top, stack);
          break;
        case Direction.Left:
          pushNodeIfNecessary(map, node, Direction.Bottom, stack);
          break;
        case Direction.Bottom:
          pushNodeIfNecessary(map, node, Direction.Left, stack);
          break;
        case Direction.Top:
          pushNodeIfNecessary(map, node, Direction.Right, stack);
          break;
      }
    } else if (node.value === "\\") {
      switch (dir) {
        case Direction.Right:
          pushNodeIfNecessary(map, node, Direction.Bottom, stack);
          break;
        case Direction.Left:
          pushNodeIfNecessary(map, node, Direction.Top, stack);
          break;
        case Direction.Bottom:
          pushNodeIfNecessary(map, node, Direction.Right, stack);
          break;
        case Direction.Top:
          pushNodeIfNecessary(map, node, Direction.Left, stack);
          break;
      }
    } else {
      console.log(node.value);
    }
  }
  return sumArray(map.flat(2).map((node) => Math.sign(node.visited?.length ?? 0)));
}

function maximizeEnergizedTiles(stdIn: string[]) {
  const maxX = stdIn[0].length - 1;
  const maxY = stdIn.length - 1;
  let maxEnergy = 0;
  for (let y = 0; y <= maxY; y += 1) {
    maxEnergy = Math.max(
      maxEnergy,
      countEnergizedTiles(stdIn, 0, y, Direction.Right),
      countEnergizedTiles(stdIn, maxX, y, Direction.Left),
    );
  }
  for (let x = 0; x < maxX; x += 1) {
    maxEnergy = Math.max(
      maxEnergy,
      countEnergizedTiles(stdIn, x, 0, Direction.Bottom),
      countEnergizedTiles(stdIn, x, maxY, Direction.Top),
    );
  }
  return maxEnergy;
}

try {
  const stdIn = await loadStdInIntoArray();

  const part1 = countEnergizedTiles(stdIn, 0, 0, Direction.Right);

  console.log(`\tPart 1: ${part1}`);

  const part2 = maximizeEnergizedTiles(stdIn);

  console.log(`\tPart 2: ${part2}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
