import { exit, loadStdInIntoArray, sumArray } from "../lib/shared.js";
function part1Fn(data: string[][]): number[] {
  return data.map((card) => {
    const [, _winningNumbers, cardNumbers] = card;
    const winningNumbers = _winningNumbers.trim().split(/\s+/);
    const matches = cardNumbers.trim().split(/\s+/).filter((x) => winningNumbers.includes(x));
    return matches.length ? 2 ** (matches.length - 1) : 0;
  });
}

function part2Fn(data: number[]) {
  const result = Array(data.length).fill(1);
  for (let i = 0; i < result.length; i += 1) {
    const wins = Math.max(0, Math.log2(data[i]) + 1);
    for (let j = 0; j < wins; j += 1) {
      result[j + i + 1] += result[i];
    }
  }
  return result;
}

try {
  const stdIn: string[][] = (await loadStdInIntoArray()).map((line) => line.split(/[|:]/));
  const part1 = part1Fn(stdIn);

  console.log(`\tPart 1: Total points ${sumArray(part1)}`);

  const part2 = part2Fn(part1);

  console.log(`\tPart 2: Scratch cards ${sumArray(part2)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
