import { permutate } from "../lib/sets.js";
import { exit, loadStdInIntoArray, sumArray, toInteger } from "../lib/shared.js";

function mapCard(card: string, withJokers = false) {
  switch (card) {
    case "A":
      return 14;
    case "K":
      return 13;
    case "Q":
      return 12;
    case "J":
      return withJokers ? 1 : 11;
    case "T":
      return 10;
    default:
      return toInteger(card);
  }
}

enum GameValue {
  HighCard,
  OnePair,
  TwoPairs,
  ThreeOfAKind,
  FullHouse,
  FourOfAKind,
  FiveOfAKind,
}

class Hand {
  public readonly bid: number;
  public readonly cards: [number, number, number, number, number];
  public readonly gameValue: GameValue;

  constructor(line: string, withJokers = false) {
    const [cards, bid] = line.trim().split(/\s+/);
    this.bid = toInteger(bid);
    this.cards = [
      mapCard(cards[0], withJokers),
      mapCard(cards[1], withJokers),
      mapCard(cards[2], withJokers),
      mapCard(cards[3], withJokers),
      mapCard(cards[4], withJokers),
    ];
    this.gameValue = Hand.calculateGameValue(this.cards);
  }

  compare(otherHand: Hand) {
    const gameDiff = this.gameValue - otherHand.gameValue;
    if (gameDiff !== 0) {
      return this.gameValue - otherHand.gameValue;
    }
    for (let i = 0; i < 5; i += 1) {
      const diff = this.cards[i] - otherHand.cards[i];
      if (diff !== 0) {
        // console.log(this.cards, otherHand.cards, Math.sign(diff))
        return diff;
      }
    }
    throw new Error("Should never happen");
  }

  private static calculateGameValue(cards: number[]): GameValue {
    const cardCount = new Array(15).fill(0);
    for (const value of cards) {
      cardCount[value] += 1;
    }
    if (cardCount.includes(5)) {
      return GameValue.FiveOfAKind;
    }

    const jokerCount = cardCount[1];

    if (jokerCount) {
      const nonJokers = cards.filter((x) => x !== 1);
      const nonJokersUniq = Array.from(new Set(nonJokers));
      const combinations = permutate(
        new Array(jokerCount).fill(nonJokersUniq).flat(),
      );

      return Math.max(
        ...combinations.map((replacedJokers) =>
          Hand.calculateGameValue([...nonJokers, ...replacedJokers.slice(0, jokerCount)])
        ),
      );
    }

    if (cardCount.includes(4)) {
      return GameValue.FourOfAKind;
    }
    const idx = cardCount.indexOf(2);
    if (cardCount.includes(3)) {
      if (idx > -1) {
        return GameValue.FullHouse;
      }
      return GameValue.ThreeOfAKind;
    }
    if (idx > -1) {
      if (cardCount.includes(2, idx + 1)) {
        return GameValue.TwoPairs;
      }
      return GameValue.OnePair;
    }

    return GameValue.HighCard;
  }

  toString() {
    return `${this.cards} Bid: ${this.bid} Game Value: ${GameValue[this.gameValue]}`;
  }
}

function part1Fn(data: string[]) {
  return data.map((line) => new Hand(line))
    .sort((a, b) => a.compare(b))
    .map((hand, idx) => {
      return (idx + 1) * hand.bid;
    });
}

function part2Fn(data: string[]) {
  return data.map((line) => new Hand(line, true))
    .sort((a, b) => a.compare(b))
    .map((hand, idx) => {
      return (idx + 1) * hand.bid;
    });
}

try {
  const stdIn = await loadStdInIntoArray();
  const part1 = part1Fn(stdIn);

  console.log(`\tPart 1: Total Winnings ${sumArray(part1)}`);

  const part2 = part2Fn(stdIn);

  console.log(`\tPart 2: Total Winnings with jokers ${sumArray(part2)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
