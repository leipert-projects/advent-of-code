import { exit, loadStdIn, sumArray, toInteger } from "../lib/shared.js";

function HASH(string: string): number {
  let c = 0;
  for (let i = 0; i < string.length; i += 1) {
    c += string.charCodeAt(i);
    c *= 17;
    c %= 256;
  }
  return c;
}

function HASHMAP(commands: string[]): Array<Map<string, number>> {
  const hashmap = Array(256);
  for (const command of commands) {
    const [key, action, num] = command.split(/([=-])/);
    const box = HASH(key);
    hashmap[box] ||= new Map();
    if (action === "-") {
      hashmap[box].delete(key);
    } else {
      hashmap[box].set(key, toInteger(num));
    }
  }
  return hashmap;
}

function focusingPower(hashmap: Map<string, number>[]): number {
  let power = 0;
  for (let i = 0; i < hashmap.length; i++) {
    const box = hashmap[i];
    if (!box) {
      continue;
    }
    let c = 0;
    for (const lens of box.values()) {
      c += 1;
      power += (i + 1) * c * lens;
    }
  }
  return power;
}

try {
  const stdIn = (await loadStdIn()).trim().split(",");

  console.log(`\tPart 1: ${sumArray(stdIn.map(HASH))}`);
  console.log(`\tPart 2: ${focusingPower(HASHMAP(stdIn))}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
