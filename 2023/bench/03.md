## 2023/data/03_example.txt

```
Part 1: Sum of Part numbers 4361
Part 2: Gear Ratios 467835
```

| Command                                                                                   |  Mean [ms] | Min [ms] | Max [ms] |    Relative |
| :---------------------------------------------------------------------------------------- | ---------: | -------: | -------: | ----------: |
| `bun run 2023/03.ts < ./2023/data/03_example.txt`                                         | 14.0 ± 1.7 |     13.2 |     30.0 |        1.00 |
| `deno run --allow-read=./,/dev/stdin --no-config 2023/03.ts < ./2023/data/03_example.txt` | 17.4 ± 0.6 |     16.6 |     20.3 | 1.25 ± 0.16 |
| `node 2023/03.ts.js < ./2023/data/03_example.txt`                                         | 35.5 ± 0.4 |     34.4 |     37.0 | 2.54 ± 0.31 |

## 2023/data/03.txt

```
Part 1: Sum of Part numbers 531932
Part 2: Gear Ratios 73646890
```

| Command                                                                           |   Mean [ms] | Min [ms] | Max [ms] |    Relative |
| :-------------------------------------------------------------------------------- | ----------: | -------: | -------: | ----------: |
| `bun run 2023/03.ts < ./2023/data/03.txt`                                         | 181.1 ± 1.2 |    179.6 |    184.1 |        1.00 |
| `deno run --allow-read=./,/dev/stdin --no-config 2023/03.ts < ./2023/data/03.txt` | 209.4 ± 1.3 |    207.8 |    212.2 | 1.16 ± 0.01 |
| `node 2023/03.ts.js < ./2023/data/03.txt`                                         | 227.0 ± 0.9 |    225.5 |    228.9 | 1.25 ± 0.01 |

- bun 1.0.13
- deno 1.38.2 (release, aarch64-apple-darwin)
- node v21.2.0
