import { exit, loadStdInIntoArray } from "../lib/shared.js";
import { combinate, manhattanDistance } from "../lib/math.js";

function buildMap(data: string[], scale = 2): [number, number][] {
  let yOffset = 0;
  const xCount: number[] = [];
  const yCount: number[] = [];
  const rawNodes: [number, number][] = [];
  for (let y = 0; y < data.length; y += 1) {
    const line = data[y];
    let emptyRow = true;
    for (let x = 0; x < line.length; x += 1) {
      xCount[x] ||= 0;
      if (line[x] === "#") {
        xCount[x] += 1;
        emptyRow = false;
        rawNodes.push([x, y]);
      }
    }
    if (emptyRow) {
      yOffset += 1;
    }
    yCount.push(yOffset);
  }
  let xOffset = 0;
  for (let i = 0; i < xCount.length; i += 1) {
    if (xCount[i] === 0) {
      xOffset += 1;
    }
    xCount[i] = xOffset;
  }

  return rawNodes.map(([x, y]) => {
    return [xCount[x] * (scale - 1) + x, yCount[y] * (scale - 1) + y];
  });
}

function sumOfGalaxyDistances(nodes: [number, number][]): number {
  let sum = 0;
  for (const pair of combinate(nodes.length, 2)) {
    sum += manhattanDistance(
      nodes[pair[1]],
      nodes[pair[0]],
    );
  }
  return sum;
}

try {
  const data = await loadStdInIntoArray();
  const part1 = sumOfGalaxyDistances(buildMap(data));

  console.log(`\tPart 1: ${part1}`);

  const part2 = sumOfGalaxyDistances(buildMap(data, 1000000));

  console.log(`\tPart 2: ${part2}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
