import { exit, loadStdInIntoArray, sumArray } from "../lib/shared.js";

function toDigit(str: string | undefined): number {
  const res = {
    one: 1,
    two: 2,
    three: 3,
    four: 4,
    five: 5,
    six: 6,
    seven: 7,
    eight: 8,
    nine: 9,
    eno: 1,
    owt: 2,
    eerht: 3,
    ruof: 4,
    evif: 5,
    xis: 6,
    neves: 7,
    thgie: 8,
    enin: 9,
    "1": 1,
    "2": 2,
    "3": 3,
    "4": 4,
    "5": 5,
    "6": 6,
    "7": 7,
    "8": 8,
    "9": 9,
  }[`${str}`];
  if (!res) {
    console.log(`Unknown ${str}`);
    return -1;
  }
  return res;
}
function part1Fn(data: string[][]): number {
  return sumArray(data.map(([line, reverse]) => {
    const first = line.match(/[0-9]/g);
    const last = reverse.match(/[0-9]/g);
    return toDigit(first?.[0]) * 10 + toDigit(last?.[0]);
  }));
}

function part2Fn(data: string[][]): number {
  return sumArray(data.map(([line, reverse]) => {
    const first = line.match(/[0-9]|one|two|three|four|five|six|seven|eight|nine/);
    const last = reverse.match(/[0-9]|eno|owt|eerht|ruof|evif|xis|neves|thgie|enin/);
    return toDigit(first?.[0]) * 10 + toDigit(last?.[0]);
  }));
}

try {
  const stdIn = (await loadStdInIntoArray()).map(
    (line) => [line, line.split("").reverse().join("")],
  );
  const part1 = part1Fn(stdIn);

  console.log(`\tPart 1: calibration sum ${part1}`);

  const part2 = part2Fn(stdIn);

  console.log(`\tPart 2: calibration sum ${part2}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
