import { bisect, multiplyArray } from "../lib/math.js";
import { exit, loadStdInIntoArray, toInteger } from "../lib/shared.js";

function howManyWays(time: number, prevRecord: number): number {
  const distance = (c: number) => (time - c) * c - prevRecord;

  let foo;

  do {
    foo = Math.round(Math.random() * time);
  } while (distance(foo) <= 0);

  const startBound = bisect(
    distance,
    0,
    foo,
    1,
  );

  const start = Math.min(
    distance(startBound - 1) > 0 ? startBound - 1 : time,
    distance(startBound) > 0 ? startBound : time,
    distance(startBound + 1) > 0 ? startBound + 1 : time,
  );

  const endBound = bisect(
    distance,
    foo,
    time,
    1,
  );

  const end = Math.max(
    distance(endBound - 1) > 0 ? endBound - 1 : 0,
    distance(endBound) > 0 ? endBound : 0,
    distance(endBound + 1) > 0 ? endBound + 1 : 0,
  );

  return end - start + 1;
}
function part1Fn(data: number[][]) {
  const [time, distance] = data;
  const results = [];
  for (const j in time) {
    results.push(howManyWays(time[j], distance[j]));
  }
  return results;
}

function part2Fn(data: number[][]) {
  const [_time, _distance] = data;
  const time = _time.join("");
  const distance = _distance.join("");
  return howManyWays(toInteger(time), toInteger(distance));
}

try {
  const stdIn = (await loadStdInIntoArray()).map((line) =>
    (line.match(/\d+/g) ?? []).map(toInteger)
  );
  const part1 = part1Fn(stdIn);

  console.log(`\tPart 1: ${multiplyArray(part1)}`);

  const part2 = part2Fn(stdIn);
  console.log(`\tPart 2: ${part2}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
