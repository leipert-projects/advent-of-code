import { exit, loadStdInIntoArray, toInteger } from "../lib/shared.js";
function getNextValue(sequences: number[][]) {
  for (let i = 1; i < sequences.length; i += 1) {
    sequences[i].unshift(sequences[i][0] + sequences[i - 1][0]);
  }
  return sequences[sequences.length - 1][0];
}

function getPrevValue(sequences: number[][]) {
  for (let i = 1; i < sequences.length; i += 1) {
    sequences[i] = sequences[i].reverse();
    sequences[i].unshift(sequences[i][0] - sequences[i - 1][0]);
  }
  return sequences[sequences.length - 1][0];
}

function part1Fn(data: number[][][]) {
  let res = 0;
  for (const line of data) {
    res += getNextValue(line);
  }
  return res;
}

function part2Fn(data: number[][][]) {
  let res = 0;
  for (const line of data) {
    res += getPrevValue(line);
  }
  return res;
}

try {
  const sequences = (await loadStdInIntoArray())
    .map((line) => {
      const initialSequence = line.split(/\s+/).map(toInteger).reverse();
      const sequences = [initialSequence];
      while (sequences[0].some((x) => x !== 0)) {
        sequences.unshift(sequences[0].flatMap((value, idx, arr) => {
          if (idx + 1 === arr.length) {
            return [];
          }
          return value - arr[idx + 1];
        }));
      }
      return sequences;
    });
  const part1 = part1Fn(sequences);

  console.log(`\tPart 1: ${part1}`);

  const part2 = part2Fn(sequences);

  console.log(`\tPart 2: ${part2}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
