import { exit, loadStdInIntoArray, toInteger } from "../lib/shared.js";

interface ConverterMap {
  [key: string]: Array<[number, number, number]>;
}

class Almanac {
  public seeds: number[] = [];

  private typeMap: Map<string, string>;

  private converters: ConverterMap = {};

  constructor(lines: string[]) {
    this.typeMap = new Map<string, string>();
    let currMap = "";
    for (const line of lines) {
      if (line.startsWith("seeds:")) {
        this.seeds = (line.match(/\d+/g) ?? []).map(toInteger);
        continue;
      }
      const match = line.match(/(\w+)-to-(\w+) /);
      if (match) {
        currMap = match[1];
        this.converters[currMap] = [];
        this.typeMap.set(match[1], match[2]);
        continue;
      }
      const [dest, start, length] = (line.match(/\d+/g) || []).map(toInteger);

      this.converters[currMap].push([start, length, dest]);
    }
  }

  runSeed(curr: number, type = "seed"): number {
    const target = this.typeMap.get(type);
    if (!target) {
      return curr;
    }
    for (const [start, length, dest] of this.converters[type]) {
      if (curr >= start && curr < (start + length)) {
        return this.runSeed(dest - start + curr, target);
      }
    }
    return this.runSeed(curr, target);
  }

  toString() {
    return `Seeds: ${this.seeds} + ${Object.entries(this.converters.seed)}`;
  }
}

function part1Fn(almanac: Almanac) {
  return Math.min(
    ...almanac.seeds.map((x) => almanac.runSeed(x)),
  );
}

// function part2Fn(data: string[]) {
//   console.log(data)
// }

try {
  const stdIn = await loadStdInIntoArray();

  const part1 = part1Fn(new Almanac(stdIn));

  console.log(`\tPart 1: ${part1}`);

  // const part2 = part2Fn(stdIn);
  //
  // console.log(`\tPart 2: ${part2}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
