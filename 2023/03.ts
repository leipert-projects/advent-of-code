import { exit, loadStdInIntoArray, sumArray, toInteger } from "../lib/shared.js";
import Map2D, { Node } from "../lib/Map2D.ts";

interface SeenNode extends Node<string> {
  seen: boolean;
}

// deno-lint-ignore no-empty-interface
interface SnowMachine extends Map2D<string, SeenNode> {}

const mapToInts = (map: SnowMachine): number[] => {
  return map
    .render((x) => x ?? ".")
    .join("|")
    .match(/\d+/g)?.map(toInteger) || [];
};
const markSeen = (map: SnowMachine, node: SeenNode): [number, SeenNode[]] => {
  if (node.seen) {
    return [0, []];
  }
  node.seen = true;
  let c = 0;
  const nodes = [node];
  map.getNeighbors(node, true).forEach((neighbor) => {
    const n = neighbor;
    if (!n.seen) {
      c += 1;
    }
    const [, chain] = markSeen(map, n);
    nodes.push(...chain);
  });
  return [c, nodes];
};

function buildMap(data: string[]): SnowMachine {
  const map = new Map2D<string, SeenNode>();
  for (let y = 0; y < data.length; y += 1) {
    const line = data[y];
    for (let x = 0; x < line.length; x += 1) {
      if (line[x] !== ".") {
        map.set(x, y, line[x]);
      }
    }
  }
  return map;
}

function part1Fn(map: SnowMachine): number[] {
  for (const node of map) {
    if (!/\d/.test(node.value)) {
      markSeen(map, node);
    }
  }

  for (const node of map) {
    if (!node.seen) {
      map.delete(node.x, node.y);
    }
  }

  return mapToInts(map);
}

function part2Fn(map: SnowMachine): number[] {
  const res = [];
  for (const node of map) {
    if (node.value === "*") {
      const [c, nodes] = markSeen(map, node);
      if (c === 2) {
        const temp = new Map2D<string, SeenNode>();
        for (const n of nodes) {
          temp.set(n.x, n.y, n.value);
        }
        temp.set(node.x, node.y, ".");
        const [one, two] = mapToInts(temp);
        res.push(one * two);
      }
    }
  }

  return res;
}

try {
  const stdIn = await loadStdInIntoArray();
  const part1 = part1Fn(buildMap(stdIn));

  console.log(`\tPart 1: Sum of Part numbers ${sumArray(part1)}`);

  const part2 = part2Fn(buildMap(stdIn));

  console.log(`\tPart 2: Gear Ratios ${sumArray(part2)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
