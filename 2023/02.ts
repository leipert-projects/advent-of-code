import { exit, loadStdInIntoArray, sumArray, toInteger } from "../lib/shared.js";

type Color = "red" | "green" | "blue";
interface Game extends RegExpMatchArray {
  id: number;
}

function part1Fn(data: Game[]): number[] {
  const availableCubes = { red: 12, green: 13, blue: 14 };
  return (data.filter((game) => {
    return game.every((m) => {
      const [count, color] = m.split(" ");
      return toInteger(count) <= availableCubes[color as Color];
    });
  }).map((game) => game.id));
}

function part2Fn(data: Game[]): number[] {
  return (data.map((game) => {
    return game.reduce((acc, m) => {
      const [count, color] = m.split(" ");
      acc[color as Color] = Math.max(acc[color as Color], toInteger(count));
      return acc;
    }, { red: 1, green: 1, blue: 1 });
  }).map((minMax) => minMax.green * minMax.blue * minMax.red));
}

try {
  const stdIn: Game[] = (await loadStdInIntoArray()).flatMap((line) => {
    const matches = line.match(/\d+ (?:red|green|blue)/g);
    const id = line.match(/\d+/g);
    if (!matches || !id) {
      return [];
    }
    (matches as Game).id = toInteger(id[0]);
    return [matches as Game];
  });
  const part1 = part1Fn(stdIn);

  console.log(`\tPart 1: ${sumArray(part1)}`);

  const part2 = part2Fn(stdIn);

  console.log(`\tPart 2: ${sumArray(part2)}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
