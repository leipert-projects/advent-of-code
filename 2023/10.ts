import { exit, loadStdInIntoArray } from "../lib/shared.js";
import Map2D, { Node } from "../lib/Map2D.ts";

enum PipePart {
  NIL = 2,
  START = 3,
  LEFT_RIGHT = 11,
  UP_DOWN = 13,
  NORTH_WEST = 17,
  NORTH_EAST = 19,
  SOUTH_WEST = 23,
  SOUTH_EAST = 29,
  NORTH = UP_DOWN * NORTH_WEST * NORTH_EAST,
  SOUTH = UP_DOWN * SOUTH_EAST * SOUTH_WEST,
  EAST = LEFT_RIGHT * SOUTH_EAST * NORTH_EAST,
  WEST = LEFT_RIGHT * SOUTH_WEST * NORTH_WEST,
}

interface PipeNode extends Node<PipePart> {
  visited?: boolean;
  steps?: number;
  value: PipePart;
}

class Pipescape extends Map2D<PipePart, PipeNode> {
  start?: PipeNode;
}

function mapCharToPipePart(arg0: string): PipePart {
  switch (arg0) {
    case ".":
      return PipePart.NIL;
    case "S":
      return PipePart.START;
    case "-":
      return PipePart.LEFT_RIGHT;
    case "|":
      return PipePart.UP_DOWN;
    case "F":
      return PipePart.NORTH_WEST;
    case "7":
      return PipePart.NORTH_EAST;
    case "L":
      return PipePart.SOUTH_WEST;
    case "J":
      return PipePart.SOUTH_EAST;
    default:
      throw new Error(`Unknown char ${arg0}`);
  }
}

function buildMap(data: string[]): Pipescape {
  const map = new Pipescape();
  for (let y = 0; y < data.length; y += 1) {
    const line = data[y];
    for (let x = 0; x < line.length; x += 1) {
      const node = map.set(x, y, mapCharToPipePart(line[x]));
      if (node.value === PipePart.START) {
        map.start = node;
      }
    }
  }
  return map;
}

function part1Fn(map: Pipescape) {
  if (map.start) {
    map.start.visited = true;
    map.start.steps = 0;
    const queue: PipeNode[] = [];
    //left
    const [left, right, top, bottom] = [
      map.getNode(map.start.x - 1, map.start.y),
      map.getNode(map.start.x + 1, map.start.y),
      map.getNode(map.start.x, map.start.y - 1),
      map.getNode(map.start.x, map.start.y + 1),
    ];
    if (right && (PipePart.EAST % right.value) === 0) {
      pushIfNecessary(queue, right, 0);
    }
    if (left && (PipePart.WEST % left.value) === 0) {
      pushIfNecessary(queue, left, 0);
    }
    if (top && (PipePart.NORTH % top.value) === 0) {
      pushIfNecessary(queue, top, 0);
    }
    if (bottom && (PipePart.SOUTH % bottom.value) === 0) {
      pushIfNecessary(queue, bottom, 0);
    }

    while (queue.length) {
      const node = queue.shift();
      if (!node) {
        continue;
      }

      const { x, y, value } = node;
      node.visited = true;
      const steps = node.steps ?? 0;

      if ((PipePart.EAST % value) === 0) {
        pushIfNecessary(queue, map.getNode(x - 1, y), steps);
      }
      if ((PipePart.WEST % value) === 0) {
        pushIfNecessary(queue, map.getNode(x + 1, y), steps);
      }
      if ((PipePart.NORTH % value) === 0) {
        pushIfNecessary(queue, map.getNode(x, y + 1), steps);
      }
      if ((PipePart.SOUTH % value) === 0) {
        pushIfNecessary(queue, map.getNode(x, y - 1), steps);
      }
    }

    let max = 0;
    for (const node of map) {
      max = Math.max(max, node.steps ?? 0);
    }
    return max;
  }
  throw new Error("No start node found");
}
function pushIfNecessary(queue: PipeNode[], node: PipeNode | undefined, steps: number) {
  if (node && !node.visited) {
    node.steps = steps + 1;
    queue.push(node);
  }
}

enum Topology {
  NIL,
  LOOP,
  INSIDE,
}

interface TopologyNode extends Node<Topology> {
  visited?: boolean;
}

function part2Fn(map: Pipescape) {
  const spacedMap = new Map2D<Topology, TopologyNode>();
  for (let x = map.minX - 1; x < (map.maxX * 2) + 2; x += 1) {
    for (let y = map.minY - 1; y < (map.maxY * 2) + 2; y += 1) {
      spacedMap.set(x, y, Topology.NIL);
    }
  }
  for (const node of map) {
    const { visited, x, y, value } = node;
    if (visited) {
      spacedMap.set(x * 2, y * 2, Topology.LOOP);
      if ((PipePart.EAST % value) === 0) {
        spacedMap.set(x * 2 - 1, y * 2, Topology.LOOP);
      }
      if ((PipePart.WEST % value) === 0) {
        spacedMap.set(x * 2 + 1, y * 2, Topology.LOOP);
      }
      if ((PipePart.NORTH % value) === 0) {
        spacedMap.set(x * 2, y * 2 + 1, Topology.LOOP);
      }
      if ((PipePart.SOUTH % value) === 0) {
        spacedMap.set(x * 2, y * 2 - 1, Topology.LOOP);
      }
    } else {
      spacedMap.set(x * 2, y * 2, Topology.INSIDE);
    }
  }
  const start = spacedMap.getNode(spacedMap.minX, spacedMap.minY);
  if (!start) {
    throw new Error("No start node found");
  }
  const queue = [start];
  while (queue.length) {
    const node = queue.shift();
    if (!node) {
      continue;
    }
    if (node?.value !== Topology.LOOP && !node?.visited) {
      queue.push(...spacedMap.getNeighbors(node));
      node.value = Topology.NIL;
    }

    node.visited = true;
  }

  let c = 0;
  for (const node of spacedMap) {
    if (node.value === Topology.INSIDE) {
      c += 1;
    }
  }
  return c;
}

try {
  const map = buildMap(await loadStdInIntoArray());
  const part1 = part1Fn(map);

  console.log(`\tPart 1: Max steps within pipe ${part1}`);

  const part2 = part2Fn(map);

  console.log(`\tPart 2: Inside fields ${part2}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
