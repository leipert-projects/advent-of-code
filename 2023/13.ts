import { exit, loadStdInIntoChunks, sumArray } from "../lib/shared.js";
import { transposeStringArray } from "../lib/matrices.ts";

function mirrorMirror(lines: string[], prevPattern = 0): number {
  for (let i = 0; i < lines.length - 1; i += 1) {
    let smudged = false;
    if (((i + 1) === prevPattern)) {
      continue;
    }
    const l = lines[i];
    const r = lines[i + 1];
    if (l !== r) {
      if (!(prevPattern && l && r && oneCharDiff(l, r))) {
        smudged = true;
        continue;
      }
    }
    let res = true;
    for (let j = 1; i - j >= 0; j += 1) {
      const a = lines[i - j];
      const b = lines[i + j + 1];
      if (b === undefined || a === undefined) {
        break;
      }
      if (a === b) {
        continue;
      }

      if (prevPattern && !smudged && oneCharDiff(a, b)) {
        smudged = true;
        continue;
      }
      res = false;
      break;
    }
    if (res) {
      return i + 1;
    }
  }

  return 0;
}

function part1Fn(data: string[][]): number[] {
  const result = [];

  for (const pattern of data) {
    const res = mirrorMirror(pattern);
    if (res !== 0) {
      result.push(res * 100);
    } else {
      result.push(mirrorMirror(transposeStringArray(pattern)));
    }
  }

  return result;
}

function part2Fn(data: string[][], prevResults: number[]): number {
  let result = 0;

  for (let i = 0; i < data.length; i++) {
    const pattern = data[i];

    let res = mirrorMirror(pattern, prevResults[i] / 100);
    if (res !== 0) {
      result += res * 100;
    } else {
      res = mirrorMirror(
        transposeStringArray(pattern),
        prevResults[i],
      );
      result += res;
    }
  }

  return result;
}

try {
  const stdIn = await loadStdInIntoChunks((l) => l);
  const part1 = part1Fn(stdIn);

  console.log(`\tPart 1: ${sumArray(part1)}`);

  const part2 = part2Fn(stdIn, part1);

  console.log(`\tPart 2: ${part2}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}

function oneCharDiff(l: string, r: string) {
  let diff = 0;
  for (let i = 0; i < l.length; i += 1) {
    if (l[i] !== r[i]) {
      if (diff > 0) {
        return false;
      }
      diff += 1;
    }
  }
  return true;
}
