import { lowestCommonMultiple } from "../lib/math.js";
import { exit, loadStdInIntoArray } from "../lib/shared.js";

function constructMap(data: string[]) {
  const [steps, ...nodes] = data;

  const map = Object.fromEntries(
    nodes.map((line) => [
      line.substring(0, 3),
      line.substring(line.indexOf("(") + 1, line.indexOf(")")).split(/[^A-Z]+/),
    ]),
  );

  return { steps, map };
}
function part1Fn(data: string[]) {
  const { steps, map } = constructMap(data);
  let curr = "AAA";
  let i = 0;
  while (curr !== "ZZZ") {
    const step = steps[i % steps.length] === "L" ? 0 : 1;
    curr = map[curr][step];
    i += 1;
  }

  return i;
}

function part2Fn(data: string[]) {
  const { steps, map } = constructMap(data);

  return Object.keys(map)
    .filter((node) => node.endsWith("A"))
    .map((node) => {
      let curr = node;
      let i = 0;
      while (!curr.endsWith("Z")) {
        const step = steps[i % steps.length] === "L" ? 0 : 1;
        curr = map[curr][step];
        i += 1;
      }
      return i;
    }).reduce((acc, curr) => lowestCommonMultiple(acc, curr));
}

try {
  const stdIn = await loadStdInIntoArray();
  const part1 = part1Fn(stdIn);

  console.log(`\tPart 1: ${part1}`);

  const part2 = part2Fn(stdIn);

  console.log(`\tPart 2: ${part2}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
