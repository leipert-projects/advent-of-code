import { exit, loadStdInIntoArray, sumArray } from "../lib/shared.js";
import Map2D, { Node } from "../lib/Map2D.ts";

enum Stone {
  Boulder = "O",
  Square = "#",
}

// deno-lint-ignore no-empty-interface
interface Reflector extends Map2D<Stone, Node<Stone>> {}

function buildMap(data: string[], rollNorth = true): Reflector {
  const map = new Map2D<Stone, Node<Stone>>();
  for (let y = 0; y < data.length; y++) {
    const line = data[y];
    for (let x = 0; x < line.length; x++) {
      const char = line[x];
      if (char === Stone.Square) {
        map.set(x, y, char);
      }
      if (char === Stone.Boulder) {
        let y1 = y - 1;
        while (rollNorth && y1 >= map.minY) {
          if (map.get(x, y1) !== undefined) {
            break;
          }
          y1 -= 1;
        }
        map.set(x, y1 + 1, char);
      }
    }
  }
  return map;
}
function* part1Fn(data: string[]) {
  const map = buildMap(data, true);
  for (const node of map) {
    if (node.value === Stone.Boulder) {
      yield (map.maxY - node.y + 1);
    }
  }
}

const direction: [0 | 1, 0 | 1, -1 | 1][] = [
  [1, 0, -1],
  [0, 1, -1],
  [1, 0, 1],
  [0, 1, 1],
];

function rollAllDirections(
  boulders: [number, number][],
  squareIndex: readonly [number[][], number[][]],
) {
  for (const [coordinate, otherCoordinate, mult] of direction) {
    boulders = boulders
      .sort((b1, b2) => (mult * -1 * b1[coordinate]) + (mult * b2[coordinate]));

    const otherCoordinateIndex: number[][] = [];

    for (let i = 0; i < boulders.length; i += 1) {
      const node = boulders[i];
      let current = node[coordinate];
      const other = node[otherCoordinate];
      otherCoordinateIndex[other] ||= [...squareIndex[otherCoordinate][other]];

      do {
        current += mult;
      } while (!(otherCoordinateIndex[other].includes(current)));

      current -= mult;
      node[coordinate] = current;
      otherCoordinateIndex[other].push(current);
    }
  }
  return boulders;
}

function* part2Fn(data: string[]) {
  const map = buildMap(data, false);
  let boulders: [number, number][] = [];

  const squareIndex: [number[][], number[][]] = [
    Array(map.maxX + 1).fill("").map(() => [-1, map.maxY + 1]),
    Array(map.maxY + 1).fill("").map(() => [-1, map.maxX + 1]),
  ];

  for (const node of map) {
    if (node.value === Stone.Square) {
      squareIndex[0][node.x] ||= [];
      squareIndex[0][node.x].push(node.y);
      squareIndex[1][node.y] ||= [];
      squareIndex[1][node.y].push(node.x);
    } else if (node.value === Stone.Boulder) {
      boulders.push([node.x, node.y]);
    }
  }

  const stored = new Map<string, number>();

  let cycleLength = 0;

  for (let i = 0; i < 1000000000; i += 1) {
    boulders = rollAllDirections(boulders, squareIndex);
    if (cycleLength > 0) {
      continue;
    }
    const key = JSON.stringify(boulders);
    if (stored.has(key)) {
      cycleLength = i - (stored.get(key) ?? 0);
      i += Math.floor((1000000000 - i) / cycleLength) * cycleLength;
    }
    stored.set(key, i);
  }

  for (const node of boulders) {
    yield (map.maxY - node[1] + 1);
  }
}

try {
  const stdIn = await loadStdInIntoArray();
  const part1 = part1Fn(stdIn);

  console.log(`\tPart 1: ${sumArray([...part1])}`);

  const part2 = part2Fn(stdIn);

  console.log(`\tPart 2: ${sumArray([...part2])}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
