import { combinate } from "../lib/math.js";
import { exit, loadStdInIntoArray, sumArray, toInteger } from "../lib/shared.js";
function part1Fn(data: string[]) {
  let total = 0;
  for (const line of data) {
    const [springs, count] = line.split(" ");
    const order = count.split(",").map(toInteger);
    const rgx = new RegExp(order.map((x) => "#".repeat(x)).join("\.+"));
    const totalBroken = sumArray(order);
    const unknownSprings = [];
    let brokenSpringCount = 0;
    for (let i = 0; i < springs.length; i += 1) {
      if (springs[i] === "?") {
        unknownSprings.push(i);
      } else if (springs[i] === "#") {
        brokenSpringCount += 1;
      }
    }
    const unknownSpringCount = unknownSprings.length;
    const missingSprings = totalBroken - brokenSpringCount;

    if (missingSprings === 0) {
      total += 1;
      continue;
    }
    const combinations = combinate(unknownSpringCount, missingSprings);
    for (const combination of combinations) {
      const variant = springs.split("");
      for (let i = 0; i < unknownSpringCount; i += 1) {
        variant[unknownSprings[i]] = combination.includes(i) ? "#" : ".";
      }
      if (rgx.test(variant.join(""))) {
        total += 1;
      }
    }
  }
  return total;
}

// function part2Fn(data: string[]) {
//   console.log(data)
// }

try {
  const stdIn = await loadStdInIntoArray();
  const part1 = part1Fn(stdIn);

  console.log(`\tPart 1: ${part1}`);

  // const part2 = part2Fn(stdIn);
  //
  // console.log(`\tPart 2: ${part2}`);
} catch (e) {
  console.warn("ERROR:");
  console.warn(e);
  exit(1);
}
