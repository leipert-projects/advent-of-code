#!/usr/bin/env zsh
set -euo pipefail
IFS=$'\n\t'
SCRIPT_DIR="${0:a:h}"

function run_script {
  local script=$1
  local input=$2
  local bun="bun run $script < $input"
  local deno="deno run --allow-read=$SCRIPT_DIR,/dev/stdin --no-config $script < $input"
  if [[ $script == *.ts ]]; then
    yarn run esbuild "$script" --bundle --outfile="$script.js" --minify --target=node20 --platform=node --format=esm
    hyperfine --export-markdown ".tmp" --warmup 3 "$bun" "$deno" "node $script.js < $input"
    rm -f "$script.js"
  else
    hyperfine --export-markdown ".tmp" --warmup 3 "$bun" "$deno" "node $script < $input"
  fi
}

function run_with_data_file {
    local script=$1
    local data_file=$2
    {
      echo "## $file"
      echo ""
      echo '```'
      bun run "$script" 2>&1 < "$data_file"
      echo '```'
      echo ""
    } >> "$out_file"
    run_script "$script" "$data_file"
    {
      sed "s#$SCRIPT_DIR/\{0,1\}#./#g" "$SCRIPT_DIR/.tmp"
    } >> "$out_file"
    rm -f ".tmp"
}

script="$1"
year="$2"
day="$3"
out_file="$4"

if ! [ -f "$script" ]; then
  echo "ERROR: $script doesn't exist"
  exit 1
fi
rm -f "$out_file"
touch "$out_file"
no_input="true"
if [ "${NO_STDIN:-false}" = "false" ]; then
  for file in "$year/data/${day}"{_example,''}.txt; do
    data_file="$SCRIPT_DIR/$file"
    if [ -f "$data_file" ]; then
      run_with_data_file "$script" "$data_file"
      no_input="false"
    fi
  done
fi
if [ "$no_input" = "true" ]; then
  file="$script"
  run_with_data_file "$script" "/dev/null"
fi
{
  echo ""
  echo " - bun $(bun --version)"
  echo " - $(deno --version | grep "deno")"
  echo " - node $(node --version)"
} >> "$out_file"

deno fmt "$out_file"
